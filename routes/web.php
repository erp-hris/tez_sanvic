<?php

Route::any('/', 'Auth\LoginController@showLoginForm');

Auth::routes();


// Dashboard
Route::any('/', 'HomeController@index');

Route::any('/{module}/{option}', 'ApplicationController@index');
Route::any('/{module}/{option}/create', 'ApplicationController@create');
Route::any('/{module}/{option}/datatables', 'ApplicationController@datatables');
Route::any('/{module}/{option}/store', 'ApplicationController@store');
Route::any('/{module}/{option}/upload', 'ApplicationController@upload');
Route::any('/{module}/{option}/{id}/attachment', 'ApplicationController@attachment');
Route::any('/tez/{permit}/{id}/to-evaluate', 'ApplicationController@to_evaluate');
Route::any('/create_account', 'ApplicationController@create_account');

Route::any('/report', 'ReportController@index');
Route::any('/report/view-report/{option}', 'ReportController@view_report');
