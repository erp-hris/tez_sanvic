<?php

return [


'module' => 'Module',

// page
'add_new_record' => 'Add New Record',
'update_record' => 'Updating Record',
'delete_record' => 'Deleting Record',
'action' => 'Action',
'options' => 'Options',
'view' => 'View',
'edit' => 'Edit',
'delete' => 'Delete',
'save' => 'Save',
'cancel' => 'Cancel',

'add_new_application' => 'Add New Application',
'update_application' => 'Updating Application',
'delete_application' => 'Deleting Application',
'approve_application' => 'Approving Application',

// alerts
'added_successfully' => 'The :attribute has been added successfully!',
'updated_successfully' => 'The :attribute has been updated successfully!',
'deleted_successfully' => 'The :attribute has been deleted successfully!',

'check_inputs' => 'Please check input fields.',
'no_record_found' => 'No Record Found.',

'add_this' => 'Are you sure you want to add this?',
'delete_this' => 'Are you sure you want to delete this?',
'update_this' => 'Are you sure you want to update this?',
'continue_this' => 'Are you sure you want to continue?',
'download_this' => 'Are you sure you want to download this?',
'logout_this' => 'Are you sure you want to logout this?',
'exit_this' => 'Are you sure you want to exit?',
'deactivate_this' => 'Are you sure you want to deactivate this?',

'apply_this' => 'Are you sure you want to apply this?',
'approve_this' => 'Are you sure you want to approve this?',
'cancel_this' => 'Are you sure you want to cancel?',

'do_continue' => 'Do you want to continue?',



'business_permit' => 'Business Permit',
'business_permit_application_form' => 'Application Form',
'business_permit_attachment' => 'Attachments',

'location_permit' => 'Location Permit',
'location_permit_application_form' => 'Application Form',
'location_permit_attachment' => 'Attachments',

'building_permit' => 'Building Permit',
'building_permit_application_form' => 'Application Form',
'building_permit_attachment' => 'Attachments',
'architectural_permit' => 'Architectural Permit',
'structural_permit' => 'Structural Permit',
'electrical_permit' => 'Electrical Permit',
'mechanical_permit' => 'Mechanical Permit',
'electronics_permit' => 'Electronics Permit',
'sanitary_permit' => 'Sanitary Permit',

'occupancy_permit' => 'Occupancy Permit',
'occupancy_permit_application_form' => 'Application Form',
'occupancy_permit_attachment' => 'Attachments',

'menu' => 'Menu',
'home' => 'Home',
'account' => 'Account',
'system_config' => 'System Config',
'user_account' => 'User Account',

'report' => 'Report',


/** file-setup **/

//label
'code' => 'Code',
'remarks' => 'Remarks',
'name' => 'Name',

'file_setup' => 'File Setup',
'files' => 'Requirements',

'please_select' => 'Please select option',

];