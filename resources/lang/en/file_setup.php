<?php

return [

// label
'code' => 'Code',
'remarks' => 'Remarks',
'name' => 'Name',
'general' => 'General',

// agencies
'agency' => 'Agency',
'agencies' => 'Agencies',
'add_agencies' => 'Add New Agency',
'edit_agencies' => 'Edit Agency',

//departments
'department' => 'Department',
'departments' => 'Departments',
'add_departments' => 'Add New Department',
'edit_departments' => 'Edit Department',

//offices
'office' => 'Office',
'offices' => 'Offices',
'add_offices' => 'Add New Office',
'edit_offices' => 'Edit Office',

//divisions
'division' => 'Division',
'divisions' => 'Divisions',
'add_divisions' => 'Add New Division',
'edit_divisions' => 'Edit Division',

//positions
'position' => 'Position',
'positions' => 'Positions',
'add_positions' => 'Add New Position',
'edit_positions' => 'Edit Position',

//appointment_status
'appointment_status' => 'Appointment Status',
'add_appointment_status' => 'Add New Appointment Status',
'edit_appointment_status' => 'Edit Appointment Status',
'appointment_type' => 'Appointment Type',

//employment_status
'employment_status' => 'Employment Status',
'add_employment_status' => 'Add New Employment Status',
'edit_employment_status' => 'Edit Employment Status',

//salary_grade
'salary_grade' => 'Salary Grade',
'add_salary_grade' => 'Add New Salary Grade',
'edit_salary_grade' => 'Edit Salary Grade',

'step' => 'Step',
'tranche' => 'Tranche',
'amount' => 'Amount',

//plantilla_items
'plantilla_items' => 'Plantilla Items',
'add_plantilla_items' => 'Add New Plantilla Item',
'edit_plantilla_items' => 'Edit Plantilla Item',

'position_id' => 'Position',
'salary_step' => 'Salary Step',
'salary_amount' => 'Salary Amount',
'position_level' => 'Position Level',
'position_classification' => 'Position Classfication',

//countries
'country' => 'Country',
'countries' => 'Countries',
'add_countries' => 'Add New Country',
'edit_countries' => 'Edit Country',

//occupations
'occupation' => 'Occupation',
'occupations' => 'Occupations',
'add_occupations' => 'Add New Occupation',
'edit_occupations' => 'Edit Occupation',

//citizenships
'citizenship' => 'Citizenship',
'citizenships' => 'Citizenships',
'add_citizenships' => 'Add New Citizenship',
'edit_citizenships' => 'Edit Citizenship',

//cities
'city' => 'City',
'cities' => 'Cities',
'add_cities' => 'Add New City',
'edit_cities' => 'Edit Cities',

'province_id' => 'Province',
'zip_code' => 'Zip Code',

//provinces
'province' => 'Province',
'provinces' => 'Provinces',
'add_provinces' => 'Add New Province',
'edit_provinces' => 'Edit Province',

//schools
'school' => 'School',
'schools' => 'Schools',
'add_schools' => 'Add New School',
'edit_schools' => 'Edit School',

'primary_level' => 'Primary Level',
'secondary_level' => 'Secondary Level',
'vocational_level' => 'Vocational Level',
'college_level' => 'College Level',
'masters_level' => 'Masters Level',

//courses
'course' => 'Course',
'courses' => 'Courses',
'add_courses' => 'Add New Course',
'edit_courses' => 'Edit Course',

'course_level' => 'Course Level',

//eligibilities
'eligibilities' => 'Eligibilities',
'add_eligibilities' => 'Add New Eligibility',
'edit_eligibilities' => 'Edit Eligibility',

//organizations
'organization' => 'Organization',
'organizations' => 'Organizations',
'add_organizations' => 'Add New Organization',
'edit_organizations' => 'Edit Organization',

//providers
'providers' => 'Providers/Sponsor',
'add_providers' => 'Add New Provider/Sponsor',
'edit_providers' => 'Edit Provider/Sponsor',

'training_institution' => 'Training Institution',
'address' => 'Address',
'education' => 'Education',
'expertise' => 'Expertise',
'experience' => 'Experience',

//trainings
'trainings' => 'Trainings',
'add_trainings' => 'Add New Training',
'training_type' => 'Training Type',
'semester' => 'Semester',
'year' => 'Year',
'provider_id' => 'Provider',
'training_description' => 'Training Description',
'proposed_attendees' => 'Proposed Attendees',
'venue' => 'Venue',
'training_start_date' => 'Start Date',
'training_end_date' => 'End Date',
'cost_per_employee' => 'Cost Per Employee',
'slot' => 'Slots',
'approved_budget' => 'Approved Budget',
'training_deadline_date' => 'Deadline Date',

// benefits

'benefits' => 'Benefits',
'add_benefits' => 'Add New Benefits',
'edit_benefits' => 'Edit Benefit',
'tax_type' => 'Tax Type',
'computation_type' => 'Computation Type', 
'amount' => 'Amount',

//adjustments

'adjustments' => 'Adjustments',
'add_adjustments' => 'Add New Adjustment',
'edit_adjustments' => 'Edit Adjustment',

//deductions

'deductions' => 'Deductions',
'add_deductions' => 'Add New Deduction',
'edit_deductions' => 'Edit Deduction',
'payroll_group' => 'Payroll Group',

//loans

'loans' => 'Loans',
'add_loans' => 'Add New Loan',
'edit_loans' => 'Edit Loan',
'loan_type' => 'Loan Type',

//responsibility

'responsibility_centers' => 'Responsibility Centers',
'add_responsibility_centers' => 'Add New Responsibility Center',
'edit_responsibility_centers' => 'Edit Responsibility Center',

//banks

'banks' => 'Banks',
'add_banks' => 'Add New Bank',
'edit_banks' => 'Edit Bank',
'branch_name' => 'Branch Name',
'account_no' => 'Account No',

//philhealth policy

'philhealth_policy' => 'Philhealth Policy',
'add_philhealth_policy' => 'Add New Philhealth Policy',
'edit_philhealth_policy' => 'Edit Philhealth Policy',

'pay_period' => 'Pay Period',
'semi_monthly' => 'Semi-Monthly',
'monthly' => 'Monthly',

'deduction_period' => 'Deduction Period',
'both' => 'Both',
'first_half' => 'First Half',
'second_half' => 'Second Half',

'policy_type' => 'Policy Type',
'system_generated' => 'System Generated',
'inputted' => 'Inputted',

'based_on' => 'Based on',
'monthly_salary' => 'Monthly Salary',
'gross_salary' => 'Gross Salary',
'gross_taxable' => 'Gross taxable',

//pagibig policy

'pagibig_policy' => 'Pagibig Policy',
'add_pagibig_policy' => 'Add New Pagibig Policy',
'edit_pagibig_policy' => 'Edit Pagibig Policy',

//gsis policy
'gsis_policy' => 'GSIS Policy',
'add_gsis_policy' => 'Add New GSIS Policy',
'edit_gsis_policy' => 'Edit GSIS Policy',

// wage rate

'wage_rate' => 'Wage Rate',
'add_wage_rate' => 'Add New Wage Rate',
'edit_wage_rate' => 'Edit Wage Rate',
'effective_date' => 'Effective Date',

// tax_table

'tax_table' => 'Tax Table',
'add_tax_table' => 'Add New Tax Table',
'edit_tax_table' => 'Edit Tax Table',

'salary_bracket_level1' => 'Salary Bracket Level 1',
'salary_bracket_level2' => 'Salary Bracket Level 2',
'salary_bracket_level3' => 'Salary Bracket Level 3',
'salary_bracket_level4' => 'Salary Bracket Level 4',
'salary_bracket_level5' => 'Salary Bracket Level 5',
'salary_bracket_level6' => 'Salary Bracket Level 6',

'daily' => 'Daily',
'weekly' => 'Weekly',
'semi_monthly' => 'Semi-Monthly',
'monthly' => 'Monthly',

'compensation_level' => 'Compensation Level',
'minimum_withholding_tax' => 'Minimum Withholding Tax',

// annual tax table

'annual_tax_table' => 'Annual Tax Table',
'add_annual_tax_table' => 'Add New Annual Tax Table',
'edit_annual_tax_table' => 'Edit Annual Tax Table',

// tax policy

'tax_policy' => 'Tax Policy',

//absences

'absences' => 'Office Authority',
'add_absences' => 'Add New Office Authority',
'edit_absences' => 'Edit Office Authority',
'absence_type' => 'Absence Type',

//leaves

'leaves' => 'Leaves',
'add_leaves' => 'Add New Leave',
'edit_leaves' => 'Edit Leave',

//Holidays

'holidays' => 'Holidays',
'add_holidays' => 'Add New Holiday',
'edit_holidays' => 'Edit Holiday',
'start_date' => 'Start Date',
'end_date' => 'End Date',
'legal_holiday' => 'Is legal holiday?',
'yearly' => 'Applied Every Year',

//office_suspensions
'office_suspensions' => 'Office Suspensions',
'add_office_suspensions' => 'Add New Office Suspension',
'edit_office_suspensions' => 'Edit Office Suspension',

'start_time' => 'Start time',
'whole_day' => 'Is whole day?',
'time_in_required' => 'Is time in required?',

//providers
'providers' => 'Providers/Sponsor',
'add_providers' => 'Add New Provider/Sponsor',
'training_institution' => 'Training Institution',
'address' => 'Address',
'education' => 'Education',
'expertise' => 'Expertise',
'experience' => 'Experience',

//trainings
'trainings' => 'Trainings',
'add_trainings' => 'Add New Training',
'training_type' => 'Training Type',
'semester' => 'Semester',
'year' => 'Year',
'provider_id' => 'Provider',
'training_description' => 'Training Description',
'proposed_attendees' => 'Proposed Attendees',
'venue' => 'Venue',
'training_start_date' => 'Start Date',
'training_end_date' => 'End Date',
'cost_per_employee' => 'Cost Per Employee',
'slot' => 'Slots',
'approved_budget' => 'Approved Budget',
'training_deadline_date' => 'Deadline Date',

// work_schedules
'work_schedules' => 'Work Schedules',
'add_work_schedules' => 'Add New Work Schedule',
'edit_work_schedules' => 'Edit Work Schedule',

'schedule_type' => 'Schedule Type',
'am_in' => 'AM (In)',
'mid_out' => 'MID (Out)',
'mid_in' => 'MID (In)',
'pm_out' => 'PM (Out)',
'strict_mid' => 'Strict MID',
'flexi_time' => 'Flexi Time',
'restday' => 'Restday',

'monday' => 'Monday',
'tuesday' => 'Tuesday',
'wednesday' => 'Wednesday',
'thursday' => 'Thursday',
'friday' => 'Friday',
'saturday' => 'Saturday',
'sunday' => 'Sunday',

// branch
'branch' => 'Branch',
'branches' => 'Branches',
'add_branches' => 'Add New Branch',
'edit_branches' => 'Edit Branch',

// section
'section' => 'Section',
'sections' => 'Sections',
'add_sections' => 'Add New Section',
'edit_sections' => 'Edit Section',

//location
'location' => 'Location',
'locations' => 'Locations',
'add_locations' => 'Add New Location',
'edit_locations' => 'Edit Location',
];