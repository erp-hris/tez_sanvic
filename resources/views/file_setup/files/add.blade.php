
<div class="row">
    <div class="col-lg-12">
        <div class="row margin-top">
            <div class="col-md-6">
                <label>Code</label>
                <input type="text" class="form-control form-control-xs" id="code" name="code">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <label>File Name</label>
                <input type="text" class="form-control form-control-xs" id="name" name="name">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>Permit Type</h5>
                <select class="select2 select2-xs" id="permit_type" name="permit_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Business Permit</option>
                    <option value="2">Location Permit</option>
                    <option value="3">Building Permit</option>
                    <option value="4">Occupancy Permit</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>Is Required?</h5>
                <select class="select2 select2-xs" id="required" name="required">
                    <option value="0">NO</option>
                    <option value="1">YES</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <label>Remarks</label>
                <input type="text" class="form-control form-control-xs" id="remarks" name="remarks">
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        
        $('#save_btn').click(function()
        {
            var frm = document.querySelector('#save_form');
            Swal.fire({
                title: "{{ __('page.add_new_record') }}",
                text: "{{ __('page.add_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave) {
                    axios.post(frm.action, {
                        code : $("#code").val(),
                        name : $("#name").val(),
                        permit_type : $("#permit_type").val(),
                        required : $("#required").val(),
                        remarks : $("#remarks").val(),
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Record has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                window.location.href="{{ url($module.'/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        })
    });

  
   
  </script>
@endsection