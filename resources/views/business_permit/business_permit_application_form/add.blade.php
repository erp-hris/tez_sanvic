
<div class="row">
    <div class="col-lg-12">
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Classification of Tourism-Related Enterprise
                </h5>
                <select class="select2 select2-xs" id="classification" name="enterprise_classification">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Primary Tourism Enterprise</option>
                    <option value="2">Secondary Tourism Enterprise</option>
                </select>
            </div>
            <div class="col-md-6">
                <br><br>
                <h5>
                    <a href="{{ url('/file_upload/checklist.pdf') }}" target="_blank">View TIEZA Permit Issuance Coverage</a>
                </h5>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6" id="primary_selection">
                <h5>
                    Types of Primary Tourism Enterprise
                </h5>
                <select class="select2 select2-xs" id="classification1" name="primary_enterprise_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Accommodation Establishments</option>
                    <option value="2">Sports and recreational Facilities</option>
                    <option value="3">Travel and Tour Agencies</option>
                    <option value="4">Tourist Transport Services</option>
                    <option value="5">Meetings, Incentives, Conventions and Exhibition (MICE) Organizers</option>
                    <option value="6">Foreign Exchange Dealers</option>
                </select>
            </div>
            <div class="col-md-6" id="primary_selection2">
                <h5>
                    Types of Tourist Transport Services
                </h5>
                <select class="select2 select2-xs" id="classification1_1" name="tourist_transport_type" disabled>
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Bus and Coaster Rental Services</option>
                    <option value="2">Van and Passenger Vehicle Rental Services</option>
                    <option value="3">Taxi Operators</option>
                    <option value="4">Tourist Drivers</option>
                    <option value="5">Sea Tourist Transport</option>
                </select>
            </div>
            <div class="col-md-6" id="secondary_selection">
                <h5>
                    Types of Secondary Tourism Enterprise
                </h5>
                <select class="select2 select2-xs" id="secondary_enterprise_type" name="secondary_enterprise_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Restaurants</option>
                    <option value="2">Specialty Shops</option>
                    <option value="3">Department Stores</option>
                    <option value="4">Museum</option>
                    <option value="5">Galleries</option>
                    <option value="6">Spas</option>
                    <option value="7">Gasoline Stations</option>
                    <option value="8">Agri-tourism Farms and Facilities</option>
                    <option value="9">School Dormitories, Hospitals, and Private Residences</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>Application No.</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled value="0000001">
            </div>
            <div class="col-md-3">
                <h5>Date Filed.</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled value="{{ date('Y-m-d', time()) }}">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 30px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>APPLICANT'S SECTION</h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Basic Information:
                </h5>
                <select class="select2 select2-xs" id="application_type" name="application_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">New</option>
                    <option value="2">Renewal</option>
                </select>
            </div>
            <div class="col-md-3">
                <h5>
                    Mode of Payment
                </h5>
                <select class="select2 select2-xs" id="payment_mode" name="payment_mode">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Annually</option>
                    <option value="2">Semi-Annually</option>
                    <option value="3">Quarterly</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    TIN
                </h5>
                <input type="text" class="form-control form-control-xs" id="tin" name="tin">
            </div>
            <div class="col-md-3">
                <h5>
                    DTI/SEC/CDA Registration Number
                </h5>
                <input type="text" class="form-control form-control-xs" id="registration_number" name="registration_number">
            </div>
            <div class="col-md-3">
                <h5>
                    Type of Business
                </h5>
                <select class="select2 select2-xs" id="business_type" name="business_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Single</option>
                    <option value="2">Partnership</option>
                    <option value="3">Corporation</option>
                    <option value="4">Cooperative</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Amendment From
                </h5>
                <select class="select2 select2-xs" id="amendment_from" name="amendment_from">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Single</option>
                    <option value="2">Partnership</option>
                    <option value="3">Corporation</option>
                    <option value="4">Cooperative</option>
                </select>
            </div>
            <div class="col-md-3">
                <h5>
                    Amendment To
                </h5>
                <select class="select2 select2-xs" id="amendment_to" name="amendment_to">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Single</option>
                    <option value="2">Partnership</option>
                    <option value="3">Corporation</option>
                    <option value="4">Cooperative</option>
                </select>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Are you enjoying tax incentive from any government entity?
                </h5>
                <select class="select2 select2-xs" id="tax_incentive_from_govt" name="tax_incentive_from_govt">
                    <option value="" selected>{{ __('page.please_select') }}</option>
                    <option value="0">NO</option>
                    <option value="1">YES</option>
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If Yes, Please specify the entity
                </h5>
                <input type="text" class="form-control form-control-xs" id="govt_entity" name="govt_entity" disabled>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Name of Taxpayer/Registrant:
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="last_name" name="last_name">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="first_name" name="first_name">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="middle_name" name="middle_name">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-3 text-center">
                Last Name
            </div>
            <div class="col-md-3 text-center">
                First Name
            </div>
            <div class="col-md-3 text-center">
                Middle Name
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Business Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_name" name="business_name">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Trade Name / Franchise
                </h5>
                <input type="text" class="form-control form-control-xs" id="trade_name" name="trade_name">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 30px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>OTHER INFORMATION <i>(Note: For renewal application, do not fill up this section)</i></h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Business Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_address" name="business_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Postal Code
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_postal_code" name="business_postal_code">
            </div>
            <div class="col-md-3">
                <h5>
                    Tel No
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_tel_no" name="business_tel_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Email Add
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_email_add" name="business_email_add">
            </div>
            <div class="col-md-3">
                <h5>
                    Mobile No
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_mobile_no" name="business_mobile_no">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Owner's Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="owner_address" name="owner_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Postal Code
                </h5>
                <input type="text" class="form-control form-control-xs" id="owner_postal_code" name="owner_postal_code">
            </div>
            <div class="col-md-3">
                <h5>
                    Tel No
                </h5>
                <input type="text" class="form-control form-control-xs" id="owner_tel_no" name="owner_tel_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Email Add
                </h5>
                <input type="text" class="form-control form-control-xs" id="owner_email_add" name="owner_email_add">
            </div>
            <div class="col-md-3">
                <h5>
                    Mobile No
                </h5>
                <input type="text" class="form-control form-control-xs" id="owner_mobile_no" name="owner_mobile_no">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-8">
                <h5>
                    In case of emergency, provide name of contact person:
                </h5>
                <input type="text" class="form-control form-control-xs" id="contact_person" name="contact_person">
            </div>
            <div class="col-md-4">
                <h5>
                    Telephone Number/Mobile No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="contact_person_number" name="contact_person_number">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-4">
                <h5>
                    Business Area (in sq.m)
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_area" name="business_area">
            </div>
            <div class="col-md-4">
                <h5>
                    Total Number of Employees
                </h5>
                <input type="text" class="form-control form-control-xs" id="total_employees" name="total_employees">
            </div>
            <div class="col-md-4">
                <h5>
                    No of Employees residing within the LGU
                </h5>
                <input type="text" class="form-control form-control-xs" id="residing_with_lgu" name="residing_with_lgu">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 30px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4><i>Note: Fill-up only (If business place is rented)</i></h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Lessor's Full Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="lessor_full_name" name="lessor_full_name">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Lessor's Full Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="lessor_address" name="lessor_address">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Lessor's Tel / Mobile No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="lessor_contact_number" name="lessor_contact_number">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Lessor's Email Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="lessor_email_add" name="lessor_email_add">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Monthly Rental
                </h5>
                <input type="text" class="form-control form-control-xs" id="monthly_rental" name="monthly_rental">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 30px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4><i>BUSINESS ACTIVITY</i></h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <table width="100%" border="1">
                    <thead class="text-center">
                        <tr>
                            <th rowspan="2">Line of Business</th>
                            <th rowspan="2">No. of Units</th>
                            <th rowspan="2">Capitalization</th>
                            <th colspan="2">Gross/Sales Receipt (for Renewal)</th>
                        </tr>
                        <tr>
                            <th>Essential</th>
                            <th>Non-Essential</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 1; $i <= 5; $i++)
                        <tr>
                            <td>
                                <input type="text" class="form-control form-control-xs" id="" name="">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-xs" id="" name="">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-xs" id="" name="">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-xs" id="" name="">
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-xs" id="" name="">
                            </td>
                        </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        $("#primary_selection").hide();
        $("#secondary_selection").hide();
        $("#primary_selection2").hide();
        $("#classification").change(function () {
            if($(this).val() == 1)
            {
                $("#secondary_selection").hide();
                $("#primary_selection").show();
                $("#primary_selection2").show();
            }
            else if($(this).val() == 2)
            {
                $("#primary_selection").hide();
                $("#primary_selection2").hide();
                $("#secondary_selection").show();
            }
            else
            {
                $("#primary_selection").hide();
                $("#primary_selection2").hide();
                $("#secondary_selection").hide();
            }
        });
        $("#classification1").change(function () {
            if($(this).val() == 4)
            {
                $("#classification1_1").prop("disabled", false);
            }
            else
            {
                $("#classification1_1").prop("disabled", true);
                $("#classification1_1").val(0).trigger('change');
            }
        });
        $('#save_btn').click(function()
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "{{ __('page.add_new_application') }}",
                text: "{{ __('page.apply_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave) {
                    axios.post(frm.action, {
                        @foreach($fields as $key => $value)
                        {{ $value }} : $("[name='{{ $value }}']").val(),
                        @endforeach
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                //window.location.href="{{ url('attendance/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });

  
   
  </script>
@endsection