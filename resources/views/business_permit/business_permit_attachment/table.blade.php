                
                <table id="application_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                    <thead>
                        <tr class="text-center">
                            <th style="width: 10%;">{{ __('page.action') }}</th>
                            <th style="width: 15%;">Filed Date</th>
                            <th style="width: 15%;">Issued Date</th>
                            <th style="width: 25%;">Business Name</th>
                            <th style="width: 10%;">Status</th>
                            <th style="width: 10%;">Lock</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>