@php
    $arr = [
        'Proof of Business Registration ',
        'Securities and Exchange Commission (SEC), if Corporation',
        'Cooperative Development Authority (CDA), if Cooperative',
        'Department of Trade and Industry (DTI), if Single    Proprietorship',
        'Registration Certificate from LGU',
        'Accreditation Certificate from DOT',
        'Certified True Copy of Articles of Incorporation and By-Laws in case of Partnership, corporation, association',
        'Occupancy Permit',
        'Proof of Right over the business location',
        'Original/Transfer Certificate of Title), if owned by the applicant',
        'Contract of Lease, if not owned by the applicant',
        'Land Tax Clearance from LGU',
        'Zoning /Locational Clearance from TIEZA',
        'Sanitary Permit/Inspection Certificate from LGU',
        'Fire Safety Insurance Certificate from BFP',
        'Social Security System Identification and Clearance',
        'Phil Health Clearance',
        'PAG-IBIG/HDMF Clearance',
        'Valid visa and Labor Permit from DOLE for non-Filipino personnel',
        'Management Structure or list of names of Employer and Employees',
        'Insurance Coverage against accidents for passenger and loss of luggage',
        'List of vehicles owned by the agency',
        'Travel Agency Management Training Certificate or equivalent',
        'MARINA Certificate of Public Conveyance (all crews are duly licensed) for sea transport',
        'DOPT Franchise for land transport',
    ];

    $attachments = $array['attachments'];
@endphp
<div class="row">
    <div class="col-lg-12">
        <div class="row" style="margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>Documentary Requirements for TIEZA BUILDING CLEARANCE</h4>
            </div>
        </div>
        <hr>
            <h5>
                For Business
            </h5>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                Files
            </div>
            <div class="col-lg-5">
                Uploaded File
            </div>
            <div class="col-lg-4">
                New File
            </div>
        </div>
        @foreach($array['files'] as $key => $value)
        <div class="row margin-top">
            <div class="col-md-3">
                {{ ($key+1) }}. {{ $value->name }}
            </div>
            <div class="col-md-5">
                @php
                    if(count($attachments) > 0)
                    {
                        foreach($attachments as $nkey => $nvalue)
                        {
                            if($nvalue->file_id == $value->id)
                            {
                                echo '<a href="'.url('/tieza/location_permit').'/'.$array['id'].'/'.$nvalue->attachment_name.'" target="_blank">'.$nvalue->attachment_name.'</a>';
                            }
                        }    
                    }
                    else
                    {
                        echo '<span id="attachment_'.$value->id.'_'.$array['id'].'">No Attachment Yet</span>';
                    }
                @endphp
                
            </div>
            <div class="col-md-3">
                <input type="file" class="form-control form-control-xs" name="file_{{ $value->id }}" id="file_{{ $value->id }}">
            </div>
            <div class="col-md-1">
                <button class="btn btn-primary" type="button" id="save_{{ $value->id }}_{{ $array['id'] }}">
                    <i class="icon icon-left mdi mdi-save"></i>
                </button>
            </div>
        </div>
        @endforeach
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        $("[id*='save_']").each(function () {
            $(this).click(function () {
                var obj = $(this).attr('id').split("_");
                var idx = obj[1];
                var id  = obj[2];   
                var frm = document.querySelector('#save_form');
                Swal.fire({
                    title: "Upload Attachment",
                    text: "{{ __('page.add_this') }}",
                    confirmButtonText: 'Proceed',
                    confirmButtonClass: 'btn btn-primary',
                    closeButtonClass: 'btn btn-secondary',
                    cancelButtonClass: 'btn btn-secondary',
                    showCloseButton: true,
                    showCancelButton: true,
                    customClass: 'colored-header colored-header-primary'
                }).then((isSave) => {
                    if (isSave.value) {
                        var formData = new FormData();
                        var file = document.querySelector('#file_' + idx);

                        formData.append("record_id", id);
                        formData.append('file_id', idx);
                        formData.append("file", file.files[0]);

                        axios.post(frm.action, formData, {
                            headers: {
                              'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            var file_to_show = response.data.file_name;
                            var str = "<a href='{{ url('/tieza/building_permit') }}" + '/' + id + '/' + file_to_show + "' target='_blank'>"+ file_to_show +"</a>";   
                            $("#attachment_" + idx + '_' + id).html(str);
                            var timerInterval = 0;
                            Swal.fire({
                                title: "The Attachment has been added successfully!",
                                html: 'I will close in <strong></strong> seconds.',
                                timer: 1000,
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                onOpen: function() {
                                    swal.showLoading();
                                    timerInterval = setInterval(function () {
                                        swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                    }, 100);
                                },
                                onClose: function() {
                                    clearInterval(timerInterval);
                                }
                            }).then(function (result) {
                                if ( result.dismiss === swal.DismissReason.timer ) {
                                    
                                }
                            });
                        })
                        .catch((error) => {
                            
                        });
                    }
                });
                return false;
            });
        });
    });

  
   
  </script>
@endsection