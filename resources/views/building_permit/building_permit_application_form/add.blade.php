
<div class="row">
    <div class="col-lg-12">
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>Type of Application for Building Permit</h5>
                <select class="select2 select2-xs" id="application_type" name="application_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">New</option>
                    <option value="2">Renewal</option>
                    <option value="3">Amendatory</option>
                </select>
            </div>
            <div class="col-md-3">
                <h5>Type of Building Permit</h5>
                <select class="select2 select2-xs" id="permit_type" name="permit_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Architectural Permit</option>
                    <option value="2">Civil / Structural Permit</option>
                    <option value="3">Electrical Permit</option>
                    <option value="4">Mechanical Permit</option>
                    <option value="5">Electronics Permit</option>
                    <option value="6">Sanitary / Plumbing</option>
                </select>
            </div>
            <div class="col-md-3">
                <h5>Application No.</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled value="0000001">
            </div>
            <div class="col-md-3">
                <h5>Area No.</h5>
                <input type="text" class="form-control form-control-xs" name="area_no" id="area_no" disabled >
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>TO BE ACCOMPLISHED IN PRINT BY THE APPLICANT</h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    OWNER / APPLICANT
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    Last Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="last_name" name="last_name">
            </div>
            <div class="col-md-3">
                <h5>
                    First Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="first_name" name="first_name">
            </div>
            <div class="col-md-3">
                <h5>
                    M.I.
                </h5>
                <input type="text" class="form-control form-control-xs" id="middle_name" name="middle_name">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    ADDRESS
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    House No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="house_number" name="house_number">
            </div>
            <div class="col-md-3">
                <h5>
                    Street
                </h5>
                <input type="text" class="form-control form-control-xs" id="street" name="street">
            </div>
            <div class="col-md-3">
                <h5>
                    Barangay
                </h5>
                <input type="text" class="form-control form-control-xs" id="barangay" name="barangay">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <h5>
                    City / Municipality
                </h5>
                <input type="text" class="form-control form-control-xs" id="city" name="city">
            </div>
            <div class="col-md-3">
                <h5>
                    Zip Code
                </h5>
                <input type="text" class="form-control form-control-xs" id="zip_code" name="zip_code">
            </div>
            <div class="col-md-3">
                <h5>
                    Telephone No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="telephone_number" name="telephone_number">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    LOCATION OF CONSTRUCTION:
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    Lot No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="lot_number" name="lot_number">
            </div>
            <div class="col-md-3">
                <h5>
                    Blk No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="block_no" name="block_no">
            </div>
            <div class="col-md-3">
                <h5>
                    TCT No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="tct_no" name="tct_no">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Tax Dec No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="tax_declaration_no" name="tax_declaration_no">
            </div>
            <div class="col-md-3">
                <h5>
                    Street
                </h5>
                <input type="text" class="form-control form-control-xs" id="construction_street" name="construction_street">
            </div>
            <div class="col-md-3">
                <h5>
                    Barangay
                </h5>
                <input type="text" class="form-control form-control-xs" id="construction_barangay" name="construction_barangay">
            </div>
            <div class="col-md-3">
                <h5>
                    City / Municipality Of
                </h5>
                <input type="text" class="form-control form-control-xs" id="construction_city" name="construction_city">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Scope of Work
                </h5>
                <select class="select2 select2-xs" id="work_scope" name="work_scope">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">NEW CONSTRUCTION
                    <option value="2">ERECTION
                    <option value="3">ADDITION
                    <option value="4">ALTERATION
                    <option value="5">RENOVATION
                    <option value="9">CONVERSION
                    <option value="6">REPAIR
                    <option value="7">MOVING
                    <option value="8">RAISING
                    <option value="9">ACCESSORY BUILDING/STRUCTURE
                    <option value="10">OTHERS (Specify)
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If other please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_work_scope" name="other_work_scope" disabled>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Use or Character of Occupancy
                </h5>
                <select class="select2 select2-xs" id="occupancy_character" name="occupancy_character">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">GROUP A : RESIDENTIAL, DWELLINGS</option>
                    <option value="2">GROUP B : RESIDENTIAL HOTEL, APARTMENT</option>
                    <option value="3">GROUP C : EDUCATIONAL, RECREATIONAL</option>
                    <option value="4">GROUP D : INSTITUTIONAL</option>
                    <option value="5">GROUP E : BUSINESS AND MERCANTILE</option>
                    <option value="6">GROUP F : INDUSTRIAL</option>
                    <option value="7">GROUP G : INDUSTRIAL STORAGE AND HAZARDOUS</option>
                    <option value="8">GROUP H : RECREATIONAL, ASSEMBLY OCCUPANT LOAD LESS THAN 1000</option>
                    <option value="9">GROUP I : RECREATIONAL, ASSEMBLY OCCUPANT LOAD 1000 OR MORE</option>
                    <option value="10">GROUP J : AGRICULTURAL, ACCESSORY</option>
                    <option value="11">OTHERS (Specify)
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If other please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_occupancy_character" name="other_occupancy_character" disabled>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Occupancy Classified
                </h5>
                <input type="text" class="form-control form-control-xs" id="occupancy_classification" name="occupancy_classification">
            </div>
            <div class="col-md-6">
                <h5>
                    Total Estimated Cost Php
                </h5>
                <input type="text" class="form-control form-control-xs" id="estimated_cost" name="estimated_cost">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Number of Units
                </h5>
                <input type="text" class="form-control form-control-xs" id="units" name="units">
            </div>
            <div class="col-md-6">
                <h5>
                    Proposed Date of Construction
                </h5>
                <input type="text" class="form-control form-control-xs" id="construction_date" name="construction_date">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Total Floor Area (sq.m)
                </h5>
                <input type="text" class="form-control form-control-xs" id="floor_area" name="floor_area">
            </div>
            <div class="col-md-6">
                <h5>
                    Expected Date of Completion
                </h5>
                <input type="text" class="form-control form-control-xs" id="expected_completion_date" name="expected_completion_date">
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        
        $('#save_btn').click(function()
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "{{ __('page.add_new_application') }}",
                text: "{{ __('page.apply_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave) {
                    axios.post(frm.action, {
                        @foreach($fields as $key => $value)
                        {{ $value }} : $("#{{ $value }}").val(),
                        @endforeach
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                //window.location.href="{{ url('attendance/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });

  
   
  </script>
@endsection