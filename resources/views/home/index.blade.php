@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
    .data-info {
        color: black;
        text-align: right;
    }
    .data-info:hover {
        cursor: pointer;
        transition: 0.5s;
        font-weight: 600;
    }
    .card-body-tieza-bg-color {
        box-shadow: 5px 5px 0px gray;
    }
    .card-header-tieza-bg-color {
        box-shadow: 5px 5px 0px gray;   
        color: black !important;
    }
    h1{
        font-family:'Arial Black' !important;
        color:rgb(6,10,57) !important;
    }
    #dashboard_canvas {
        color: black !important;
        font-weight: 600;
    }
    #dashboard_canvas a:visited {
        color: black !important;
        font-weight: 600;
    }
    #dashboard_canvas a:active {
        color: black !important;
        font-weight: 600;
    }

</style>
@endsection

@section('content')
    <div class="row" id="dashboard_canvas">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <b>{{ strtoupper(__('page.business_permit')) }}</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <b>{{ strtoupper(__('page.location_permit')) }}</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <b>{{ strtoupper(__('page.building_permit')) }}</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <b>{{ strtoupper(__('page.occupancy_permit')) }}</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Evaluate</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Evaluate</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Evaluate</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Evaluate</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Approve</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Approve</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Approve</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget mb-1 card-header-tieza-bg-color" style="color:black !important;">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <b>To Approve</b>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget-tile card-body-tieza-bg-color">
                        <div class="row data-info">
                            <div class="col-lg-12 text-right">
                                <b>--</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       $(document).ready(function () {
        App.init();
        App.dataTables(); 
       });
    </script>
@endsection