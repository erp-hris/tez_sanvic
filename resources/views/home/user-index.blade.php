@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
    .data-info {
        color: black;
        text-align: right;
    }
    .data-info:hover {
        cursor: pointer;
        transition: 0.5s;
        font-weight: 600;
    }
    .card-body-tieza-bg-color {
        box-shadow: 5px 5px 0px gray;
    }
    .card-header-tieza-bg-color {
        box-shadow: 5px 5px 0px gray;   
        color: black !important;
    }
    h1{
        font-family:'Arial Black' !important;
        color:rgb(6,10,57) !important;
    }
    #dashboard_canvas {
        color: black !important;
        font-weight: 600;
    }
    #dashboard_canvas a:visited {
        color: black !important;
        font-weight: 600;
    }
    #dashboard_canvas a:active {
        color: black !important;
        font-weight: 600;
    }

</style>
@endsection

@section('content')
    <div class="row" id="dashboard_canvas">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Welcome {{ Auth::user()->name }}</h1>
        </div>
    </div> 
    
@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       $(document).ready(function () {
        App.init();
        App.dataTables(); 
       });
    </script>
@endsection