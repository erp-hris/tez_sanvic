@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @php 
        $data = [
        'option' => $module, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $module.'.'.$option.'.table',
        'add_url' => url($module.'/'.$option.'/create'),
        ];
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
            App.dataTables();
            load_datables('#application_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
        });
        function to_evaluator(permit, id)
        {   
            Swal.fire({
                title: "Pass to Evaluator",
                html: "Do you want to send this to <b>EVALUATOR</b>?<br>After Passing this to Evaluator the current application will <b><u>automatically lock</u></b>, you cannot change the application once lock.",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave) {
                    axios.post("{{ url('tez') }}/" + permit + "/" + id  + "/to-evaluate")
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                load_datables('#application_tbl', '{{$json_url}}', {!! json_encode($columns) !!});
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        }
    </script>
@endsection
