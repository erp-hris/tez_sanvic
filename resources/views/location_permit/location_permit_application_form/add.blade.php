
<div class="row">
    <div class="col-lg-12">
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>Application No.</h5>
                <input type="text" class="form-control form-control-xs" name="id" id="id" disabled value="0000001">
            </div>
            <div class="col-md-3">
                <h5>Date Filed.</h5>
                <input type="text" class="form-control form-control-xs" name="filed_date" id="filed_date" disabled value="{{ date('Y-m-d', time()) }}">
            </div>
            <div class="col-md-3">
                <h5>Application Fee: Php</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled>
            </div>
            <div class="col-md-3">
                <h5>O.R. Number</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled>
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>PROPONENT'S INFORMATION</h4>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Name of the Proponent
                </h5>
                <input type="text" class="form-control form-control-xs" id="proponent_name" name="proponent_name">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Name of Authorized Representative (if any)
                </h5>
                <input type="text" class="form-control form-control-xs" id="representative" name="representative">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Business Name / Project Title
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_name" name="business_name">
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Line of Business / Type of Construction
                </h5>
                <input type="text" class="form-control form-control-xs" id="business_line" name="business_line">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    Location
                </h5>
                <input type="text" class="form-control form-control-xs" id="location" name="location">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Contact Number
                </h5>
                <input type="text" class="form-control form-control-xs" id="contact_number" name="contact_number">
            </div>
            <div class="col-md-3">
                <h5>
                    Fax No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="fax_number" name="fax_number">
            </div>
            <div class="col-md-3">
                <h5>
                    E-mail Address
                </h5>
                <input type="text" class="form-control form-control-xs" id="email_add" name="email_add">
            </div>
            <div class="col-md-3">
                <h5>
                    Website
                </h5>
                <input type="text" class="form-control form-control-xs" id="website" name="website">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h5>
                    <i><b>Note:</b> Authorized Representative must present ID Card and authorization in claiming the locational clearance.</i>
                </h5>
            </div>
        </div>

        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>NATURE OF APPLICATION</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Nature of Application
                </h5>
                <select class="select2 select2-xs" id="application_type" name="application_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">New</option>
                    <option value="2">Reconsideration</option>
                    <option value="3">Others</option>
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If others please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_application" name="other_application">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>PROJECT TYPE</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Project Type
                </h5>
                <select class="select2 select2-xs" id="project_type" name="project_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">New Development</option>
                    <option value="2">Improvement</option>
                    <option value="3">Additional / Extension / Expansion</option>
                    <option value="4">Others</option>
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If others please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_project" name="other_project">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>PROJECT DESCRIPTION</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h5>
                    No. of Units / Rooms
                </h5>
                <input type="text" class="form-control form-control-xs" id="units" name="units">
            </div>
            <div class="col-md-3">
                <h5>
                    No. of Storey
                </h5>
                <input type="text" class="form-control form-control-xs" id="storey" name="storey">
            </div>
            <div class="col-md-3">
                <h5>
                    Lot Area
                </h5>
                <input type="text" class="form-control form-control-xs" id="lot_area" name="lot_area">
            </div>
            <div class="col-md-3">
                <h5>
                    Floor Area
                </h5>
                <input type="text" class="form-control form-control-xs" id="floor_area" name="floor_area">
            </div>
        </div>
        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>PROJECT OWNERSHIP</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>
                    Project Type
                </h5>
                <select class="select2 select2-xs" id="ownership_type" name="ownership_type">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Owned</option>
                    <option value="2">Leased</option>
                    <option value="3">OCT</option>
                    <option value="4">TCT</option>
                    <option value="5">Rights</option>
                    <option value="6">Other</option>
                </select>
            </div>
            <div class="col-md-6">
                <h5>
                    If others please specify
                </h5>
                <input type="text" class="form-control form-control-xs" id="other_ownership" name="other_ownership">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Project Investment Cost (Php)
                </h5>
                <input type="text" class="form-control form-control-xs" id="investment_cost" name="investment_cost">
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        $('#save_btn').click(function()
        {
            var frm = document.querySelector('#add_form');
            Swal.fire({
                title: "{{ __('page.add_new_application') }}",
                text: "{{ __('page.apply_this') }}",
                confirmButtonText: 'Proceed',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-secondary',
                closeButtonClass: 'btn btn-secondary',
                showCloseButton: true,
                showCancelButton: true,
                customClass: 'colored-header colored-header-primary'
            }).then((isSave) => {
                if (isSave) {
                    axios.post(frm.action, {
                        @foreach($fields as $key => $value)
                        {{ $value }} : $("#{{ $value }}").val(),
                        @endforeach
                    })
                    .then((response) => {
                        console.log(response);
                        var timerInterval = 0;
                        Swal.fire({
                            title: "The Application has been added successfully!",
                            html: 'I will close in <strong></strong> seconds.',
                            timer: 1000,
                            customClass: 'content-actions-center',
                            buttonsStyling: true,
                            onOpen: function() {
                                swal.showLoading();
                                timerInterval = setInterval(function () {
                                    swal.getContent().querySelector('strong').textContent = swal.getTimerLeft();
                                }, 100);
                            },
                            onClose: function() {
                                clearInterval(timerInterval);
                            }
                        }).then(function (result) {
                            if ( result.dismiss === swal.DismissReason.timer ) {
                                //window.location.href="{{ url('attendance/'.$option) }}";
                            }
                        });
                    })
                    .catch((error) => {
                        
                    });
                }
            });
            return false;
        });
    });

  
   
  </script>
@endsection