@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    @php 
        $data = [
        'option' => $module, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $module.'.'.$option.'.table',
        'add_url' => url($module.'/'.$option.'/create'),
        ];
    @endphp

    @include('others.main_content', $data)
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
            App.dataTables();
        });
    </script>
@endsection
