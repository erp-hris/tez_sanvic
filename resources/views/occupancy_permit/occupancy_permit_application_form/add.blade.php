
<div class="row">
    <div class="col-lg-12">
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>Type of Application for Certificate of Occupancy</h5>
                <select class="select2 select2-xs" id="" name="">
                    <option value="0">{{ __('page.please_select') }}</option>
                    <option value="1">Full</option>
                    <option value="2">Partial</option>
                </select>
            </div>
            <div class="col-md-3">
                <h5>Building Permit No.</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled value="0000001">
            </div>
            <div class="col-md-3">
                <h5>Date Issued</h5>
                <input type="text" class="form-control form-control-xs" name="" id="" disabled >
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    OWNER / APPLICANT
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    Last Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    First Name
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    M.I.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    ADDRESS
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    House No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Street
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Barangay
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
            </div>
            <div class="col-md-3">
                <h5>
                    City / Municipality
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Zip Code
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Telephone No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    LOCATION OF CONSTRUCTION:
                </h5>
            </div>
            <div class="col-md-3">
                <h5>
                    Lot No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Blk No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    TCT No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-3">
                <h5>
                    Tax Dec No.
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Street
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    Barangay
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
            <div class="col-md-3">
                <h5>
                    City / Municipality Of
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-6">
                <h5>
                    Use or Character of Occupancy
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="" disabled>
            </div>
            <div class="col-md-3">
                <h5>
                    Group
                </h5>
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <table border="1" width="100%">
                    <tr class="text-center">
                        <td style="width: 33.33%;"></td>
                        <td style="width: 33.33%;">
                            <h5>PLANNED</h5>
                        </td>
                        <td style="width: 33.33%;">
                            <h5>ACTUAL</h5>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px;">
                            <h5>
                                DATE OF START OF CONSTRUCTION
                            </h5>
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px;">
                            <h5>
                                DATE OF COMPLETION
                            </h5>
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px;">
                            <h5>
                                TOTAL FLOOR AREA (Squre Meters)
                            </h5>
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px;">
                            <h5>
                                NO. OF STOREY(S)
                            </h5>
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 10px;">
                            <h5>
                                NO. OF UNITS
                            </h5>
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-xs" id="" name="">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
        <div class="row" style="margin-top: 30px; margin-bottom: 10px; border-top: 1px solid gray; border-bottom: 1px solid gray; background: gray; color: white;">
            <div class="col-md-12">
                <h4>SUMMARY OF ACTUAL COSTS</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <h5>
                    1. TOTAL COST OF MATERIALS
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <h5>
                    1.1 CEMENT (bags)
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <h5>
                    1.2 LUMBER (bd. ft.)
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <h5>
                    1.3 REINFORCING BARS (kg.)
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <h5>
                    1.4 G.I. SHEETS (sheets)
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <h5>
                    1.5. PREFABS STRUCTURAL STEEL (kg.)
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <h5>
                    1.6 Other Materials (bags)
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-5">
                <h5>
                    2. TOTAL COST OF DIRECT LABORS
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <i>This includes compensation whether by salary or contract for project architect/engineer down to laborers</i>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-5">
                <h5>
                    3. TOTAL COST OF EQUIPMENT UTILIZATION
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-5">
                <h5>
                    4. OTHER COSTS:
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <i>This includes professional services fees, permits and other fees</i>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-5">
                <h5>
                    TOTAL COST OF BUILDING / STRUCTURE
                </h5>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control form-control-xs" id="" name="">
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        App.init();
        App.formElements();
        $("#primary_selection").hide();
        $("#secondary_selection").hide();
        $("#primary_selection2").hide();
        $("#classification").change(function () {
            if($(this).val() == 1)
            {
                $("#secondary_selection").hide();
                $("#primary_selection").show();
                $("#primary_selection2").show();
            }
            else if($(this).val() == 2)
            {
                $("#primary_selection").hide();
                $("#primary_selection2").hide();
                $("#secondary_selection").show();
            }
            else
            {
                $("#primary_selection").hide();
                $("#primary_selection2").hide();
                $("#secondary_selection").hide();
            }
        });
        $("#classification1").change(function () {
            if($(this).val() == 4)
            {
                $("#classification1_1").prop("disabled", false);
            }
            else
            {
                $("#classification1_1").prop("disabled", true);
                $("#classification1_1").val(0).trigger('change');
            }
        });
        $('#save_btn').click(function()
        {
            var country_id = $('#country').val();
            var file_name = $('#file').val();
            var remarks = $('#remarks').val();
            if(country_id == "") 
            {
                $('#inv_cntry').removeClass('d-none');
                $('#country').addClass('is-invalid');
                return;
            }
            title = "Upload";
            text = "Are you sure you want to upload this data?"
            success = "Upload Successfully!";
            color = "colored-header colored-header-primary";
            button = "btn btn-primary";
            const swal_continue = alert_continue(title, text,button, color);
            swal_continue.then((result) => {
                if(result.value){
                    const formData = new FormData();
                    formData.append('country_id',country_id);
                    var file  = document.querySelector('#file');
                    formData.append('file_name', file.files[0]);
                    formData.append('remarks', remarks);
                    console.log(formData);
                    axios.post('add_residency/save',formData,{
                        headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                   
                  })
                  .then(function (response) {
                    const swal_success = alert_success(success, 1500);
                                swal_success.then((response) => {

                                location.href= "{{url('application/residency')}}";
                                console.log(response);

                                });
                    
                  })  
                  .catch((error) => {
                      const errors = error.response.data.errors;

                      if(typeof(errors) == 'string')
                      {
                          alert_warning(errors);
                      }
                      else
                      {
                          const firstItem = Object.keys(errors)[0];
                          const firstItemDOM = document.getElementById(firstItem);
                          const firstErrorMessage = errors[firstItem][0];

                          firstItemDOM.scrollIntoView();

                          alert_warning("{{ __('page.check_inputs') }}", 1500);

                          showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                      }
                    });  
                }

            });

        });
    });

  
   
  </script>
@endsection