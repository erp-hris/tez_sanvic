                
        
                        <a href="javascript:void(0);" class="btn btn-space btn-primary" title="{{ __('page.save') }}" id="{{ isset($save_btn) ? $save_btn : 'save_btn' }}"><i class="icon icon-left mdi mdi-file-plus"></i>{{ __('page.save') }}</a>
                        <a href="{{ isset($cancel_url) ? $cancel_url : 'javascript:void(0);'  }}" class="btn btn-space btn-danger" title="{{ __('page.cancel') }}" id="cancel_btn"><i class="icon icon-left mdi mdi-close"></i>{{ __('page.cancel') }}</a>
                  