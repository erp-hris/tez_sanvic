	@section('employees_css')
        @include('layouts.auth-partials.form-css')
    @endsection

    <div class="card" id="list_tec">
        <div class="card-header">

            @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                <div class="row margin-top">
                    <label class="control-label">{{ __('page.filter_by') }}</label>
                    <select class="select2 select2-xs" id="filter_user" name="filter_user">
                        <option value="">{{ __('page.please_select') }}</option>
                        @foreach($list_processors as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->username.' - '.$val->full_name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <div class="row margin-top">
                <select class="select2 select2-xs" id="filter_list" name="filter_list">
                    <option value="all">{{ __('page.please_select') }}</option>
                    
                        @foreach($application_status as $key => $val)
                            <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                        @endforeach
                  
                    <option value="4">{{ __('page.completed') }}</option>
                </select>
            </div>

            <div class="row margin-top">
                <select class="select2 select2-xs" id="filter_order" name="filter_order">
                    <option value="1">{{ __('page.ascending') }}</option>
                    <option value="0">{{ __('page.descending') }}</option>
                </select>
            </div>
            <div class="row margin-top">
                <input type="text" class="form-control form-control-xs" name="filter_value" id="filter_value" placeholder="{{ __('page.search_name') }}">
            </div>
            <div class="row margin-top">
                <input type="text" class="form-control form-control-xs" name="filter_tec" id="filter_tec" placeholder="{{ __('page.tec_no') }}">
            </div>
            <div class="row margin-top">
                <h6 class="">From</h6>
                <input type="date" class="form-control form-control-xs" name="filter_from" id="filter_from" >
                <h6 class="">To</h6>
                <input type="date" class="form-control form-control-xs" name="filter_to" id="filter_to" >
            </div>
            <div class="row margin-top">
          
            </div>
           
           
          
            <div class="row margin-top">
                <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;{{ __('page.filter') }}</a>
                <a href="javascript:void(0);" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;{{ __('page.cancel') }}</a>
            </div>
        </div>  
        <div class="card-body">
            <div class="row" style="max-height: 500px; overflow: auto;" id="div_tec_application">
                <div class="col-md-12">
                    <div id="list_tec_application"></div>
                </div>     
            </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-footer">
            <div id="alert_record"></div>
        </div>        
    </div>

    @section('employees_scripts')
    	@include('layouts.auth-partials.form-scripts')

    	<script type="text/javascript">
            var limit = 10;
            var start = 0; 
            var action = 'inactive'; 

            $(document).change(function(){
                $('input[type="checkbox"]').on('change', function() {
                    
                    $(this).prop('checked', true);
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });
            });

            $(document).ready(function () {
                App.formElements(); 

                $('#filter_list').val('1').trigger('change');
                $('#filter_order').val('1').trigger('change');

                if(action == 'inactive')
                {
                    $('#alert_record').html('Loading.. Please Wait').fadeIn(1000);

                    action = 'active';

                    filter_tec_application(null, '1', '1', null, start);
                } 

                $('#div_tec_application').scroll(function(){   
                    if($(window).scrollTop() + $('#div_tec_application').height() >= $('#div_tec_application').height() && action == 'inactive')
                    {
                        action = 'active';

                        var filter_user = $('#filter_user').val();
                        var filter_list = $('#filter_list').val();
                        var filter_order = $('#filter_order').val();
                        var filter_value = $('#filter_value').val();

                        start = start + limit;  

                        $('#alert_record').html('Loading.. Please Wait').fadeIn(1000);
                        $('input[type="checkbox"]').attr('disabled', true);

                        setTimeout(function(){  
                            filter_tec_application(filter_user, filter_list, filter_order, filter_value, start);   
                        }, 2000);
                    } 
                }); 
            });
 
            $('#filter_value').keypress(function(e){
                if(e.which == 13){
                    $('#filter_btn').click();
                }
            });

            $('#filter_btn').click(function(){

                var filter_user = $('#filter_user').val();
                var filter_list = $('#filter_list').val();
                var filter_order = $('#filter_order').val();
                var filter_value = $('#filter_value').val();
                var filter_tec = $('#filter_tec').val();
                var filter_from = $('#filter_from').val();
                var filter_to = $('#filter_to').val();


                var dupcheck = $('#dupcheck').val();
                start = 0;

                $("#list_tec_application").empty();

                filter_tec_application(filter_user, filter_list, filter_order, filter_value, start,dupcheck,filter_tec,filter_from,filter_to);
            });

            $('#cancel_btn').click(function(){
                start = 0;

                $("#list_tec_application").empty(); 
               
                $('#filter_user').val('').trigger('change.select2');

                @if(Auth::user()->isProcessor())
                    $('#filter_list').val('1').trigger('change.select2');
                    list = 1;
                @elseif(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                    $('#filter_list').val('all').trigger('change.select2');
                    list = 'all';
                @endif

                $('#filter_order').val('1').trigger('change.select2');
                $('#filter_value').val('');
            });

            function filter_tec_application(filter_user, filter_list, filter_order, filter_value, start,dupcheck="",filter_tec="",filter_from = "",filter_to = "")
            {   
                axios.get("{{ $based_url.'/tec_application/json' }}", {
                    params: {
                        filter_user : filter_user,
                        filter_list : filter_list,
                        filter_order : filter_order,
                        filter_value : filter_value, 
                        filter_tec : filter_tec, 
                        filter_from : filter_from, 
                        filter_to : filter_to, 


                        start : start,
                        dupcheck:dupcheck,
                       
                    }
                })
                .then(function(response) {  
                    const tec_applications = response.data.tec_applications; 
                    const total_record = response.data.count_tec_application; 

                    
                    // console.log(response.data.query);
                    if($.trim(tec_applications))
                    {       
                        $.each(tec_applications, function( key, value ) {
                            $("#list_tec_application").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_application('"+ value['id'] +"') name='employee'><label class='custom-control-label' style='font-size:11px;' for='"+ value['id'] +"'>"+ value['app_id'] +"</label><br><label class='custom-control-label' for='"+ value['id'] +"' style='font-size:11px;'>"+ value['full_name'] +"</label></div>")
                        }); 
                      

                      
                        action = 'inactive'; 
                        if(dupcheck == "1"){

                            $('input[type="checkbox"]').prop("checked",true);
                            var tec_id = $('input:checked').attr("id");
                           
                            select_application(tec_id);
                        } 

                       
                    } 
                    else
                    {
                        $('#alert_record').html('No Record Found.').fadeOut(1000);

                        action = 'active'; 
                    }
                }); 

                $('input[type="checkbox"]').attr('disabled', false);

                 $('#alert_record').fadeOut(1000);
            }

            @include('others.page_script')
        </script>
   	@endsection