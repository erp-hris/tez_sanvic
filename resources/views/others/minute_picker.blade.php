@php
    if (isset($interval)) $min_interval = $interval;
    else $min_interval = "5";
    $hour_start = "1";
    echo '<option value="0">Select Time</option>';
    for ($hours=$hour_start; $hours <= 24 ; $hours++) { 
        if ($hours < 24) {
            for ($mins=0; $mins < 60 ; $mins+=$min_interval) { 
                $time = str_pad($hours, 2, '0',STR_PAD_LEFT).':'.str_pad($mins, 2, '0',STR_PAD_LEFT);
                echo '<option value="'.(($hours * 60) + $mins).'">'.$time.'</option>';
            }   
        } else {
            echo '<option value="'.($hours * 60).'">24:00</option>';
        }
    }
@endphp