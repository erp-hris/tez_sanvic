<div class="row text-center"> 
    <div class="col-md-12">          
        <img src="{{ asset('/img/report_header.png') }}" alt="logo" width="50%">
        <br><br>
        <span class="report-title">
            {{ $report_title }}
        </span>
    </div>
</div> 