	@section('employees_css')
        @include('layouts.auth-partials.form-css')
    @endsection

    <div class="card card-border-color card-border-color-primary">
        <div class="card-header">
            <div class="row margin-top">
                <label class="control-label">{{ __('page.filter_by') }}</label>
                <select class="select2 select2-xs" id="filter_by">
                    <option value="">{{ __('page.please_select') }}</option>
                    @foreach($filter_by as $key => $val)
                    	<option value="{{ $val }}">{{ __('page.'.$val) }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row margin-top">
                <select class="select2 select2-xs" id="filter_list" name="filter_list">
                    <option value="">{{ __('page.please_select') }}</option>
                    @foreach($user_level as $key => $val)
                    	<option value="{{ $val['level'] }}">{{ __('page.'.$val['name']) }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row margin-top">
                <select class="select2 select2-xs" id="filter_status" name="filter_status">
                    <option value="1">{{ __('page.active') }}</option>
                    <option value="0">{{ __('page.inactive') }}</option>
                </select>
            </div>

            <div class="row margin-top">
                <input type="text" class="form-control form-control-xs" name="filter_value" id="filter_value" placeholder="{{ __('page.search_name') }}">
            </div>

            <div class="row margin-top">
                <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;{{ __('page.filter') }}</a>
                <a href="javascript:void(0);" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;{{ __('page.cancel') }}</a>
            </div>
        </div>  
        <div class="card-body">
            <div class="row" style="max-height: 350px; overflow: auto;">
                <div class="col-md-12">
                    <div id="list_user_account"></div>
                </div>     
            </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-footer">
            <div id="total_user_account"></div>
        </div>        
    </div>

    @section('employees_scripts')
    	@include('layouts.auth-partials.form-scripts')

    	<script type="text/javascript">
            $(document).change(function(){
                $('input[type="checkbox"]').on('change', function() {
                    $(this).prop('checked', true);
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });
            });

            $(document).ready(function () {
                App.formElements();

                var filter_by;
                var filter_list;
                var filter_status;
                var filter_value;
                
                filter_by = localStorage.getItem("filter_by");
                filter_list = localStorage.getItem("filter_list");
                filter_status = localStorage.getItem("filter_status");
                filter_value = localStorage.getItem("filter_value");
                
                localStorage.clear();

                localStorage.setItem('filter_by', filter_by);
                localStorage.setItem('filter_list', filter_list);
                localStorage.setItem('filter_status', filter_status);
                localStorage.setItem('filter_value', filter_value);

                if(filter_by && filter_status)
                {  
                    if(filter_by)
                    {
                        $('#filter_by').val(filter_by).trigger('change');

                        $('#filter_list').val(filter_list).trigger('change');

                        $('#filter_status').val(filter_status).trigger('change');

                        $('#filter_value').val(filter_value);

                        filter_user_account(filter_by, filter_list, filter_status, filter_value);
                    }
                    else
                    {
                        $('#filter_status').val(filter_status).trigger('change');

                        $('#filter_value').val(filter_value);

                        filter_employees('', '', filter_value, filter_status);   
                    }
                }
                else
                {
                    load_list_user_account();
                }
            });

            $('#filter_value').keypress(function(e){
                if(e.which == 13){
                    $('#filter_btn').click();
                }
            });

            $('#filter_btn').click(function(){
                var filter_by = $('#filter_by').val();
                var filter_list = $('#filter_list').val();
                var filter_status = $('#filter_status').val();
                var filter_value = $('#filter_value').val();

                localStorage.setItem("filter_by", filter_by);
                localStorage.setItem("filter_list", filter_list);
                localStorage.setItem("filter_status", filter_status);
                localStorage.setItem("filter_value", filter_value);

                filter_user_account(filter_by, filter_list, filter_status, filter_value);
            });
    
            $('#cancel_btn').click(function(){
                load_list_user_account();
            });

            function load_list_user_account()
            {
                $("#list_user_account").empty();
                $("#total_user_account").empty();
                
                $('#filter_by').val('').trigger('change.select2');
                $('#filter_list').val('').trigger('change.select2');
                $('#filter_status').val('1').trigger('change.select2');
                $('#filter_value').val('');
                
                localStorage.removeItem("filter_by");
                localStorage.removeItem("filter_list");
                localStorage.removeItem("filter_status");
                localStorage.removeItem("filter_value");

                axios.get("{{ url('/user_account/json') }}", {
                    params: {
                        filter_status : 1
                    }
                })
                .then(function(response){
                    const user_accounts = response.data.user_accounts;
                    const total_record = response.data.count_user_account;

                    $.each(user_accounts, function( key, value ) {
                        $("#list_user_account").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_account('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['full_name'] +"</label></div>")
                    });

                    $("#total_user_account").append('<label class="control-label">Total Record: '+total_record+'</label>');
                })
                .catch((error) => {
                    alert_warning(error.response.data.errors, 1500);
                })
            }

            function filter_user_account(filter_by, filter_list, filter_status, filter_value)
            {
                axios.get("{{ url('/user_account/json') }}", {
                    params: {
                    	filter_by : filter_by,
                    	filter_list : filter_list,
                    	filter_status : filter_status,
                    	filter_value : filter_value
                    }
                })
                .then(function(response) {
                    $("#list_user_account").empty();
                	$("#total_user_account").empty();

                    const user_accounts = response.data.user_accounts;
                    const total_record = response.data.count_user_account;

                    if($.trim(user_accounts))
                    {   
                        $.each(user_accounts, function( key, value ) {
	                        $("#list_user_account").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_account('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['full_name'] +"</label></div>")
	                    });

                        $("#total_user_account").append('<label class="control-label">Total Record: '+total_record+'</label>');
                    }
                    else
                    {
                        load_list_user_account();
                        alert_warning("{{ __('page.no_record_found') }}", 1500);
                    }
                })
            }

            @include('others.page_script')
        </script>
   	@endsection