		
		<aside class="page-aside">
            <div class="aside-content">
                <div class="aside-header py-2 d-xl-none d-lg-none">
                        <button class="navbar-toggle" data-target="#aside-spy" data-toggle="collapse" type="button"><span class="icon mdi mdi-caret-down"></span></button><h1>&nbsp;&nbsp;{{ __('page.list_employees') }}</h1>
                </div>
                <div class="aside-nav collapse" id="aside-spy">
                    @include('others.list_employees')
                </div>
            </div>
        </aside>