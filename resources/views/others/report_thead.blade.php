<thead>
    <tr class="th_noshow">
        <th colspan="{{ $colspan }}">
            <img src="{{ asset('img/header_tieza_1.jpg') }}" width="100%">
            <table class="centered-contact">
                <tr valign="top">
                    <th class="text-right" style="padding-right: 5px;">
                        <b>
                            6th & 7th Floors, Tower 1<br>
                            Double Dragon Plaza<br>
                            Double Dragon Meridian Park <br>
                            Macapagal Avenue corner <br>
                            Edsa Extension<br>
                            1302 Bay Area, Pasay City<br>
                        </b>    
                    </th>
                    <th class="text-left" style="border-left: 1px solid black;">
                        &nbsp;&nbsp;
                        <img src="{{ asset('img/phone.png') }}" width="16px;">
                        &nbsp;&nbsp;
                        <b>(+632) 8249-5900 Local-625</b>
                        <br>
                        &nbsp;&nbsp;
                        <img src="{{ asset('img/email.png') }}" width="16px;">
                        &nbsp;&nbsp;
                        <b>hrservices@tieza.gov.ph</b>
                        <br> 
                        &nbsp;&nbsp;
                        <img src="{{ asset('img/globe.png') }}" width="16px;">
                        &nbsp;&nbsp;
                        <b>www.tieza.gov.ph</b>
                    </th>
                </tr>
            </table>
        </th>
    </tr>
    <tr>
        <th colspan="{{ $colspan }}" class="report-title">
            {{ $report_title }}
        </th>
    </tr>
</thead>
<tfoot>
    <tr>
        <td colspan="{{ $colspan }}" style="height: 150px;">
            &nbsp;
        </td>
    </tr>
</tfoot>