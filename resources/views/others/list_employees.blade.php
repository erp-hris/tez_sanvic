    
    @section('employees_css')
        <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/select2/css/select2.min.css') }}">
    @endsection

    <div class="card">
        <div class="card-header">
                    @if(isset($attendance))
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Month</label>
                            <select class="select2 select2-xs" name="month" id="month">
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label>Year</label>
                            <select class="select2 select2-xs" name="year" id="year">
                                @for($i=2019;$i<=2022;$i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    @endif
                    
                    @if(in_array($module, ['personal_information', 'system_config', 'attendance', 'payroll']))

                    <div class="row margin-top">
                        <label class="control-label">Filter by</label>
                        <select class="select2 select2-xs" id="filter_by">
                            <option value="">Please select option</option>
                            <option value="position">POSITION</option>
                            <option value="sector">SECTOR</option>
                            <option value="department">DEPARTMENT</option>
                            <option value="division">DIVISION</option>
                            <option value="travel_tax_unit">TRAVEL TAX UNIT</option>
                            <option value="entities">ENTITIES</option>
                            <option value="employment_status">EMPLOYMENT STATUS</option>
                        </select>
                    </div>

                    <div class="row margin-top">
                        <select class="select2 select2-xs" id="list_filter" name="list_filter">
                        </select>
                    </div>

                    <div class="row margin-top">
                        <select class="select2 select2-xs" id="filter_status" name="filter_status">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>

                    @endif

                    <div class="row margin-top">
                        <input type="text" class="form-control form-control-xs" name="filter_value" id="filter_value" placeholder="Search Name">
                    </div>

                    @if(in_array($module, ['personal_information', 'system_config', 'attendance', 'payroll']))
                    <div class="row margin-top">
                        <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;Filter</a>
                        <a href="javascript:void(0);" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;Cancel</a>
                    </div>
                    @endif
        </div>  

        <div class="card-body">
                    <div class="row" style="max-height: 500px; overflow: auto;">
                        <div class="col-md-12 lp-0">
                            <div id="list_employees"></div>
                        </div>
                    </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-footer">
            <div id="total_emp"></div>
        </div>        
    </div>

    @section('employees_scripts')
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.maskedinput/jquery.maskedinput.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/js/app-form-elements.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/js/app-form-masks.js') }}" type="text/javascript"></script>
        <script src="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
                type="text/javascript"></script>

        <script type="text/javascript">
            $(document).change(function(){
                $('input[type="checkbox"]').on('change', function() {
                    $(this).prop('checked', true);
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });
            });

            $(document).ready(function () {
                App.formElements();
                App.masks();

                var filter_by;
                var list_filter;
                var filter_value;
                var filter_status;
               
                filter_by = localStorage.getItem("filter_by");
                list_filter = localStorage.getItem("list_filter");
                filter_status = localStorage.getItem("filter_status");
                filter_value = localStorage.getItem("filter_value");
                
                localStorage.clear();

                localStorage.setItem('filter_by', filter_by);
                localStorage.setItem('list_filter', list_filter);
                localStorage.setItem('filter_status', filter_status);
                localStorage.setItem('filter_value', filter_value);

                if(filter_value && filter_status)
                {
                    if(filter_by || filter_value)
                    {
                        $('#filter_by').val(filter_by).trigger('change');

                        $('#list_filter').val(list_filter).trigger('change');

                        $('#filter_status').val(filter_status).trigger('change');

                        $('#filter_value').val(filter_value);

                        filter_employees(filter_by, list_filter, filter_value, filter_status);
                    }
                    else
                    {
                        $('#filter_status').val(filter_status).trigger('change');

                        $('#filter_value').val(filter_value);

                        filter_employees('', '', filter_value, filter_status);   
                    }
                }
                else
                {
                    load_list_employees();
                }
                  
            });

            $('#filter_by').on('change', function() {
               load_select_filter();
            });

            function load_select_filter()
            {
                var selected_filter = $('#filter_by').val();

                $('#list_filter').empty();

                if(selected_filter !== '')
                {
                    axios.get("{{ url('/file_setup/json') }}/" + selected_filter + '/all')
                    .then(function(response){
                        const records = response.data.records;

                        $("#list_filter").append("<option value=''>Please select option</option>");


                        var list_filter = localStorage.getItem('list_filter');

                        $.each(records, function( key, value ) {
                            $("#list_filter").append('<option value='+ value['id'] +'>'+ value['name'] +'</option>')
                        });

                        $('#list_filter').val(list_filter).trigger('change');
                    })
                    .catch(function(error){
                        Swal.fire({
                        text: "Something error. Please contact administrator.",
                        type: 'warning',
                        customClass: 'content-text-center',
                        showConfirmButton: false,
                        timer: 1500
                        });  
                    })

                    $('#list_filter').val('49').trigger('change');
                }  
            }

            $('#filter_value').keypress(function(e){
                if(e.which == 13){//Enter key pressed
                    $('#filter_btn').click();//Trigger search button click event
                }
            });

            $('#filter_btn').click(function(){

                var filter_by = $('#filter_by').val();
                var list_filter = $('#list_filter').val();
                var filter_status = $('#filter_status').val();
                var filter_value = $('#filter_value').val();

                localStorage.setItem("filter_by", filter_by);
                localStorage.setItem("list_filter", list_filter);
                localStorage.setItem("filter_status", filter_status);
                localStorage.setItem("filter_value", filter_value);

                filter_employees(filter_by, list_filter, filter_value, filter_status);
            });

            function filter_employees(filter_by, list_filter, filter_value, filter_status)
            {
                axios.get("{{ url('/employess/json/filter') }}", {
                    params: {
                      filter_by : filter_by,
                      list_filter : list_filter,
                      filter_value : filter_value,
                      filter_status : filter_status
                    }
                })
                .then(function (response) {
                    $("#list_employees").empty()
                    $("#total_emp").empty()

                    const employees = response.data.employees;
                    const total_record = response.data.count_employee;

                    if($.trim(employees))
                    {   
                        $.each(employees, function( key, value ){
                            var first_name = [value['first_name']];

                            if(value['extension_name'] !== '') first_name.push(value['extension_name']);

                            $("#list_employees").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_employee('"+ value['id'] +"')><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['last_name'] + ", " +  first_name.join(' ')+ " " + value['middle_name']+ " " +"</label></div>")
                        });

                        $("#total_emp").append('<label class="control-label">Total Record: '+total_record+'</label>');
                    }
                    else
                    {
                        load_list_employees();

                        Swal.fire({
                            text: "{{ __('page.no_record_found') }}",
                            type: 'warning',
                            customClass: 'content-text-center',
                            showConfirmButton: false,
                            timer: 1500
                        });  
                    }
                })
            }

            $('#cancel_btn').click(function(){
                load_list_employees();
            });

            function load_list_employees()
            {
                $("#list_employees").empty();
                $("#total_emp").empty();
                $('#list_filter').empty();
                $("#list_filter").append("<option value=''>Please select option</option>");

                $('#filter_by').val('').trigger('change.select2');
                $('#list_filter').val('').trigger('change.select2');
                $('#filter_status').val('1').trigger('change.select2');

                $('#filter_value').val('');
                
                localStorage.removeItem("filter_by");
                localStorage.removeItem("list_filter");
                localStorage.removeItem("filter_value");
                localStorage.removeItem("filter_status");

                axios.get("{{ url('/employees/json') }}")
                .then(function(response){
                    const employees = response.data.employees;
                    const total_record = response.data.count_employee;

                    $.each(employees, function( key, value ) {
                        var first_name = [value['first_name']];

                        if(value['extension_name'] !== '') first_name.push(value['extension_name']);

                        $("#list_employees").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_employee('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['last_name'] + ", " +  first_name.join(' ')+ " " + value['middle_name']+ " " +"</label></div>")

                    });

                    $("#total_emp").append('<label class="control-label">Total Record: '+total_record+'</label>');
                })
                .catch(function(error){
                    Swal.fire({
                    text: "Something error. Please contact administrator.",
                    type: 'warning',
                    customClass: 'content-text-center',
                    showConfirmButton: false,
                    timer: 1500
                    });  
                })
            }

            @include('others.page_script')
        </script>
    @endsection