<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/report.css') }}" type="text/css"/>
    
    <style type="text/css">
        
    </style>
</head>
<body>
    <div>
        <div class="container-fluid">
            @include('others.report_header', [$report_title])  

            <div class="row margin-top">
                <div class="col-md-12">
                    <table width="100%" border=1>
                        <thead>
                            <tr class="th-ucase">
                                <th>#</th>
                                <th>Permit Type</th>
                                <th>Applicant Name</th>
                                <th>Date Filed</th>
                                <th>Date of Evaluation</th>
                                <th>No. of Days on Evaluation</th>
                                <th>Date of Approval</th>
                                <th>No. of Days on Approval</th>
                                <th>Date of Issuance</th>
                                <th>No. of Days on Issuance</th>
                                <th>Permit No.</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for($i=1; $i <= 10; $i++)
                            <tr>
                                <td class="text-center">{{ $i }}</td>
                                <td></td>
                                <td>Dela Cruz, Juan</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
 

    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>

</html>