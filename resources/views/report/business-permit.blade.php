<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-assets/css/app.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/report.css') }}" type="text/css"/>
    
    <style type="text/css">
        body {
            font-size: 12pt;
        }
        .small-txt {
            font-size: 10pt;
        }
    </style>
</head>
<body>
    <div>
        <div class="container">
            @include('others.report_header', [$report_title])  
            <br>
            <div class="row margin-top">
                <div class="col-md-3 text-center">
                    ___________________________________
                    <br>
                    <b>DATE ISSUED</b>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-3 text-center">
                    ___________________________________
                    <br>
                    <b>PLATE NUMBER</b>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">
                    ___________________________________
                    <br>
                    <b>BUSINESS PERMIT NUMBER</b>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row margin-top">
                <div class="col-md-3 text-center">
                    ___________________________________
                    <br>
                    <b>PERMIT EXPRESS</b>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-3 text-center">
                    ___________________________________
                    <br>
                    <b>STATUS</b>
                </div>
            </div>
            <br><br>
            <div class="row margin-top">
                <div class="col-md-4">
                    <b>This CERTIFIES that </b>
                </div>
                <div class="col-md-8" style="border-bottom: 1px solid black;">
                    &nbsp;
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <b>Business Trade Name</b>
                </div>
                <div class="col-md-8" style="border-bottom: 1px solid black;">
                    &nbsp;
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-4">
                    <b>Business Address at</b>
                </div>
                <div class="col-md-8" style="border-bottom: 1px solid black;">
                    &nbsp;
                </div>
            </div>
            <br>
            <div class="row margin-top">
                <div class="col-md-12" style="text-indent: 5%; text-align: justify;">
                    Has been granted <b>BUSINESS PERMIT</b> to operate the following business subject to existing laws, rules and regulations, guidelines of the <b>TOURISM INFRASTRUCTURE AND ENTERPRISE ZONE AUTHORITY (TIEZA)</b> and related ordinances of the <b>MUNICIPALITY OF SAN VICENTE</b> provided further, that conditions stipulated in the application must be complied with, any infraction or violation therefore will be sufficient ground for revocation of <b>PERMIT</b>.
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="border-bottom: 2px solid black;">
                    &nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3><b>TYPE OF BUSINESS PERMIT</b></h3>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-12" style="border-top: 2px solid black;">
                    &nbsp;
                </div>
            </div>
            <br>
            <div class="row margin-top">
                <div class="col-md-5" style="border: 1px solid black;">
                    <div class="row">
                        <div class="col-md-12">
                            Paid Under the following O.R.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            O.R. No.
                        </div>
                        <div class="col-md-4">
                            O.R. Date
                        </div>
                        <div class="col-md-4">
                            Amount
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 small-txt" style="text-align: justify;">
                    This permit shall be posted conspicuously at the place where the business is/are being conducted and shall be presented and/or surrendered to competent authorities upon demand: <b>NOT TRANSFERRABLE AND NOT VALID WITHOUT THE PAYMENT OF BUSINESS PERMIT FEE AND TAXES</b>. In case of closure of the business, surrender this Permit to the <b>TIEZA TEZ Office</b> for official retirement.
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-md-5 small-txt">
                    Issued subject to the conditions of compliance on the following documents within six (6) months from the date of issuance of permit.
                </div>
            </div>
            <br>
            <div class="row margin-top">
                <div class="col-md-5 small-txt ">
                    Accreditation Certificate from DOT, Copies of annual or quarterly tax payment, Pag Ibig Clearance and SSS Clearance.
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 text-center">
                    <b>APPROVED</b>
                    <br>
                    BY AUTHORITY OF THE CHIEF OPERATIONG OFFICER:
                </div>
            </div>
            <br>
            <div class="row margin-top">
                <div class="col-md-3 text-center">
                    <img src="{{ asset('img/badge.jpg') }}" width="100%">
                </div>
                <div class="col-md-3 text-center">
                    <img src="{{ asset('img/qr.png') }}" width="100%">
                </div>
                <div class="col-md-6 text-center">
                    <b>ENGR. BERNARDO C. ALARILLA</b>
                    <br>
                    Administrator, San Vicente Flagship TEZ
                </div>
            </div>
            <br>
            <div class="row margin-top">
                <div class="col-md-12 text-center" style="border: 1px solid black;">
                    ERASURE AND/OR ALTERATION WILL INVALIDATE THIS PERMIT
                </div>
            </div>
            <br><br>
            <div class="row margin-top">
                <div class="col-md-12 small-txt">
                    Printed by:
                    <br>
                    Run Date: {{ date('F d, Y',time()) }}
                    <br>
                    Run Time: {{ date('H:i A', time()) }}
                </div>
            </div>
            <div class="footer">
                <img src="{{ asset('img/footer.png') }}" width="100%" height="120">
            </div>
        </div>
    </div>
 

    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>

</html>