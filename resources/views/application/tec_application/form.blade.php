
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <label class="control-label text-red">{{ __('page.required_fields') }}</label>
                    
                    </div>
                    @if(!Auth::user()->isUser())
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label">Processing Time: </label><span id="minutes">00</span>:<span id="seconds">00</span>
                        <input type = "hidden"  id = "dupcheck">
                       
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <label class="control-label" id="tec_cert"><b></b></label>
                        
                    </div>
                   @endif
                </div>
               
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.application_no') }}</label>
                                <input type="text" class="form-control form-control-xs" id="application_no" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.date_application') }}</label>  
                                <input type="text" class="form-control form-control-xs" value="{{ date("d M Y") }}" id="date_application" readonly>
                            </div>
                        </div>
                    </div>
                </div>

                {{--
                <div class="form-group row" id="frm_input_applicant_name">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.applicant_name')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="applicant_name" 
                                    id="applicant_name">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($list_users as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->full_name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-applicant_name"></div>
                    </div>
                </div>
                --}}

                <input type="hidden" name="user_id" id="user_id" />

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.last_name') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="last_name" id="last_name">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.first_name') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="first_name" id="first_name">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.middle_name') }}</label>
                        <input type="text" class="form-control form-control-xs" name="middle_name" id="middle_name">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.mobile_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="mobile_no" id="mobile_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.email_address') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="email_address" id="email_address">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.passport_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="passport_no" id="passport_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.ticket_booking_ref_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="ticket_booking_ref_no" id="ticket_booking_ref_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.date_ticket_issued') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="date" class="form-control form-control-xs" name="date_ticket_issued" id="date_ticket_issued">
                    </div>
                </div>

                <div class="form-group row" id="frm_input_country_designation">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.country_designation')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="country_designation" 
                                    id="country_designation">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($countries as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-country_designation"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.date_validity') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="date" class="form-control form-control-xs" name="date_validity" id="date_validity">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.date_flight') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="date" class="form-control form-control-xs" name="date_flight" id="date_flight">
                    </div>
                </div>

                <div class="form-group row" id="frm_input_airlines_name">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.airlines_name')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="airlines_name" 
                                    id="airlines_name">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($list_airlines as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-airlines_name"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group row" id="frm_input_type_applicant">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.type_applicant')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="type_applicant" 
                                    id="type_applicant">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($list_type_applicants as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->code.' - '.$val->name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-type_applicant"></div>
                    </div>
                </div>

                <div id="show_applicant_to_upload">               
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <label class="control-label">Please upload scanned copies of the requirement below:</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.id_picture_2x2') }}</label>
                        <div class="pull-right" id="float_id_picture_2x2_link"></div>
                        <div id="link_id_picture_2x2"></div>
                        <input class="form-control form-control-xs" type="file" name="id_picture_2x2" id="id_picture_2x2" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="id_picture_2x2_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.passport_identification_page') }}
                        </label>
                        <div class="pull-right" id="float_passport_identification_link"></div>
                        <div id="link_passport_identification_page"></div>
                        <input class="form-control form-control-xs" type="file" name="passport_identification_page" id="passport_identification_page" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="passport_identification_page_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.airline_ticket_issued') }}
                        </label>
                        <div class="pull-right" id="float_airline_ticket_link"></div>
                        <div id="link_ticket_booking_ref_no_"></div>
                        <input class="form-control form-control-xs" type="file" name="ticket_booking_ref_no_" id="ticket_booking_ref_no_" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="ticket_booking_ref_no_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.additional_file') }}
                        </label>
                        <div class="pull-right" id="float_additional_file_link"></div>
                        <div id="link_additional_file"></div>
                        <input class="form-control form-control-xs" type="file" name="additional_file" id="additional_file" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="additional_file_fn"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                    <div class="form-group row" id="frm_input_assigned_processor">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.assigned_processor')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="assigned_processor" 
                                        id="assigned_processor">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_processors as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->username.' - '.$val->full_name }}</option>
                                        @endforeach
                                </select>
                            <div class="select_error" id="error-assigned_processor"></div>
                        </div>
                    </div>
                @endif

                @if(Auth::user()->isProcessor() || Auth::user()->isSuperAdmin())
                    <div class="form-group row" id="frm_input_assigned_supervisor">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.assigned_supervisor')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="assigned_supervisor" 
                                        id="assigned_supervisor">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_supervisor as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->username.' - '.$val->full_name }}</option>
                                        @endforeach
                                </select>
                                <div class="select_error" id="error-assigned_supervisor"></div>
                        </div>
                    </div>
                @endif
                @if(!Auth::user()->isUser())      
                <div class="form-group row" id="frm_input_status">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.status')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="status" 
                                    id="status">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($application_status as $key => $val)
                                        <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-status"></div>
                    </div>
                </div>
                @endif
                <div id="only_if_denied">
                    <div class="form-group row" id="frm_input_reason_denied">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.reason_denied')}}<span class="text-red">&nbsp;*</span> 
                            </label>
                            <div class="pull-right" id="float_reason_denied_link"><a class="" data-toggle="modal" data-target="#reason_denied_modal" id="reason_denied_" onclick="load_reason_denied()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a></div>
                                <select class="select2 select2-xs" 
                                        name="reason_denied" 
                                        id="reason_denied">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_reason_denials as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                                        @endforeach
                                        <option value="0">{{ __('page.other_reasons') }}</option>
                                </select>
                            <div class="select_error" id="error-reason_denied"></div>
                        </div>
                    </div>

                    <div id="only_if_other_reasons_selected">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.other_reasons') }}<span class="text-red">&nbsp;*</span></label>
                                <textarea class="form-control form-control-xs" name="other_reasons" id="other_reasons"></textarea> 
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <input type="hidden" name="tec_id" id="tec_id" />

        <!-- TEC Attachment Modal -->

        <div class="modal fade colored-header colored-header-primary" id="Image_2x2_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.id_picture_2x2') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center" id="load_image_2x2"></div>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="passport_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.passport_identification_page') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <table id="passport_files_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="airline_ticket_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.airline_ticket_issued') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <table id="airline_ticket_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="additional_file_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.additional_file') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="additional_file_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="file_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.type_applicant') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="file_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="reason_denied_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.reason_denied') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="form-group row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label">
                                            <div id="title_reason_denial"></div>
                                        </label> 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label">{{ __('page.reason') }}<span class="text-red">&nbsp;*</span></label>
                                        <textarea class="form-control form-control-xs" name="reason" id="reason"></textarea> 
                                        <input type="hidden" name="selected_reason" id="selected_reason" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                        <a href="javascript:void(0);" class="btn btn-space btn-primary" title="{{ __('page.save') }}" id="save_reason_denial" onclick="save_reason_denial()"><i class="icon icon-left mdi mdi-file-plus"></i>{{ __('page.save') }}</a>
                                        <a href="javascript:void(0);" class="btn btn-space btn-danger" title="{{ __('page.cancel') }}" id="cancel_reason_denial" onclick="clear_reason_denial();"><i class="icon icon-left mdi mdi-close"></i>{{ __('page.cancel') }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <table id="reason_denied_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                                    <thead>
                                        <tr class="text-center">
                                            <th style="width: 10%;">{{ __('page.action') }}</th> 
                                            <th style="width: 10%;">{{ __('page.reason') }}</th> 
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
          var dupTrue;
          var dupContinue;
          var timerMins;
          var timerSecs;


                window.onload = function() {
                    startTime();
                };
                function startTime()
                { 
                     minCount = 0,secCount =0;

                     timerSecs = setInterval(function ()
                    {
                        secCount += 1;
                        if(secCount > 59)
                        {
                            secCount = 0;
                        }
                        if(secCount < 10) $('#seconds').html("0"+secCount);
                        else $('#seconds').html(secCount);
                    },1000);
                     timerMins = setInterval(function ()
                    {
                        minCount += 1;
                        if(minCount < 10) $('#minutes').html("0"+minCount);
                        else  $('#minutes').html(minCount);
                      
                    },60000);
                }

                $(document).ready(function () {
                    $('#only_if_denied').hide();
                    $('#only_if_other_reasons_selected').hide();
                     
                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                        $('#only_approved').hide();
                        $('#only_denied').hide();

                        @if(Auth::user()->isRegularAdmin())
                            $('#type_applicant').prop('disabled', true);
                            $('#id_picture_2x2').prop('disabled', true);
                            $('#passport_identification_page').prop('disabled', true);
                            $('#ticket_booking_ref_no_').prop('disabled', true);
                            $('#additional_file').prop('disabled', true);
                            $('#assigned_processor').prop('disabled', true);
                            $('#assigned_supervisor').prop('disabled', true);
                            $('#file_0').prop('disabled', true);
                            $('#file_1').prop('disabled', true);
                            $('#file_2').prop('disabled', true);
                        @endif
                    @elseif(Auth::user()->isProcessor())
                        $('#frm_input_assigned_supervisor').hide();
                    @endif


                    $('#first_name').on('change',function()
                    {
                       var lname = $('#last_name').val();
                       var fname = $(this).val();
                       axios.get("{{ $based_url.'/chk_duplicate/json' }}", {
                            params: {
                                        lname : lname,
                                        fname : fname
                                    }
                                })
                               .then(function(response){
                                    if(response.data > 0)
                                    {
                                        const swal_continue = alert_continue("Duplicate Record", "Are you sure you want to Proceed", 'btn btn-primary', 'colored-header colored-header-primary');
                                        swal_continue.then((result) => {
                                            if(result.value){
                                               $('#filter_value').val(lname+fname);
                                               $('#filter_list').val("all");
                                               $('#dupcheck').val("1");
                                               $('#filter_btn').click();
                                               $('#dupcheck').empty();
                                               
                                            }
                                        });
                                        // Swal.fire("Duplicate Record!");
                                    }
                               })
                               .catch((error) => {
                                      alert(error);
                               });
                    });
                });
 
                @php
                    $upload_columns = array(['data' => 'action', 'sortable' => false], ['data' => 'file_name'], ['data' => 'file_type'], ['name' => 'upload_at.timestamp', 'type' => 'num', 'data' => [ "_" => 'upload_at.display', "sort" => 'upload_at' ]], ['data' => 'file']);

                    $reason_denied_columns = array(['data' => 'action', 'sortable' => false], ['data' => 'name']);
                @endphp

                function load_2x2_picture(file)
                {
                    $('#load_image_2x2').empty();

                    var tec_id = $('#tec_id').val();

                    $('#load_image_2x2').append('<img src="'+ "{{ url('/attachment') }}" + "/tec/" + tec_id + "/" + file +'" width=500px height=500px />');
                }

                function load_passport_attachment()
                {
                    var tec_id = $('#tec_id').val();

                    load_datables('#passport_files_tbl', "{!! $based_url.'/tec_application/datatables/attachment?selected_option=passport&section_id=0&tec_id=' !!}" + tec_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_airline_attachment()
                {
                    var tec_id = $('#tec_id').val();

                    load_datables('#airline_ticket_tbl', "{!! $based_url.'/tec_application/datatables/attachment?selected_option=airline&section_id=0&tec_id=' !!}" + tec_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_additional_attachment()
                {
                    var tec_id = $('#tec_id').val();

                    load_datables('#additional_file_tbl', "{!! $based_url.'/tec_application/datatables/attachment?selected_option=additional&section_id=0&tec_id=' !!}" + tec_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_file(file)
                {
                    var tec_id = $('#tec_id').val();

                    var section = $('#type_applicant').val();

                    load_datables('#file_tbl', "{!! $based_url.'/tec_application/datatables/attachment?' !!}selected_option="+ file +"&section_id="+ section +"&tec_id=" + tec_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_reason_denied()
                {
                    clear_reason_denial();

                    load_datables('#reason_denied_tbl', "{!! $based_url.'/tec_application/datatables?selected_option=reason_denied' !!}", {!! json_encode($reason_denied_columns) !!}, null);

                    var reason_denied = $('#reason_denied');

                    axios.get("{{ $based_url.'/reason_denials/json' }}")
                    .then(function(response){
                        const reason_denials = response.data.reason_denial;

                        reason_denied.empty(); 

                        if(reason_denials)
                        {  
                            reason_denied.append('<option value="">' + "{{ __('page.please_select') }}" + '</option>');

                            $.each(reason_denials, function(key, value) {  
                               reason_denied.append('<option value="'+ value['id'] +'">'+value['name']+'</option>');
                            });  

                            reason_denied.append('<option value="0">Other Reasons</option>'); 
                        }                         
                    })
                    .catch((error) => {
                        alert_warning(error.response.data.errors, 1500);
                    });
                }



                function select_reason_denial(id)
                {
                    clear_reason_denial();

                    axios.get("{{ $based_url.'/reason_denials/json' }}", {
                            params: {
                                reason_denial_id : id
                            }
                        })
                        .then(function(response){
                            const reason_denial = response.data.reason_denial;

                            if(reason_denial)
                            {
                                $('#reason').val(reason_denial['name']).trigger('change');
                                $('#selected_reason').val(reason_denial['id']);
                                $('#title_reason_denial').empty();
                                $('#title_reason_denial').append('Edit Reason Denial');
                            }                        
                        })
                        .catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        });
                }

                function remove_reason_denial(id)
                {
                    var frm, text, title, success, formData;

                    frm = document.querySelector('#delete_reason_denied_form');

                    formData = new FormData();
                    formData.append('selected_reason', id);

                    title = "Remove Reason Denial";
                    text = "{{ __('page.remove_this') }}";
                    success = "{{ __('page.removed_successfully', ['attribute' => 'Reason Denial']) }}";
                        
                    const swal_continue = alert_continue(title, text, 'btn btn-danger', 'colored-header colored-header-danger');
                    swal_continue.then((result) => {
                        if(result.value){
                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                     load_reason_denied();
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                            });
                        }
                    });
                }

                function clear_reason_denial()
                {
                    $('#selected_reason').val('');
                    $('#reason').val('').trigger('change');
                    $('#title_reason_denial').empty();
                    $('#title_reason_denial').append('Add Reason Denial');
                }

                function save_reason_denial()
                {
                    var frm, text, title, success, selected_reason, formData;

                    selected_reason = $('#selected_reason').val();

                    frm = document.querySelector('#save_reason_denied_form');

                    formData = new FormData();

                    if(selected_reason)
                    {
                        formData.append('selected_reason', selected_reason); 
                        title = "Edit Reason Denial";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => 'Reason Denial']) }}";
                    }
                    else
                    {
                        title = "Add Reason Denial";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => 'Reason Denial']) }}";
                    }

                    formData.append('reason', $('#reason').val()); 

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {

                        clearErrors();

                        if(result.value){
                            axios.post(frm.action, formData, {
                                headers: {
                                  'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => { 
                                    load_reason_denied();
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    const firstItem = Object.keys(errors)[0];

                                    const split_firstItem = firstItem.split('.');
                                    const firstItemSplit = split_firstItem[0];

                                    const firstItemDOM = document.getElementById(firstItemSplit);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();

                                    alert_warning("{{ __('page.check_inputs') }}", 1500);

                                    showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['']);
                                }
                            });  
                        }
                    });
                }

                function remove_item(option, id)
                {
                    var frm, text, title, success, formData;

                    frm = document.querySelector('#remove_form');

                    formData = new FormData();
                    formData.append('tau_id', id);

                    title = "{{ __('page.remove_attachment') }}";
                    text = "{{ __('page.remove_this') }}";
                    success = "{{ __('page.removed_successfully', ['attribute' => trans('page.remove_attachment')]) }}";
                        
                    const swal_continue = alert_continue(title, text, 'btn btn-danger', 'colored-header colored-header-danger');
                    swal_continue.then((result) => {
                        if(result.value){
                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    if(option == 'passport')
                                    {
                                        load_passport_attachment();
                                    }
                                    else if(option == 'airline')
                                    {
                                        load_airline_attachment();
                                    }
                                    else if(option == 'additional')
                                    {
                                        load_additional_attachment();
                                    }
                                    else if(option == 'file_0')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_1')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_2')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_3')
                                    {
                                        load_file(option);
                                    }
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                            });
                        }
                    });
                }

                $('#type_applicant').on('change', function() {
                    $('#show_applicant_to_upload').empty();

                    var type_applicant = $('#type_applicant').val();

                    if($(this).val().length)
                    {
                        axios.get("{{ $based_url.'/section_upload/json' }}", {
                            params: {
                                section_id : $(this).val()
                            }
                        })
                        .then(function(response){
                            const sections_uploads = response.data.sections_uploads;

                            if(sections_uploads)
                            {
                                $.each(sections_uploads, function( key, value ) {
                                    $("#show_applicant_to_upload").append("<div class='form-group row'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>"+ value['description'] +"</label><div class='pull-right' id='link_file_" + key + "'><a class='' data-toggle='modal' data-target='#file_modal' id='additional_file_files' onclick=load_file('file_"+ key +"') href='javascript:void(0);'><span class='mdi mdi-hc-2x mdi-more'></span></a></div><input class='form-control form-control-xs' type='file' name='file_"+ key +"' id='file_"+ key +"' multiple='multiple'></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><div id='file_fn_"+ key +"'></div></div>");
                                });
                            }                        
                        })
                        .catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        });
                    }
                });

                $('#status').on('change', function() {
                    $('#only_if_denied').hide();

                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                        $('#only_approved').hide();
                        $('#only_denied').hide();
                    @endif

                    if($(this).val() == '3') 
                    {
                        $('#only_if_denied').show();
                    }
                });

                $('#reason_denied').on('change', function() {
                    $('#only_if_other_reasons_selected').hide();
 

                    if($(this).val() == '0') 
                    {
                        $('#only_if_other_reasons_selected').show();
                    }
                });

                $("#save_btn").click(function() {
                    var frm, text, title, success, tec_id, formData, file_id_picture_2x2, file_passport_identification_page, file_ticket_booking_ref_no_, file_0, file_1, file_2, file_3;

                    tec_id = $('#tec_id').val();
                    user_id = $('#user_id').val();


                    frm = document.querySelector('#save_form');

                    formData = new FormData();

                    if(tec_id)
                    {
                        formData.append('tec_id', tec_id);
                        formData.append('user_id', user_id);
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                   
                    }
                    else
                    {
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        clearErrors();
                        if(result.value){ 
                            // formData.append("applicant_name", $("#applicant_name").val());

                            formData.append("date_application", $('#date_application').val());

                            formData.append("last_name", $("#last_name").val());
                            formData.append("first_name", $("#first_name").val());
                            formData.append("middle_name", $("#middle_name").val());
                            formData.append("mobile_no", $("#mobile_no").val());
                            formData.append("email_address", $("#email_address").val());

                            formData.append("passport_no", $("#passport_no").val());
                            formData.append("ticket_booking_ref_no", $("#ticket_booking_ref_no").val());
                            formData.append("date_ticket_issued", $("#date_ticket_issued").val());
                            formData.append("country_designation", $("#country_designation").val());
                            formData.append("date_validity", $("#date_validity").val());


                            formData.append("date_flight", $("#date_flight").val());
                            formData.append("airlines_name", $("#airlines_name").val());

                            file_id_picture_2x2 = document.querySelector('#id_picture_2x2');

                          
                            formData.append("id_picture_2x2", file_id_picture_2x2.files[0]);

                            file_passport_identification_page = document.querySelector('#passport_identification_page');
                            for(var i = 0; i < file_passport_identification_page.files.length; i++ ){
                                let file = file_passport_identification_page.files[i];
                                formData.append('passport_identification_page[]', file);
                            }

                            file_ticket_booking_ref_no_ = document.querySelector('#ticket_booking_ref_no_');
                            for(var i = 0; i < file_ticket_booking_ref_no_.files.length; i++ ){
                                let file = file_ticket_booking_ref_no_.files[i];
                                formData.append('ticket_booking_ref_no_[]', file);
                            }

                            file_additional_file = document.querySelector('#additional_file');
                            for(var i = 0; i < file_additional_file.files.length; i++ ){
                                let file = file_additional_file.files[i];
                                formData.append('additional_file[]', file);
                            }
                            formData.append("type_applicant", $("#type_applicant").val());


                            if($('#file_0').length)
                            {
                                file_0 = document.querySelector('#file_0');
                                for(var i = 0; i < file_0.files.length; i++ ){
                                    let file = file_0.files[i];
                                    formData.append('file_0[]', file);
                                   
                                }
                               
                            }
                         
                            
                            if($('#file_1').length)
                            {
                                file_1 = document.querySelector('#file_1');
                                for(var i = 0; i < file_1.files.length; i++ ){
                                    let file = file_1.files[i];
                                    formData.append('file_1[]', file);
                                }
                            }

                            if($('#file_2').length)
                            {
                                file_2 = document.querySelector('#file_2');
                                for(var i = 0; i < file_2.files.length; i++ ){
                                    let file = file_2.files[i];
                                    formData.append('file_2[]', file);
                                }
                            }

                            if($('#file_3').length)
                            {
                                file_3 = document.querySelector('#file_3');
                                for(var i = 0; i < file_3.files.length; i++ ){
                                    let file = file_3.files[i];
                                    formData.append('file_3[]', file);
                                }
                            }

                            @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                                formData.append("assigned_processor", $("#assigned_processor").val());

                                @if(Auth::user()->isSuperAdmin())
                                    formData.append("assigned_supervisor", $("#assigned_supervisor").val());
                                @endif
                            @endif

                            formData.append("status", $("#status").val());
                            if($("#status").val() == '3') 
                            {
                                formData.append("reason_denied", $("#reason_denied").val());

                                if($("#reason_denied").val() == '0')
                                {
                                    formData.append("other_reasons", $("#other_reasons").val());
                                }
                            }
                            if(!tec_id) formData.append("process_time","00:"+$('#minutes').html()+":"+$('#seconds').html());

                            Swal.fire({
                                title: 'Loading.. Please Wait',
                                customClass: 'content-actions-center',
                                buttonsStyling: true,
                                allowOutsideClick: false,
                                onOpen: function() {
                                
                                swal.showLoading();
 
                                axios.post(frm.action, formData, {
                                    headers: {
                                      'Content-Type': 'multipart/form-data'
                                    }
                                })
                                .then((response) => {
                                    const swal_success = alert_success(success, 1500);
                                    swal_success.then((value) => {
                                        clearInputs();
                                        // clear append
                                       
                                        clearInterval(timerSecs);
                                        clearInterval(timerMins);
                                        $('#minutes').html('00');
                                        startTime();
                                        if(tec_id)
                                        {
                                         
                                            $('#filter_btn').click();
                                            select_application(tec_id);
                                        }
                                        else
                                        {
                                            swal_success0 = alert_success("Application No : " + response.data.success.app_id);
                                            swal_success0.then((value) => {
                                                const swal_continue = alert_continue("{{ __('page.add_'.$option) }}", "Do you want to add another {{ trans('page.'.$option) }}?");
                                                swal_continue.then((result) => {
                                                    if(!result.value) {
                                                        window.location.href = "{{ $cancel_url }}";
                                                    }
                                                });
                                            });
                                        }
                                    });
                                })
                                .catch((error) => {
                                    const errors = error.response.data.errors;

                                    if(typeof(errors) == 'string')
                                    {
                                        alert_warning(errors);
                                    }
                                    else
                                    {
                                        const firstItem = Object.keys(errors)[0];

                                        const split_firstItem = firstItem.split('.');
                                        const firstItemSplit = split_firstItem[0];

                                        const firstItemDOM = document.getElementById(firstItemSplit);
                                        const firstErrorMessage = errors[firstItem][0];

                                        firstItemDOM.scrollIntoView();

                                        alert_warning("{{ __('page.check_inputs') }}", 1500);

                                        showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['applicant_name', 'country_designation', 'airlines_name', 'type_applicant', 'assigned_processor', 'status', 'reason_denied', 'assigned_supervisor']);
                                    }
                                }); 
                                }  
                            }); 
                        }
                    })
                });

                @if(Auth::user()->isSuperAdmin() || Auth::user()->isProcessor())
                    $("#add_record").click(function() {
                        var title, text;

                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.continue_this') }}";

                        const swal_continue = alert_continue(title, text);
                        swal_continue.then((result) => {
                            if(result.value){
                                window.location.href="{{ $cancel_url }}";
                            }
                        });
                    });
                @endif

                @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                    $("#delete_record").click(function() {
                        var frm, text, title, success, tec_id, formData;

                        tec_id = $('#tec_id').val();
                        frm = document.querySelector('#delete_form');

                        formData = new FormData();
                        formData.append('tec_id', tec_id);

                        title = "{{ __('page.delete_'.$option) }}";
                        text = "{{ __('page.delete_this') }}";
                        success = "{{ __('page.deleted_successfully', ['attribute' => trans('page.'.$option)]) }}";
                        
                        const swal_continue = alert_continue(title, text, 'btn btn-danger', 'colored-header colored-header-danger');
                        swal_continue.then((result) => {
                            if(result.value){
                                axios.post(frm.action, formData)
                                .then((response) => {
                                    const swal_success = alert_success(success, 1500);
                                    swal_success.then((value) => {
                                        window.location.href="{{ $cancel_url }}";
                                    });
                                })
                                .catch((error) => {
                                    const errors = error.response.data.errors;

                                    if(typeof(errors) == 'string')
                                    {
                                        alert_warning(errors);
                                    }
                                });
                            }
                        });
                    });
                
                    $("#print_preview").click(function() {
                        var tec_id, status;

                        tec_id = $('#tec_id').val();
                        status = $('#status').val();

                        axios.get("{{ $application_url.'/tec_application/print' }}", {
                            params: {
                                tec_id : tec_id,
                                status : status
                            }
                        })
                        .then(function(response){
                            window.open(response.request.responseURL, '_blank');
                        }).catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        });
                    });

                    $("#generate_certificate, #generate_denial_letter").click(function() {
                        var tec_id, status, file;

                        tec_id = $('#tec_id').val();
                        status = $('#status').val();

                        @if(Auth::user()->isSuperAdmin())
                            supervisor = $('#assigned_supervisor').val();
                        @elseif(Auth::user()->isRegularAdmin())
                            supervisor = {{ Auth::user()->id }};
                        @endif

                        const swal_continue = alert_continue('Generate Certificate / Letter', 'Note: after generating file, it will automatically send to the client. <br><br>Are you sure you want to continue?');
                        swal_continue.then((result) => {
                            if(result.value){
                                Swal.fire({
                                    title: 'Generate File...',
                                    customClass: 'content-actions-center',
                                    buttonsStyling: true,
                                    allowOutsideClick: false,
                                    onOpen: function() {
                                        swal.showLoading();

                                        axios.get("{{ $application_url.'/tec_application/generate' }}", {
                                            params: {
                                                tec_id : tec_id,
                                                status : status,
                                                supervisor : supervisor
                                            }
                                        }).then(function(response){
                                            const file = response.data.file_name;
                                            const tec_id = response.data.tec_id;
                                            
                                            const swal_success = alert_success('Generate File & Sent Successfully', 1500);
                                            swal_success.then((value) => {
                                                $('#filter_btn').click();
                                                select_application(tec_id);
                                                window.open("{{ $based_url.'/certificate/' }}" + tec_id + '/' + file, '_blank');
                                            });
                                        }).catch((error) => {
                                            
                                            alert_warning(error.response.data.errors, 1500);
                                        });
                                    }
                                });
                            }
                        });
                    });

                @endif

                function select_application(tec_id)
                {

              
                    clearErrors();
                     is_complete(false);

                    Swal.close();

                    $('#show_applicant_to_upload').empty();

                    $('#id_picture_2x2_fn').empty();
                    $('#passport_identification_page_fn').empty();
                    $('#ticket_booking_ref_no_fn').empty();
                    $('#additional_file_fn').empty();

                    $('#float_id_picture_2x2_link').empty();
                    $('#float_passport_identification_link').empty();
                    $('#float_airline_ticket_link').empty();
                    $('#float_additional_file_link').empty();
                    
                    $('#link_id_picture_2x2').empty();
                    $('#link_passport_identification_page').empty();
                    $('#link_ticket_booking_ref_no_').empty();
                    $('#link_additional_file').empty();

                    Swal.fire({
                        title: 'Loading.. Please Wait',
                        customClass: 'content-actions-center',
                        buttonsStyling: true,
                        allowOutsideClick: false,
                        onOpen: function() {
                            swal.showLoading();

                            $(window).scrollTop(0);

                            setTimeout(function(){ 
                                axios.get("{{ $based_url.'/tec_application/json' }}", {
                                    params: {
                                        tec_id : tec_id,
                                    }
                                })
                               .then(function(response){
                                    const tec_application = response.data.tec_application;
                                    $('#tec_cert').html("TEC Cert #:"+tec_application['tec_id']);
                                    $('#tec_cert').css('font-weight','bold');

                                    $('#user_id').val(tec_application['user_id']);
                                    $("#tec_id").val(tec_application['id']);
                                    $("#application_no").val(tec_application['app_id']);

                                    $('#last_name').val(tec_application['last_name']);
                                    $('#first_name').val(tec_application['first_name']);
                                    $('#middle_name').val(tec_application['middle_name']);

                                    $('#mobile_no').val(tec_application['mobile_no']);
                                    $('#email_address').val(tec_application['email']);

                                    $('#passport_no').val(tec_application['passport_no']);
                                    $("#date_application").val(tec_application['date_application']).trigger('change');
                                    $("#date_validity").val(tec_application['date_validity']).trigger('change');

                                    //$("#applicant_name").val(tec_application['user_id']).trigger('change');
                                    $("#ticket_booking_ref_no").val(tec_application['ticket_no']);
                                    $("#date_ticket_issued").val(tec_application['date_ticket_issued']).trigger('change'); 
                                    $("#country_designation").val(tec_application['country_id']).trigger('change');
                                    $("#date_flight").val(tec_application['date_flight']).trigger('change');
                                    $('#airlines_name').val(tec_application['airlines_id']).trigger('change');
                                    $("#type_applicant").val(tec_application['applicant_type_id']).trigger('change');

                                    $("#status").val(tec_application['status']).trigger('change');
                                    $("#reason_denied").val(tec_application['denial_id']).trigger('change');
                                    $("#other_reasons").val(tec_application['denial_msg']).trigger('change');

                                    if(tec_application['id_picture_2x2_fn'])
                                    {
                                        $('#float_id_picture_2x2_link').append('<a class="" data-toggle="modal" data-target="#Image_2x2_modal" id="2x2_file" onclick=load_2x2_picture("'+tec_application['id_picture_2x2_fn'] +'") href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');
                                    }
                                    
                                    $('#float_passport_identification_link').append('<a class="" data-toggle="modal" data-target="#passport_modal" id="passport_files" onclick="load_passport_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                                    $('#float_airline_ticket_link').append('<a class="" data-toggle="modal" data-target="#airline_ticket_modal" id="airline_ticket_files" onclick="load_airline_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                                    $('#float_additional_file_link').append('<a class="" data-toggle="modal" data-target="#additional_file_modal" id="additional_file_files" onclick="load_additional_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>'); 
                    
                                    $('#add_record').addClass('disabled');  

                                    $("#assigned_supervisor").val(tec_application['supervisor_id']).trigger('change');

                                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                                        $("#assigned_processor").val(tec_application['assign_processor_id']).trigger('change');

                                         @if(Auth::user()->isSuperAdmin())
                                            
                                            $('#delete_record').removeClass('disabled');   
                                         @endif
                                    @endif

                                    if(tec_application['status'] == 3) $('#only_denied').show();

                                    if(tec_application['status'] == 2) $('#only_approved').show();
                                    
                                    @if(Auth::user()->isRegularAdmin())  
                                         if(tec_application['generate_to_email']) is_complete(true);
                                         
                                    @endif
                                    swal.close();

                                    if($('#dupcheck').val() == "1")
                                    {
                                        const swal_continue = alert_continue(" ", "Do you want to continue?", 'btn btn-primary', 'colored-header colored-header-primary');
                                        swal_continue.then((result) => {
                                            if(result.value){
                                               dupContinue = true;
                                                $('#user_id').val("");
                                                $("#tec_id").val("");
                                                $("#application_no").val("");
                                                $('#middle_name').val("");
                                                $('#mobile_no').val("");
                                                $('#email_address').val("");
                                                $('#passport_no').val("");
                                                $("#date_application").val("");
                                                $("#date_validity").val("");
                                                $("#ticket_booking_ref_no").val("");
                                                $("#date_ticket_issued").val("");
                                                $("#country_designation").val("all").trigger("change");
                                                $("#date_flight").val("");
                                                $('#airlines_name').val("all").trigger("change");
                                                $("#reason_denied").val("");
                                                $("#other_reasons").val("");
                                                $("#assigned_processor").val("all").trigger("change");
                                                $("#assigned_supervisor").val("all").trigger("change");
                                                $("#status").val("all").trigger("change");
                                                $("#type_applicant").val("all").trigger("change");

                                            }
                                            else {
                                                dupContinue = false;
                                            }
                                        });
                                    }
                                }).catch((error) => {
                                    alert_warning(error.response.data.errors, 1500);
                                });
                            }, 1200); 
                        }
                    }); 
                }

                function is_complete(status)
                {
                
                    $('#type_applicant').prop('disabled', status);
                    // $('#id_picture_2x2').prop('disabled', status);
                    // $('#passport_identification_page').prop('disabled', status);
                    // $('#ticket_booking_ref_no_').prop('disabled', status);
                    $('#additional_file').prop('disabled', status);
                    $('#assigned_processor').prop('disabled', status);
                    $('#file_0').prop('disabled', status);
                    $('#file_1').prop('disabled', status);
                    $('#file_2').prop('disabled', status);
                    $('#file_3').prop('disabled', status);

                    @if(Auth::user()->isProcessor())
                        $('#assigned_supervisor').prop('disabled', true);
                        $('#frm_input_assigned_supervisor').show();
                    @endif

                    $('#last_name').prop('disabled', status);
                    $('#first_name').prop('disabled', status);
                    $('#middle_name').prop('disabled', status);
                    $('#passport_no').prop('disabled', status);
                    $('#mobile_no').prop('disabled', status);
                    $('#email_address').prop('disabled', status);
                    $('#ticket_booking_ref_no').prop('disabled', status);
                    $('#date_ticket_issued').prop('disabled', status);
                    $('#country_designation').prop('disabled', status);
                    $('#date_validity').prop('disabled', status);
                    $('#date_flight').prop('disabled', status);
                    $('#airlines_name').prop('disabled', status);

                    $('#status').prop('disabled', status);
                    $('#reason_denied').prop('disabled', status);
                    
                    if(status == true)
                    {
                         $('#only_approved').hide();
                        $('#only_denied').hide();
                        $('#save_btn').hide();
                    }
                    else
                    {
                        $('#save_btn').show();
                    } 
                }
            </script>
        @endsection