<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
     <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            font-family: cambria;
            font-size: 10pt;
            background: white;
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
        th {
            vertical-align: top;
        }
        td {
            padding: 0px;
            font-size: 9pt;
            vertical-align: top;
        }

        .page-header, .page-header-space {
            height: 150px;
          }

          .page-footer, .page-footer-space {
          }

          .page-header {
            display: none;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }



        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: block;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
    </style>
</head>
<body>
    <div class="page-header" style="text-align: center;">
        <img src="{{ asset('img/logo_arial2.png') }}" width="60%"/>
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer_2021.png') }}" width="100%">
    </div>

    <div>
        <div class="container-fluid">
            <div class="col-md-12">
                @php
                    $start_count = 1;

                @endphp

                @foreach($applications_chunk as $key => $val) 
                    @if($report_name == 'airlines_passenger')
                        @if(isset($status) && $status == '2')
                            <div class="page">
                                <div class="page-header-space">
                                </div>
                                    @if($key == 0)
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="font-size: 24pt;">
                                                <b>List of RTT Applications (Approved)</b> 
                                                <br>
                                            </div>
                                            @if(isset($month) && isset($year))
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                                                <br>
                                                <br>
                                            </div>
                                            @else
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></b> 
                                                <br>
                                                <br>
                                            </div>
                                            @endif

                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>RTT No</th>
                                                        <th>Name</th>
                                                        <th>Section</th> 
                                                        <th>Ticket No</th> 
                                                        <th>Date of Application</th> 
                                                        <th>Date Validity</th> 
                                                        <th>Processed By</th> 
                                                        <th>Approved By</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($val as $key1 => $val1)  
                                                        <tr>
                                                            <td style="width: 3%;">{{ $start_count ++ }}</td>
                                                            <td style="width: 6%;">{{ 'RTT-'.str_pad($val1->rtt_no, 5, '0', STR_PAD_LEFT)}}</td>
                                                            <td style="width: 18%;">{{ strtoupper($val1->full_name) }}</td>
                                                            <td style="width: 2%;">{{ $val1->sec_code }}</td>
                                                            <td style="width: 10%;">{{ $val1->ticket_no }}</td>
                                                            <td style="width: 5%;">{{ date('M d Y', strtotime($val1->date_application)) }}</td>
                                                            <td style="width: 5%;">{{ date('M d Y', strtotime($val1->date_validity)) }}</td>
                                                            <td style="width: 5%;">{{ $val1->processor_full_name }}(Processor)</td>
                                                            <td style="width: 5%;">{{ $val1->supervisor_full_name }}(Signatory)</td> 
                                                            {{--
                                                            @else
                                                                <td style="width: 5%;">{{ $val1->generate_to_email ? 'Yes' : 'No' }}</td>
                                                            @endif
                                                            --}}
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                        <br>
                                        {{ date('M d, Y H:i:s').' - '.Auth::user()->username }}
                                        </div>
                                    </div>
                            </div>
                        @elseif(isset($status) && $status == '3')
                            <div class="page">
                                <div class="page-header-space">
                                </div>
                                    @if($key == 0)
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="font-size: 24pt;">
                                                <b>List of RTT Applications (Denied)</b> 
                                                <br>
                                            </div>
                                            @if(isset($month) && isset($year))
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                                                <br>
                                                <br>
                                            </div>
                                            @endif

                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Application No</th>
                                                        <th>Name</th>
                                                        <th>Date of Application</th>
                                                        <th>Reason of Denial</th> 
                                                        <th>Completed/Processed</th>  
                                                        <th>Processed By</th> 
                                                        <th>Approved By</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($val as $key1 => $val1)  

                                                        @php
                                                            $reason_denied = $val1->denied_desc;

                                                            if($val1->denial_id == 0) $reason_denied = $val1->denial_msg; 
                                                        @endphp

                                                        <tr>
                                                            <td style="width: 3%;">{{ $start_count ++ }}</td>
                                                            <td style="width: 10%;">{{ 'APP-'.str_pad($val1->id, 5, '0', STR_PAD_LEFT)}}</td>
                                                            <td style="width: 18%;">{{ strtoupper($val1->full_name) }}</td>
                                                            <td style="width: 12%;">{{ date('M d Y', strtotime($val1->date_application)) }}</td>
                                                            <td style="width: 35%;">{{ $reason_denied }}</td>
                                                            <td style="width: 5%;">{{ $val1->generate_to_email ? 'Yes' : 'No' }}</td>
                                                            <td style="width: 5%;">{{ $val1->processor_full_name }}(Processor)</td>
                                                            <td style="width: 5%;">{{ $val1->supervisor_full_name }}(Signatory)</td> 
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                        <br>
                                        {{ date('M d, Y H:i:s').' - '.Auth::user()->username }}
                                        </div>
                                    </div>
                            </div>
                        @else
                            <div class="page">
                                <div class="page-header-space">
                                </div>
                                    @if($key == 0)
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="font-size: 24pt;">
                                                <b>List of RTT Applications</b> 
                                                <br>
                                            </div>
                                            @if(isset($month) && isset($year))
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                                                <br>
                                                <br>
                                            </div>
                                            @else
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></b> 
                                                <br>
                                                <br>
                                            </div>
                                            @endif

                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Date of Application</th>
                                                        <th>Name</th>
                                                        <th>Airline</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($val as $key1 => $val1)

                                                        @php
                                                            $status = 'undefined';

                                                            if(in_array($val1->status, array_column($application_status, 'id'))) 
                                                            {
                                                                $status_key = array_search($val1->status, array_column($application_status, 'id'));
                                                                $status = ucfirst($application_status[$status_key]['name']);
                                                            }
                                                        @endphp


                                                        <tr>
                                                            <td  style="width: 3%;">{{ $start_count ++ }}</td>
                                                            <td style="width: 15%;">{{ date('M d Y', strtotime($val1->date_application)) }}</td>
                                                            <td>{{ strtoupper($val1->full_name) }}</td>
                                                            <td  style="width: 20%;">{{ $val1->airline_name }}</td>
                                                            <td style="width: 10%;">{{ $status }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                        <br>
                                        {{ date('M d, Y H:i:s').' - '.Auth::user()->username }}
                                        </div>
                                    </div>
                            </div>
                        @endif
                    @elseif($report_name == 'summary_section_rtt')
                        <div class="page">
                                <div class="page-header-space">
                                </div>
                                    @if($key == 0)
                                        <div class="row">
                                            <div class="col-md-12 text-center" style="font-size: 24pt;">
                                                <b>Summary Report of RTT Applications</b> 
                                                <br>
                                            </div>
                                            @if(isset($month) && isset($year))
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                                                <br>
                                                <br>
                                            </div>
                                            @else
                                            <div class="col-md-12 text-center" style="font-size: 12pt;">
                                                <b>For the period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></b> 
                                                <br>
                                                <br>
                                            </div>
                                            @endif

                                        </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Code</th>
                                                        <th>Description</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $array_key_last = last($applications_chunk);
                                                        $last_val = last($array_key_last);
                                                    @endphp

                                                    @foreach($val as $key1 => $val1)  
                                                        <tr> 
                                                            <td style="width: 5%;">{{ strtoupper($val1->code) }}</td>
                                                            <td  style="width: 20%;">{{ $val1->name }}</td>
                                                            <td style="width: 10%;">{{ $val1->count }}</td>
                                                        </tr>

                                                        @if($val1->id == $last_val->id)

                                                        <tr> 
                                                            <td style="width: 5%;"><b>Total</b></td>
                                                            <td  style="width: 20%;">&nbsp;</td>
                                                            <td style="width: 10%;">{{ $total_count }}</td>
                                                        </tr>

                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                        <br>
                                        {{ date('M d, Y H:i:s').' - '.Auth::user()->username }}
                                        </div>
                                    </div>
                            </div>
                        @elseif ($report_name == 'summary_process_time')
                        
                        <div class="page">
                            <div class="page-header-space">
                            </div>
                            @if($key == 0)
                                <div class="row">
                                    <div class="col-md-12 text-center" style="font-size: 24pt;">
                                        <b>Summary of Time & Motion Report</b> 
                                        <br>
                                    </div>
                                  
                                    @if(isset($month) && isset($year))
                                    <div class="col-md-12 text-center" style="font-size: 12pt;">
                                        <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                                        <br>
                                        <br>
                                    </div>
                                    @else
                                    <div class="col-md-12 text-center" style="font-size: 12pt;">
                                        <b>For the period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></b> 
                                        <br>
                                        <br>
                                    </div>
                                    @endif
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                           
                                                <th colspan='20'> <center>EXEMPTION    </center></th>
                                            
                                            </tr>
                                            <tr>
                                            @foreach($val as $key1 => $val1)  
                                           
                                                    <th>{{ strtoupper($val1->code) }}</th>
                                            @endforeach

                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @php
                                                $array_key_last = last($applications_chunk);
                                                $last_val = last($array_key_last);
                                            @endphp
                                            <tr>
                                            @foreach($val as $key1 => $val1)  
                                           
                                                    <td>{{ strtoupper($val1->cnt) }}</td>
                                            @endforeach
                                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <br>
                                        {{ date('M d, Y H:i:s').' - '.Auth::user()->username }}
                                    </div>
                                    <div class="col-md-6">
                                        <br>
                                        Total Processed: {{ $processedTime->total_processed}}
                                        <br>
                                        Total Processing Time: {{ $processedTime->timeSum }} 
                                        <br>
                                        Average System Processing Time Per  Client: {{ $processedTime->timeAvg }} 
                                    </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                @if($applications_chunk)
                    @if($report_name == 'summary_certificate')
                        <div class="page">
                            <div class="page-header-space">
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center" style="font-size: 24pt;">
                                    <b>Issued Reduced Travel Tax Certificate Summary</b> 
                                    <br>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead style="text-align: center;">
                                            <tr>
                                                @if(isset($month))
                                                    <th colspan="3">RTT Issuance for the Month of <u>{{ date('F Y', strtotime($month.'/01/'.$year)) }}</u></th>
                                                @else
                                                    <th colspan="3">RTT Issuance for the Period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></th>
                                                @endif
                                                
                                            </tr>
                                            <tr>
                                                <th>RTT Beginning</th>
                                                <th>RTT Ending</th>
                                                <th>Total Issuance</th>
                                            </tr>
                                        </thead>
                                        <tbody style="text-align: center;">
                                            @php
                                                $array_key_last = last($applications_chunk);
                                                $array_key_first = $applications_chunk[0];

                                                $first = $array_key_first->rtt_no;
                                                $last = $array_key_last->rtt_no;
                                                $last_rtt_no = 'RTT - '.str_pad($last, 6 ,"0", STR_PAD_LEFT);
                                                $first_rtt_no = 'RTT - '.str_pad($first, 6 ,"0", STR_PAD_LEFT);
                                            @endphp
                                            <tr>
                                                <td>
                                                    {{ $first_rtt_no }}
                                                </td>
                                                <td>
                                                    {{ $last_rtt_no }}
                                                </td>
                                                <td>
                                                    {{ count($applications_chunk) }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <br>
                                {{ date('M d, Y H:i:s').' - '.Auth::user()->username }}
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>

    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>

</html>