                  <div class="fulltax">
                    <div class="row">
                        <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 text-right mt-1 p-0">
                            <label class="control-label" for="trans_date">Trans. Date</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                            <input type="date" id="trans_date" class="form-control form-control-xs datetimepicker" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                    
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 p-1">
                            <label class="control-label" for="last_name">Last Name</label>
                            <input type="text" id="last_name" name="last_name"class="form-control form-control-xs ">
                            <!-- @error('last_name') <div class="invalid-feedback">Last Name is required</div>  -->
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <label class="control-label" for="last_name">First Name</label>
                            <input type="text" id="first_name" name="first_name"  class="form-control form-control-xs  ">
                            <!-- @error('first_name') <div class="invalid-feedback">First Name is required</div>  -->
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-1">
                            <label class="control-label" for="last_name">M.I</label>
                            <input type="text" id="middle_name" name="middle_name" class="form-control form-control-xs">
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-1">
                            <label class="control-label" for="last_name">Ext.Name</label>
                            <input type="text" id="ext_name" name="ext_name" class="form-control form-control-xs">
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-4 col-lg-4 col-xl-4 text-right mt-1 p-0">
                            <label class="control-label" for="trans_date">Trans. No</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                            <input type="text" id="trans_date" class="form-control form-control-xs" readonly>
                        </div>
                    </div>
                    <br>
                   <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Class</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <select class="select2 select2-xs " 
                                    name="class_type" 
                                    id="class_type">  
                                    <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                    <option value="1">{{ __('page.first_class') }}</option>
                                    <option value="2">{{ __('page.business_class') }}</option>

                            </select>
                            <!-- @error('class_type') <div class="invalid-feedback">Class is required</div>  -->
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2"> 
                            <input type="text" class="form-control form-control-lg text-danger text-bold" id="class_amt" name="class_amt" readonly>
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right mt-0 p-1">
                        <!-- <label class="control-label" for="last_name">Mode of Payment</label> -->
                        </div>
                        <div class="col-4 col-xs-4 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                            <!-- <select class="select2 select2-xs  " 
                                    name="mop" 
                                    id="mop">   
                                    <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                    <option value="1">{{ __('page.peso') }}</option>
                                    <option value="2">{{ __('page.dollars') }}</option>
                                    <option value="3">{{ __('page.master_card') }}</option>
                                    <option value="4">{{ __('page.visa') }}</option>

                            </select>
                            @error('mop') <div class="invalid-feedback">Mode of Payment is required</div>  -->
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Type</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <select class="select2 select2-xs " 
                                    name="fulltax_type" 
                                    id="fulltax_type">   
                                    <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                    <option value="1">Type 1</option>
                                    <option value="2">Type 2</option>
                                    <option value="3">Type 3</option>
                                    <option value="4">Type 4</option>
                                    <option value="5">Type 5</option>

                            </select>
                          <!-- @error('fulltax_type') <div class="invalid-feedback">Type is required</div>   -->
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Passport No.</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="passport" name="passport" class="form-control form-control-xs ">
                        <!-- @error('passport') <div class="invalid-feedback">Passport No. is required</div>  -->
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Ticket No.</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="ticket" name="ticket" class="form-control form-control-xs  ">
                        <!-- @error('ticket') <div class="invalid-feedback">Ticket No. is required</div>  -->

                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Departure Date</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="departure" name="departure" class="form-control form-control-xs">
                        <!-- @error('departure') <div class="invalid-feedback">Departure Date is required</div>  -->
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Destination</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <select class="select2 select2-xs " 
                                    name="ft_destination" 
                                    id="ft_destination">   
                                    <option value="0" disabled selected>{{ __('page.please_select') }}</option> 
                                    @if($countries)
                                        @foreach($countries as $key => $val)
                                        <option value="{{$val->id}}">{{ $val->name }}</option>
                                        @endforeach
                                    @endif
                            </select>
                        <!-- @error('ft_destination') <div class="invalid-feedback">Destination is required</div>  -->

                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>

                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <!-- <label class="control-label" for="last_name">OR No.</label> -->
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <!-- <input type="text" id="or_no" name="or_no" class="form-control form-control-xs ">
                        @error('or_no') <div class="invalid-feedback">OR No. is required</div>   -->
                        <!-- <button type="button" class="btn btn-success " id="shw" data-modal="modal1">show modal</button> -->
                        </div>
                    </div>
                   
            </div>
            <div class="modal-container  custom-width modal-effect-11" id="modal1" tabindex="-1" role="dialog">
        <div class="modal-content modal-dialog ">
            <div class=" modal-header  modal-header-colored">
            <h4 class="modal-title"> <b> Payment Information</b></h4>
            <!-- <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button> -->
            </div>
            <div class="modal-body">
            <form method="" action="javascript:submit();">
                    {{ csrf_field() }}
                    
            <div class="row">
                <div class="col-6">
                    Passenger Name:
                </div>
                <div class="col-4">
                   <label id="lbl_name"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Accomodation Flight:
                </div>
                <div class="col-4">
                <label id="lbl_amt"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Passport Number:
                </div>
                <div class="col-4">
                <label id="lbl_passport"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Ticket No./Confirmation No.:
                </div>
                <div class="col-4">
                <label id="lbl_ticket"></label>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    Departure Date:
                </div>
                <div class="col-4">
                <label id="lbl_departure"></label>
                </div>
            </div>
            <br><br><br>
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4 text-center">
                  Total Amount to Pay
                  <input type="text" id="amt_total" class="form-control text-center b-100 " readonly> 
                </div>
                <div class="col-4"></div>

            </div>
            <br>
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4 text-center">
                  <button type="button" class="btn btn-success " id="pay" >Pay Now</button>
                </div>
                <div class="col-4"></div>

            </div>
          
            </form>
            </div>
        </div>  
    </div>
    <div class="modal-overlay"></div>

@section('scripts')
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
<script type="text/javascript"> 

    var f_class = "2700.00";
    var e_class = "1620.00";
  $(document).ready(function () {
    App.formElements();
    $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
      	classAddAfterOpen: 'modal-show'
        });

    $('#class_type').change(function()
    {
        if($(this).val() == "1") $('#class_amt').val(f_class);
        else if($(this).val() == "2") $('#class_amt').val(e_class);

    });

    $('#shw').click(function()
    {
        $('#modal1').niftyModal('show');
       
    });
    $('#pay').click(function()
    {
        alert('Under Maintenance..');
    });
    $('#save_btn').click(function()
    {
       
        const formData = new FormData();
        formData.append('last_name',$('#last_name').val());
        formData.append('first_name',$('#first_name').val());
        formData.append('middle_name',$('#middle_name').val());
        formData.append('ext_name',$('#ext_name').val());
        formData.append('class_type',$('#class_type').val());
        formData.append('class_amt',$('#class_amt').val());
        formData.append('fulltax_type',$('#fulltax_type').val());
        formData.append('passport',$('#passport').val());
        formData.append('ticket',$('#ticket').val());
        formData.append('departure',$('#departure').val());
        formData.append('ft_destination',$('#ft_destination').val());

        axios.post('{{route("fulltax.store")}}',formData,{
         headers: {
                    'Content-Type': 'multipart/form-data'
                  }
        })
        .then(response => { 
            success = "Saved Successfully!";
            const swal_success = alert_success(success, 1500);
            swal_success.then((value) => {
                var name =response.data.details.first_name + ' ' + response.data.details.middle_name + ' ' + response.data.details.last_name;
                var amount = parseFloat(response.data.details.fulltax_amount,10);
                var passport = response.data.details.passport_no;
                var ticket = response.data.details.ticket_no;
                var departure = response.data.details.departure_date;

                $('#lbl_name').text(name);
                $('#lbl_amt').text(amount);
                $('#lbl_passport').text(passport);
                $('#lbl_ticket').text(ticket);
                $('#lbl_departure').text(departure);
                $('#lbl_departure').text(departure);
                $('#amt_total').val("P"+amount.toLocaleString());






                $('#modal1').niftyModal('show');
            });
        })
        .catch(error => {
            console.log(error.response)
        });







    });
  });
</script>
@endsection