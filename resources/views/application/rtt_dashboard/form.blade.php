      
        @php
            $months = array(
                ['id' => "01", 'month' => 'January'],
                ['id' => "02", 'month' => 'February'],
                ['id' => "03", 'month' => 'March'],
                ['id' => "04", 'month' => 'April'],
                ['id' => "05", 'month' => 'May'],
                ['id' => "06", 'month' => 'June'],
                ['id' => "07", 'month' => 'July'],
                ['id' => "08", 'month' => 'August'],
                ['id' => "09", 'month' => 'September'],
                ['id' => "10", 'month' => 'October'],
                ['id' => "11", 'month' => 'November'],
                ['id' => "12", 'month' => 'Decemeber']
            );

            $year = date('Y');
        @endphp

        <div class="form-group row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <label class="control-label">Filter By Month & Year</label> 
                <select class="select2 select2-xs" id="filter_month">
                    <option value="">{{ __('page.please_select')  }}</option>
                    @foreach($months as $key => $val)
                        <option value="{{ $val['id'] }}">{{ $val['month'] }}</option>
                    @endforeach
                </select>      
            </div>
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <label class="control-label">&nbsp;</label> 
                <select class="select2 select2-xs" id="filter_year">
                    <option value="">{{ __('page.please_select')  }}</option>
                    @for($i = 2020; $i <= $year; $i++)
                        <option value="{{ $year }}">{{ $year }}</option>
                    @endfor
                </select>      
            </div>

        </div>

        <div class="form-group row">
            <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;{{ __('page.filter') }}</a>
                <a href="{{ url($application_url).'/'.$option }}" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;{{ __('page.cancel') }}</a>
            </div>
        </div>

        <hr />

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-3 col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="widget mb-1">
                            <div class="widget-head mb-0">
                                <div class="tools">
                                    <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                </div>
                                <div class="title">Awaiting Documents</div>
                            </div>
                        </div>

                        <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark1"></div>
                            <div class="data-info">
                                <div class="desc">No. of Awaiting Documents</div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="{{ $no_awaiting_applicant }}" id="no_awating_documents">0</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-3 col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="widget mb-1">
                            <div class="widget-head mb-0">
                                <div class="tools">
                                    <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                </div>
                                <div class="title">Approved Application</div>
                            </div>
                        </div>
                
                        <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3"></div>
                            <div class="data-info">
                                <div class="desc">No. of Approved Application</div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="{{ $no_approved_applicant }}" id="no_approved_documents">0</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-3 col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="widget mb-1">
                            <div class="widget-head mb-0">
                                <div class="tools">
                                    <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                </div>
                                <div class="title">Denied Application</div>
                            </div>
                        </div>
                
                        <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark4"></div>
                            <div class="data-info">
                                <div class="desc">No. of Denied Application/s</div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="{{ $no_denied_applicant }}" id="no_denied_documents">0</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-3 col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="widget mb-1">
                            <div class="widget-head mb-0">
                                <div class="tools">
                                    <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                </div>
                                <div class="title">Total Applications</div>
                            </div>
                        </div>
                
                        <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark2"></div>
                            <div class="data-info">
                                <div class="desc">No. of RTT Applications</div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number" data-toggle="counter" data-end="{{ $no_application }}" id="no_total_documents">0</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-lg-6">
                        <div class="widget be-loading">
                            <div class="widget-head">
                                <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync toggle-loading"></span><span class="icon mdi mdi-close"></span></div>
                                <div class="title">Countries</div>
                            </div>
                            <div class="widget-chart-container">
                                <div class="widget-chart-info mb-4">
                                    <div class="indicator indicator-positive float-right"><span class="icon mdi mdi-chevron-up"></span><span class="number">0%</span></div>
                                    <div class="counter counter-inline">
                                        <div class="value">0</div>
                                        <div class="desc">Impressions</div>
                                    </div>
                                </div>
                                <div id="map-widget" style="height: 265px;"></div>
                            </div>
                            <div class="be-spinner">
                                <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                    <circle class="circle" fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="widget">
                            <canvas id="donut-chart-plantilla" height="200%"></canvas> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>