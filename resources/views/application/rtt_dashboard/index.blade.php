@extends('layouts.master-auth')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jqvmap/jqvmap.min.css') }}"/>
@endsection

@section('content')
    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin() || Auth::user()->isProcessor())

    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'cancel_url' => $application_url.'/'.$option,
        ];

    @endphp

    @include('others.main_content', $data)
    @endif
@endsection

@section('scripts')
   
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-form-wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/chartjs/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-charts-chartjs.js')}}" type="text/javascript"></script>

    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/jquery.flot.pie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/jquery.flot.time.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/plugins/curvedLines.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/plugins/curvedLines.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-flot/plugins/jquery.flot.tooltip.js') }}" type="text/javascript"></script>

    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jqvmap/jquery.vmap.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>

    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/chartjs/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-charts-chartjs.js')}}" type="text/javascript"></script>
     <script src="{{ asset('beagle-v1.7.1/src/js/app-dashboard.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            App.init();
            App.formElements();
            App.wizard();
           
            $('.card-body').addClass('card-body-contrast');
            $('.card-header').removeClass('card-header-divider');

            @php    
                $selected_month = date('m');
                if(isset($filter_month)) $selected_month = $filter_month;

                $selected_year = date('Y');
                if(isset($filter_year)) $selected_year = $filter_year;
            @endphp

            $('#filter_month').val('{{ $selected_month  }}').trigger('change');
            $('#filter_year').val('{{ $selected_year }}').trigger('change');

            donutChartPlantilla({{ $no_awaiting_applicant }}, {{ $no_approved_applicant }}, {{$no_denied_applicant}})

            App.dashboard();
            App.ChartJs();
        });

    
        $('#filter_btn').click(function(){
            var filter_month = $('#filter_month').val();
            var filter_year = $('#filter_year').val();

            window.location.href = "{{ $application_url.'/'.$option }}?filter_month=" + filter_month + '&filter_year=' + filter_year;
        });

        function donutChartPlantilla(no_awaiting_applicant, no_approved_applicant, no_denied_applicant)
        {
            var color1 = App.color.primary;
            var color2 = App.color.warning;
            var color3 = App.color.danger;
            var color4 = App.color.warning;

            var ctx = document.getElementById("donut-chart-plantilla");
                
            var data = {
                labels: [
                "Awaiting Documents",
                "Approved",
                "Denied"
                ],
                datasets: [
                {
                    data: [no_awaiting_applicant, no_approved_applicant, no_denied_applicant],
                    backgroundColor: [
                        color1,
                        color2,
                        color3
                    ],
                    hoverBackgroundColor: [
                        color1,
                        color2,
                        color3
                    ]
                }]
            };

            var pie = new Chart(ctx, {
            type: 'doughnut',
            data: data
          });
        }

        // add an action script from here

    </script>
@endsection