<div class="row">
          <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <h5>Country</h5>
                            <select class="form-control form-control-sm" 
                                    name="name" 
                                    id="name">   
                                    <option value="null">{{ __('page.please_select') }}</option>
                                    @foreach($countries as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                            </select> 
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 text-right">
                           <button type="button" class="btn btn-primary" id="add_new"><i class="mdi mdi-plus"></i> Upload New Record</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
              <div class="card card-table">
                <div class="card-header">Residency
                  <div class="tools dropdown"><span class="icon mdi mdi-download"></span><a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"><span class="icon mdi mdi-more-vert"></span></a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a>
                      <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Separated link</a>
                      </div>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                     <tr>
                       <td></td>
                        <td>Date</td>
                        <td>Country</td>
                        <td> </td>

                     </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
      App.init();
      App.dataTables();
      loadDatatable();     
      $('#save_btn').css('display','none');
      $('#cancel_btn').css('display','none');

      $('#add_new').click(function()
      { 
        location.href="{{url('application/add_residency')}}";
      });
      $('#name').change(function()
      {
        loadDatatable($(this).val());
      })
    });

    function loadDatatable(id="null")
        {

            @php
            $columns = array(['data' => 'action', 'sortable' => false],['data' => 'created_at', 'sortable' => false], ['data' => 'name','name'=>'countries.name'],['data' => 'file']);
            @endphp
            load_datables('#table4', "{!! $based_url.'/residency/datatables/' !!}"+id ,{!! json_encode($columns) !!}, null);

        }

   
  </script>
@endsection
