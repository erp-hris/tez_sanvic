
<div class="row">
    <div class="col-lg-12">
        <div class=" row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h5>Country</h5>
                <select class="form-control form-control-sm " 
                        name="name" 
                        id="country">   
                        <option value="">{{ __('page.please_select') }}</option>
                        @foreach($countries as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                        @endforeach
                </select> 
               <div class="invalid-feedback d-none" id="inv_cntry">Country is required</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h5>Upload File</h5>
                <input type= "file" id="file"class="form-control form-control-sm">
            </div>
        </div>
        <div class=" row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h5>Remarks</h5>
                <input type="text" id="remarks" class="form-control form-control-sm">
            </div>
        </div>
    </div>
</div>

        
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
     
        $('#save_btn').click(function()
        {
            var country_id = $('#country').val();
            var file_name = $('#file').val();
            var remarks = $('#remarks').val();
            if(country_id == "") 
            {
                $('#inv_cntry').removeClass('d-none');
                $('#country').addClass('is-invalid');
                return;
            }
            title = "Upload";
            text = "Are you sure you want to upload this data?"
            success = "Upload Successfully!";
            color = "colored-header colored-header-primary";
            button = "btn btn-primary";
            const swal_continue = alert_continue(title, text,button, color);
            swal_continue.then((result) => {
                if(result.value){
                    const formData = new FormData();
                    formData.append('country_id',country_id);
                    var file  = document.querySelector('#file');
                    formData.append('file_name', file.files[0]);
                    formData.append('remarks', remarks);
                    console.log(formData);
                    axios.post('add_residency/save',formData,{
                        headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                   
                  })
                  .then(function (response) {
                    const swal_success = alert_success(success, 1500);
                                swal_success.then((response) => {

                                location.href= "{{url('application/residency')}}";
                                console.log(response);

                                });
                    
                  })  
                  .catch((error) => {
                      const errors = error.response.data.errors;

                      if(typeof(errors) == 'string')
                      {
                          alert_warning(errors);
                      }
                      else
                      {
                          const firstItem = Object.keys(errors)[0];
                          const firstItemDOM = document.getElementById(firstItem);
                          const firstErrorMessage = errors[firstItem][0];

                          firstItemDOM.scrollIntoView();

                          alert_warning("{{ __('page.check_inputs') }}", 1500);

                          showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                      }
                    });  
                }

            });

        });
    });

  
   
  </script>
@endsection