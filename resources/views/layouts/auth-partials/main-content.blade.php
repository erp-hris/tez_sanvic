
<div class="be-content docs">
	@yield('page_header')
    <div class="main-content container-fluid" style="background-color: #eeeeee00 !important">
    	@include('layouts.auth-partials.flash-message')
        @yield('content')
    </div>
</div>


