	
	<link rel="icon" href="{{ asset('img/tieza-logo.png') }}" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}"/>
   	<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
       <!-- <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals.bootstrap.css') }}"/> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}" />
    <script type="text/javascript" src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('beagle-v1.7.1/src/assets/lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
   