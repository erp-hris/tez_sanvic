<!-- Left Sidebar -->
<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <a href="#" class="left-sidebar-toggle">{{ __('page.'.$module) }}</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements ">
                        <li class="divider ">{{ __('page.menu') }}</li>
                        @php
                            $file_setup = [
                                'selected_module' => $module,
                                'menu_name' => 'file_setup',
                                'url' => '#', 
                                'icon' => 'icon mdi mdi-storage',
                                'is_parent' => 'yes', 
                                'sub_menu' => array(
                                    ['url' => '/file_setup/files', 'menu' => 'files'],
                                )
                            ];


                            $business_permit = [
                                'selected_module' => $module,
                                'menu_name' => 'business_permit',
                                'url' => '#', 
                                'icon' => 'icon mdi mdi-balance',
                                'is_parent' => 'yes', 
                                'sub_menu' => array(
                                    ['url' => '/business_permit/business_permit_application_form', 'menu' => 'business_permit_application_form'],
                                    ['url' => '/business_permit/business_permit_attachment', 'menu' => 'business_permit_attachment'],
                                )
                            ];

                            $location_permit = [
                                'selected_module' => $module,
                                'menu_name' => 'location_permit',
                                'url' => '#', 
                                'icon' => 'icon mdi mdi-my-location',
                                'is_parent' => 'yes', 
                                'sub_menu' => array(
                                    ['url' => '/location_permit/location_permit_application_form', 'menu' => 'location_permit_application_form'],
                                    ['url' => '/location_permit/location_permit_attachment', 'menu' => 'location_permit_attachment'],
                                )
                            ];

                            $building_permit = [
                                'selected_module' => $module,
                                'menu_name' => 'building_permit',
                                'url' => '#', 
                                'icon' => 'icon mdi mdi-city',
                                'is_parent' => 'yes', 
                                'sub_menu' => array(
                                    ['url' => '/building_permit/building_permit_application_form', 'menu' => 'building_permit_application_form'],
                                    ['url' => '/building_permit/building_permit_attachment', 'menu' => 'building_permit_attachment'],
                                    ['url' => '#', 'menu' => 'architectural_permit'],
                                    ['url' => '#', 'menu' => 'structural_permit'],
                                    ['url' => '#', 'menu' => 'electrical_permit'],
                                    ['url' => '#', 'menu' => 'mechanical_permit'],
                                    ['url' => '#', 'menu' => 'electronics_permit'],
                                    ['url' => '#', 'menu' => 'sanitary_permit'],
                                )
                            ];

                            $occupancy_permit = [
                                'selected_module' => $module,
                                'menu_name' => 'occupancy_permit',
                                'url' => '#', 
                                'icon' => 'icon mdi mdi-dns',
                                'is_parent' => 'yes', 
                                'sub_menu' => array(
                                    ['url' => '/occupancy_permit/occupancy_permit_application_form', 'menu' => 'occupancy_permit_application_form'],
                                    ['url' => '/occupancy_permit/occupancy_permit_attachment', 'menu' => 'occupancy_permit_attachment'],
                                )
                            ];


                            $report = [
                                'selected_module' => $module,
                                'menu_name' => 'report',
                                'url' => 'report', 
                                'icon' => 'icon mdi mdi-file',
                            ];
                            


                            $include_system_config = ['selected_module' => $module,
                                'menu_name' => 'system_config',
                                'url' => '#', 
                                'icon' => 'icon mdi mdi-settings',
                                'is_parent' => 'yes', 
                                'sub_menu' => array(
                                    ['url' => '/system_config/user_account', 'menu' => 'user_account'],
                                    ['url' => '/system_config/registration_approval', 'menu' => 'registration_approval'],
                                    ['url' => '/system_config/signatories', 'menu' => 'signatories'],
                                )];
                        @endphp

                        
                        @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'home', 'url' => '/', 'icon' => 'icon mdi mdi-home'])
                        @if(Auth::user()->level == 1)
                            @include('others.sidebar_li', $business_permit)
                            @include('others.sidebar_li', $location_permit)
                            @include('others.sidebar_li', $building_permit)
                        @else
                           
                            @include('others.sidebar_li', $file_setup)
                            @include('others.sidebar_li', $business_permit)
                            @include('others.sidebar_li', $location_permit)
                            @include('others.sidebar_li', $building_permit)
                            @include('others.sidebar_li', $report)
                        @endif

                        
                        <!-- @include('others.sidebar_li', $occupancy_permit)
                        @include('others.sidebar_li', $include_system_config) -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>