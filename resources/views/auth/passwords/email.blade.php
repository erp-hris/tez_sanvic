@extends('layouts.master-guest')

@section('css')
    <style type="text/css">
        .event-background{
            background: url("../img/bg-triangle-0.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center text-light h-25"><h2 class="mb-4">Online TEC Processing System</h2></div>
        </div>

        <div class="row">
            <div class="col-xl-6 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
                <img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 img-fluid" style="height: 35em;">
            </div>
            <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="splash-container">
                    @if (session('status'))
                        <div class="row">
                            <div class="col-12">
                                <div role="alert" class="alert alert-contrast alert-success alert-dismissible">
                                    <div class="icon"><span class="mdi mdi-check"></span></div>
                                    <div class="message">
                                        <button type="button" data-dismiss="alert" aria-label="Close" class="close">
                                            <span aria-hidden="true" class="mdi mdi-close"></span>
                                        </button>
                                        <strong>Alert!</strong> {{ session('status') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-header">
                            <span class="splash-title">Forgot your password?</span>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                <p>Don't worry, we'll send you an email to reset your password.</p>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="text" placeholder="Email Address" autocomplete="off" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                    @if($errors->has('email'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('email') }}</li>
                                        </ul>
                                     @endif
                                </div>

                                <div class="form-group login-submit">
                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl"><span class="mdi mdi-mail-send"></span>&nbsp;Send Password Reset Link</button>
                                </div>

                                <hr />

                                <p class="pt-2 pb-0">Don't remember your email? Please contact <a href="#">traveltax.helpdesk@tieza.gov.ph</a></p>
                                
                                <p class="pt-2 pb-0">Return to <a href="{{ url('/login') }}">Home</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection`