@extends('layouts.master-guest')

@section('modal')

<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/dist/html/assets/css/app.css') }}"/>

@endsection

@section('css')
  
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.css') }}"/>

<style type="text/css">
        .event-background{
            background: url("img/bg-tiezadarker.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
        .card 
        {
            box-shadow: 0 0 10px 0 rgba(100, 100, 100, 0.75);
        }
    </style>
@endsection

@section('content')
@include('others.form_request', ['frm_method' => 'POST', 'frm_action' => route('create.store'), 'frm_id' => 'save_form'])
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center text-dark h-25"><h2 class="mb-4">Online Permitting System<br>San Vicente Flagship Tourism Enterprise Zone</h2></div>
        </div>

        <div class="row">
            <div class="col-xl-6 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
                <img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 img-fluid" style="height: 35em;">
            </div>
            <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="splash-container card-border-color-primary">
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-header">
                            <span class="splash-title">Please enter your user information.</span>
                        </div>
                        <div class="card-body" style="padding-left:20px; padding-right: 20px;">
                            <form method="POST" action="{{ route('login') }}" data-parsley-validate="" novalidate="">
                            {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input id="username" type="text" placeholder="Username" autocomplete="off" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                                    @if($errors->has('username'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('username') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                                    @if($errors->has('password'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('password') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class=" row login-tools">
                                    <div class="col-6 login-remember">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" {{ old('remember') ? 'checked' : '' }} id="checkbox1">
                                            <label class="custom-control-label" for="checkbox1">Remember Me</label>
                                        </div>
                                    </div>
                                    <div class="col-6 login-forgot-password">
                                        <p><a href="{{ url('password/reset') }}">Forgot Password?</a></p>
                                    </div>
                                </div>
                                <div class="row form-group login-submit">
                                    <div class="col-6 ">
                                        <a data-dismiss="modal"  href= "#" id="create" class="btn btn-secondary btn-xl md-trigger" data-modal="modal1"><span class="mdi mdi-account-add"></span>&nbsp;Register</a>
                                    </div>
                                    <div class="col-6">
                                        <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl"><span class="mdi mdi-sign-in"></span>&nbsp;Sign me in</button>
                                    </div>
                                </div>
                                <hr />
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-container colored-header colored-header-primary custom-width modal-effect-11" id="modal1" tabindex="-1" role="dialog">
        <div class="modal-content modal-dialog ">
            <div class=" modal-header  modal-header-colored">
            <h3 class="modal-title"><span class="mdi mdi-account-add"></span> Registration</h3>
            <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
            </div>
            <div class="modal-body">
            <form method="" action="javascript:submit();">
                            {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    
                                    <label class="control-label is-invalid">Email address</label>
                                    <div class="input-group input-group-md">
                                        <div class="input-group-prepend"><span class="input-group-text mdi mdi-email"></span></div>
                                        <input id="email" type="email" parsley-trigger="change" placeholder="Username@example.com" autocomplete="off" class="form-control" name="email" value="{{ old('email') }}" >
                                        <div class="invalid-feedback">Please provide email address.</div>
                                    </div>
                                 
                                </div>
                                
                                <div class=" row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-6">
                                        <label class="control-label is-invalid">Password</label>
                                        <div class="input-group input-group-md">
                                            <div class="input-group-prepend"><span class="input-group-text mdi mdi-lock"></span></div>
                                            <input id="cpassword" type="password" placeholder="Enter Password" autocomplete="off" class="form-control" name="cpassword" value="{{ old('password') }}"  >
                                            <div class="invalid-feedback">Please provide password.</div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label class="control-label">Confirm Password</label>
                                        <div class="input-group input-group-md ">
                                        <div class="input-group-prepend"><span class="input-group-text mdi mdi-lock"></span></div>
                                        <input id="confirm_password" type="password" placeholder="Enter Confirm Password" autocomplete="off" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}"  >
                                        <div class="invalid-feedback">Please provide confirm password.</div>
                                    </div>
                                    </div>

                                </div>
                                <div id="recaptcha" class="g-recaptcha form-group" data-sitekey="" ></div>
                                
                                <div class="row form-group">
                                    <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label class="custom-control custom-checkbox custom-control-inline">
                                            <input class="custom-control-input form-control" id="agreement" type="checkbox" ><span class="custom-control-label custom-control-color">Check here to indicate that you have read and agree to the <a href="#">Terms & Conditions</a></span>
                                        </label>
                                    </div>
                                    <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                                        <button type="submit" class="btn btn-block btn-lg btn-primary">Submit</button>
                                    </div>
                               </div>
            </form>
            </div>
        </div>  
    </div>
    <div class="modal-overlay"></div>
@endsection

@section('scripts')
@include('layouts.auth-partials.scripts')
@yield('scripts')
@yield('additional-scripts')
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script>
   
        <script type="text/javascript">
          $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
      	classAddAfterOpen: 'modal-show'
        });
        var widgetId1;
        var onloadCallback = function() {
        widgetId1= grecaptcha.render('recaptcha', {
          'sitekey' : '6LcMN0AdAAAAAOlBgliBD4HegUAFhsD7DaOXNNCI'
        });
    }

        function submit()
        {
            var email = $('#email').val();
            var password = $('#cpassword').val();
            var confirm_password = $('#confirm_password').val();
            var agreement = $('#agreement').is(":checked");
            var captcha = grecaptcha.getResponse(widgetId1);
            if(email == "") $( "#email" ).addClass( "is-invalid" );
            if(password == "") $( "#cpassword" ).addClass( "is-invalid" );
            if(confirm_password == "") $( "#confirm_password" ).addClass( "is-invalid" );
            if(!agreement) $( "#agreement" ).addClass( "is-invalid" );
         
            if(email && password && confirm_password && agreement) 
            {
                $('#email').removeClass("is-invalid");
                $('#cpassword').removeClass("is-invalid");
                $('#confirm_password').removeClass("is-invalid");
                $('#agreement').removeClass("is-invalid");
              
                if(password != confirm_password)
                {
                    Swal.fire({
                    type: 'error',
                    title: '',
                    text: 'Password does not match.',
                    confirmButtonText: `OK`
                    });
                }
                else if(captcha == "") 
                {
                    Swal.fire({
                    type: 'error',
                    title: '',
                    text: 'Please check "Im not a robot"',
                    confirmButtonText: `OK`
                    });
                }
                else {

                    const swal_continue = alert_continue("Submit Registration", "Are you sure you want to submit this?");
                    swal_continue.then((result) => {
                       
                        if(result.value)
                        { 
                            var frm, text, title, success,  formData;
                            frm = document.querySelector('#save_form');
                            formData = new FormData();

                            formData.append("email",email);
                            formData.append("password", password);

                            axios.post(frm.action, formData, {
                                headers: {
                                    'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                success = "{{ __('page.submit_successfully', ['attribute' => 'Registration']) }}";
                                const swal_success = alert_success(success, 2000);
                                    swal_success.then((value) => {
                                     clearInputs();
                                     grecaptcha.reset();
                                     $('#modal1 .close').click();
                                    });

                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;
                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    const firstItem = Object.keys(errors)[0];

                                    const split_firstItem = firstItem.split('.');
                                    const firstItemSplit = split_firstItem[0];

                                    const firstItemDOM = document.getElementById(firstItemSplit);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();

                                    alert_warning("{{ __('page.check_inputs') }}", 1500);

                                    showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['applicant_name', 'country_designation', 'airlines_name', 'type_applicant', 'assigned_processor', 'status', 'reason_denied', 'assigned_supervisor']);
                                }
                            });
                        
                        }
                    });
                }
            }
           
        }

       
    </script>
@endsection