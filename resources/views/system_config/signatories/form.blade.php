
<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label text-red">{{ __('page.required_fields') }}</label>
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <label class="control-label">{{ __('page.users')}}<span class="text-red">&nbsp;*</span></label>
                      <br>
                            <select class="select2 select2-xs" 
                                    name="name" 
                                    id="name">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($list_users as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->full_name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-applicant_name"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.position') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="position" id="position" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        
        <input type="hidden" name="_id" id="_id" />
@section('additional-scripts')
  <script type="text/javascript">
    $(document).ready(function () {
     
      
    });

    function approve_decline(id,type)
    {
      var text,success,color,button,title;
      if(type == "1") {
        title = "Approve";
        text = "Are you sure you want to approve this request?";
        success = "Approved Successfully!";
        color = "colored-header colored-header-primary"
        button = "btn btn-primary";
      }
      else if(type == "2"){
        title = "Decline";
        text = "Are you sure you want to decline this request?"
        success = "Declined Successfully!";
        color = "colored-header colored-header-danger";
        button = "btn btn-danger";
      } 

      const swal_continue = alert_continue(title, text,button, color);
          swal_continue.then((result) => {
            
              if(result.value){

                axios({
                    method: 'post',
                    url: "registration_approval/update",
                    data: {
                      id: id,
                      type: type
                    }
                  })
                  .then(function (response) {
                    const swal_success = alert_success(success, 1500);
                                swal_success.then((response) => {

                                  loadDatatable();

                                });
                    
                  })  
                  .catch((error) => {
                      const errors = error.response.data.errors;

                      if(typeof(errors) == 'string')
                      {
                          alert_warning(errors);
                      }
                      else
                      {
                          const firstItem = Object.keys(errors)[0];
                          const firstItemDOM = document.getElementById(firstItem);
                          const firstErrorMessage = errors[firstItem][0];

                          firstItemDOM.scrollIntoView();

                          alert_warning("{{ __('page.check_inputs') }}", 1500);

                          showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                      }
                    });  
              }
            });
    }
    
    $("#add_record").click(function() {
        var title, text;

        title = "{{ __('page.add_'.$option) }}";
        text = "{{ __('page.continue_this') }}";

        const swal_continue = alert_continue(title, text);
        swal_continue.then((result) => {
            if(result.value){
                window.location.href="{{$cancel_url}}";
            }
        });
    });
    $("#save_btn").click(function() {
          var frm, text, title, success, _id, formData;

          _id = $('#_id').val();
          frm = document.querySelector('#save_form');

          formData = new FormData();

          if(_id)
          {
              formData.append('_id', _id);
              title = "{{ __('page.edit_'.$option) }}";
              text = "{{ __('page.update_this') }}";
              success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
          }
          else
          {
              title = "{{ __('page.add_'.$option) }}";
              text = "{{ __('page.add_this') }}";
              success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
          }

          const swal_continue = alert_continue(title, text);
          swal_continue.then((result) => {
              clearErrors();
              if(result.value){
                  @foreach($default_inputs as $key => $val)
                      formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                  @endforeach
                  console.log(formData);
                  axios.post(frm.action, formData)
                  .then((response) => {
                      const swal_success = alert_success(success, 1500);
                      swal_success.then((value) => {
                          clearInputs();
                          if(_id)
                          {
                              select_account(_id);
                              $('#filter_btn').click();
                          }
                          else
                          {
                              const swal_continue = alert_continue("{{ __('page.add_'.$option) }}", "Do you want to add another {{ trans('page.'.$option) }}?");
                              swal_continue.then((result) => {
                                  if(!result.value) {
                                      window.location.href = "{{ $cancel_url }}";
                                  }
                              });
                          }
                      });
                  })
                  .catch((error) => {
                      const errors = error.response.data.errors;

                      if(typeof(errors) == 'string')
                      {
                          alert_warning(errors);
                      }
                      else
                      {
                          const firstItem = Object.keys(errors)[0];
                          const firstItemDOM = document.getElementById(firstItem);
                          const firstErrorMessage = errors[firstItem][0];

                          firstItemDOM.scrollIntoView();

                          alert_warning("{{ __('page.check_inputs') }}", 1500);

                          showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                      }
                  });   
              }
          })
      });
      function select_account(user_id)
      {
          clearErrors();

          axios.get("{{ url('/signatories_list/json') }}", {
                params: {
                    user_id : user_id,
                }
            })
          .then(function(response){
              const result = response.data.signatories;

               $("#name").val(result['name']).trigger('change');
                $("#position").val(result['position']);
                $("#_id").val(result['id']);

               

                $('#delete_record').removeClass('disabled');

          }).catch((error) => {
                alert_warning(error.response.data.errors, 1500);
          });
      }
   
  </script>
@endsection