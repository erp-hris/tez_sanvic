		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label text-red">{{ __('page.required_fields') }}</label>
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.last_name') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="last_name" id="last_name" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.first_name') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="first_name" id="first_name" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.middle_name') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="middle_name" id="middle_name" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

		<div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.username_generated') }}</label>
                        <input type="text" class="form-control form-control-xs" name="username" id="username" autocomplete="off" readonly>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.email_address') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="email" class="form-control form-control-xs" name="email_address" id="email_address" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.new_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="new_password" id="new_password" onfocus="$('#confirm_password').val('')" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.confirm_password') }}</label>
                        <input type="password" class="form-control form-control-xs" name="confirm_password" id="confirm_password" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row" id="frm_input_user_level">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">{{ __('page.user_level')}}<span class="text-red">&nbsp;*</span></label>
                    <select class="select2 select2-xs" 
                           	name="user_level" 
                           	id="user_level">   
                            <option value="">{{ __('page.please_select') }}</option>
                            @foreach($user_level as $key => $val)
                                <option value="{{ $val['level'] }}">{{ __('page.'.$val['name']) }}</option>
                            @endforeach
                    </select>
                <div class="select_error" id="error-user_level"></div>
            </div>
        </div>

        <div class="form-group row" id="frm_input_user_status">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <label class="control-label">{{ __('page.user_status')}}<span class="text-red">&nbsp;*</span></label>
                    <select class="select2 select2-xs" 
                            name="user_status" 
                            id="user_status">   
                            <option value="">{{ __('page.please_select') }}</option>
                            <option value="1">{{ __('page.active') }}</option>
                            <option value="0" selected>{{ __('page.inactive') }}</option>
                    </select>
                <div class="select_error" id="error-user_status"></div>
            </div>
        </div>

       	<input type="hidden" name="user_id" id="user_id" />

        @section('additional-scripts')
            <script type="text/javascript">
                $(document).ready(function () {

                });

                $("#save_btn").click(function() {
                    var frm, text, title, success, user_id, formData;

                    user_id = $('#user_id').val();
                    frm = document.querySelector('#save_form');

                    formData = new FormData();

                    if(user_id)
                    {
                        formData.append('user_id', user_id);
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }
                    else
                    {
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        clearErrors();
                        if(result.value){
                            @foreach($default_inputs as $key => $val)
                                formData.append("{{ $val['input_name'] }}", $("#{{ $val['input_name'] }}").val());
                            @endforeach

                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    clearInputs();
                                    if(user_id)
                                    {
                                        select_account(user_id);
                                        $('#filter_btn').click();
                                    }
                                    else
                                    {
                                        const swal_continue = alert_continue("{{ __('page.add_'.$option) }}", "Do you want to add another {{ trans('page.'.$option) }}?");
                                        swal_continue.then((result) => {
                                            if(!result.value) {
                                                window.location.href = "{{ $cancel_url }}";
                                            }
                                        });
                                    }
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    const firstItem = Object.keys(errors)[0];
                                    const firstItemDOM = document.getElementById(firstItem);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();

                                    alert_warning("{{ __('page.check_inputs') }}", 1500);

                                    showErrors(firstItem, firstErrorMessage, firstItemDOM, ['user_level', 'user_status']);
                                }
                            });   
                        }
                    })
                });

	            function select_account(user_id)
	            {
	                clearErrors();

	                axios.get("{{ url('/user_account/json') }}", {
                        params: {
                            user_id : user_id,
                        }
                    })
	               .then(function(response){
	                    const user_account = response.data.user_account;

	                    $("#user_id").val(user_account['id']);
                        $("#last_name").val(user_account['last_name']);
                        $("#first_name").val(user_account['first_name']);
                        $("#middle_name").val(user_account['middle_name']);
                        $("#username").val(user_account['username']);
                        $("#email_address").val(user_account['email']);
                        $("#user_level").val(user_account['level']).trigger('change');
                        $("#user_status").val(user_account['status']).trigger('change');

                        $('#delete_record').removeClass('disabled');

	                }).catch((error) => {
                        alert_warning(error.response.data.errors, 1500);
	                });
	            }

                $("#delete_record").click(function() {
                    var frm, text, title, success, user_id, formData;

                    user_id = $('#user_id').val();
                    frm = document.querySelector('#delete_form');

                    formData = new FormData();
                    formData.append('user_id', user_id);

                    title = "{{ __('page.delete_'.$option) }}";
                    text = "{{ __('page.delete_this') }}";
                    success = "{{ __('page.deleted_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    
                    const swal_continue = alert_continue(title, text, 'btn btn-danger', 'colored-header colored-header-danger');
                    swal_continue.then((result) => {
                        if(result.value){
                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    window.location.href="{{ $cancel_url }}";
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                            });
                        }
                    });
                });

                $("#add_record").click(function() {
                    var title, text;

                    title = "{{ __('page.add_'.$option) }}";
                    text = "{{ __('page.continue_this') }}";

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        if(result.value){
                            window.location.href="{{ $cancel_url }}";
                        }
                    });
                });
            </script>
        @endsection