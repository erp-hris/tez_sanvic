@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.form-css')
@endsection

@section('content')
    @include('others.main_content', [
    'option' => $option, 
    'title' => $title, 
    'has_icon' => $icon,
    'has_file' => $file,
    'has_footer' => 'yes', 
    'isSave' => 'yes', 
    'cancel_url' => $cancel_url,
    'has_frm' => 'yes',
    'frm_method' => 'POST',
    'frm_action' => $frm_action,
    'frm_id' => 'edit_form'
    ])
@endsection

@section('scripts')
    @include('layouts.auth-partials.form-scripts')
    
    @yield('additional-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            App.formElements();
        });
    </script>
@endsection
