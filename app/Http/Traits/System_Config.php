<?php

namespace App\Http\Traits;

use DB;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait System_Config
{
	protected $system_config_option = ['announcement', 'quotation','multiple_training', 'administrator', 'employee'];

	protected function is_system_config_option_exist($option)
	{
		if(!in_array($option, $this->system_config_option)) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.option'))]));

		return true;
	}

	protected function get_announcement($id = null)
	{
		$query = DB::table('announcements')
		->where('announcements.deleted_at', null);

		if($id) 
		{
			return $query->where('announcements.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function count_announcement($id)
	{
		return DB::table('announcements')
		->where('announcements.deleted_at', null)
		->where('announcements.id', $id)
		->count();
	}

	protected function check_exist_announcement($id)
	{
		if($this->count_announcement($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_announcement($id);
	}


	protected function get_quotation($id = null)
	{
		$query = DB::table('quotations')
		->where('quotations.deleted_at', null);

		if($id) 
		{
			return $query->where('quotations.id', $id)->first();
		}
		else
		{
			return $query->get();
		}
	}

	protected function count_quotation($id)
	{
		return DB::table('quotations')
		->where('quotations.deleted_at', null)
		->where('quotations.id', $id)
		->count();
	}

	protected function check_exist_quotation($id)
	{
		if($this->count_quotation($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_quotation($id);
	}

	protected function get_employee_trainings()
	{
		return DB::table('employee_training')
		->join('employees', 'employees.id', '=', 'employee_training.employee_id')
		->leftjoin('trainings', 'trainings.id', '=', 'employee_training.training_id')
		->select('employee_training.*', DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'), 'trainings.name AS training_name')
		->where('employee_training.multiple', '=', '1')
		->where('employee_training.deleted_at', null)
		->get();	
	}

	protected function count_user($id, $level = null)
	{
		$query = DB::table('users')
		->where('users.deleted_at', null);

		if($level == '1')
		{
			$query->where('users.employee_id', $id);
		}
		else
		{
			$query->where('users.id', $id);
		}

		return $query->count();	
	}

	protected function get_user($user_level = null, $id = null, $user_status = null, $except = null)
	{
		$query = DB::table('users')
		->where('users.deleted_at', null)
		->select('users.*');

		if($user_level == '1') 
		{	
			$query->where('users.level', $user_level)
			->join('employees', 'employees.id', 'users.employee_id')
			->join('employee_informations', 'employee_informations.employee_id', '=', 'employees.id')
			->where(function ($query) {
	             $query->whereDate('employee_informations.resign_date', '>=', Carbon::today())
	             		->orwhere('employee_informations.resign_date', '=', '0000-00-00')
	                   	->orwhere('employee_informations.resign_date', null);
	        })
			->addselect(DB::raw('CONCAT(employees.last_name,", ",employees.first_name) as employee_name'));
		}
		else
		{
			$query->whereIn('users.level', ['0']);
		}
		
		if($user_status) $query->where('users.user_status', $user_status);

		if($except)
		{
			$query->whereNotIn('users.id', $except);
		}

		if($id)
		{
			if($user_level == '1')
			{
				$query->where('users.employee_id', $id);
			}
			else
			{
				$query->where('users.id', $id);
			}

			return $query->first();


			//return $query->where('users.id', $id);
		}
		else
		{
			return $query->orderby('users.name')->get();
		}
	}

	protected function check_exist_administrator_account($id)
	{
		if($this->count_user($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_user(null, $id);
	}

	protected function check_exist_employee_account($id)
	{		
		if($this->count_user($id, '1') == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_user('1', $id);
	}

	protected function filter_administrator_account($filter)
	{
		return DB::table('users')
		->where('users.deleted_at', null)
		->wherein('users.level', ['0'])
		->where(function ($query) use ($filter) {
             $query->where('users.name', 'like', '%'.$filter.'%');
        })
        ->orderBy('users.name','asc')
		->get();
	}
}