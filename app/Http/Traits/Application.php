<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use DB;
use Exception;

trait Application
{
	protected function list_business_permits($user_id, $evaluator, $approver, $issuer)
	{
		$query = DB::table('business_permit')
		->where('business_permit.is_deleted', null);
		if($user_id)
		{
			$query->where('user_id', '=', $user_id);
		}
		if($evaluator)
		{
			$query->where('status', '=', 2);
		}
		if($approver)
		{
			$query->where('status', '=', 3);
		}
		if($issuer)
		{
			$query->where('status', '=', 4);
		}
		return $query->get();
		
	}

	protected function list_location_permits($user_id, $evaluator, $approver, $issuer)
	{
		$query = DB::table('location_permit')
		->where('location_permit.is_deleted', null);
		if($user_id)
		{
			$query->where('user_id', '=', $user_id);
		}
		if($evaluator)
		{
			$query->where('status', '=', 2);
		}
		if($approver)
		{
			$query->where('status', '=', 3);
		}
		if($issuer)
		{
			$query->where('status', '=', 4);
		}
		return $query->get();
		
	}

	protected function list_building_permits($user_id, $evaluator, $approver, $issuer)
	{
		$query = DB::table('building_permit')
		->where('building_permit.is_deleted', null);
		if($user_id)
		{
			$query->where('user_id', '=', $user_id);
		}
		if($evaluator)
		{
			$query->where('status', '=', 2);
		}
		if($approver)
		{
			$query->where('status', '=', 3);
		}
		if($issuer)
		{
			$query->where('status', '=', 4);
		}
		return $query->get();
		
	}

	protected function list_files($permit_type = null)
	{
		$query = DB::table('files')
		->where('files.is_deleted', null);
		if($permit_type)
		{
			$query->where('files.permit_type', '=', $permit_type);
		}
		
		return $query->get();
	}

	protected function get_record($table, $id)
	{
		return DB::table($table)
		->where($table.'.id', '=', $id)
		->where($table.'.is_deleted', null)
		->first();
	}

	protected function check_if_attachment_exist($user_id, $file_id, $record_id)
	{
		$table = 'attachments';
		return DB::table($table)
		->where($table.'.user_id', '=', $user_id)
		->where($table.'.file_id', '=', $file_id)
		->where($table.'.record_id', '=', $record_id)
		->where($table.'.is_deleted', null)
		->first();
	}

	protected function get_attachments($user_id, $record_id, $permit_type)
	{
		$table = 'attachments';
		return DB::table($table)
		->where($table.'.user_id', '=', $user_id)
		->where($table.'.record_id', '=', $record_id)
		->where($table.'.permit_type', '=', $permit_type)
		->where($table.'.is_deleted', null)
		->get();
	}
}