<?php

namespace App\Http\Controllers;

use App\Http\Traits\Employee;
use App\Http\Traits\System_Config;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class UserDashboardController extends Controller
{
	use Employee, System_Config;

    private $personal_info_attribute;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'user-dashboard';
        
    }

    public function index()
    {
        $latest_announcement        = $this->latest_announcement();
        $get_active_quotation       = $this->get_active_quotation();



		$default_table_id = 'employees_tbl';
        
    	return view('user-dashboard.index', ['module' => $this->module, 'option' => 'dashboard', 'default_table_id' => $default_table_id, 'default_json_url' => '', 'default_columns' => '', 'latest_announcement' => $latest_announcement, 'get_active_quotation' => $get_active_quotation]);
    }
}
