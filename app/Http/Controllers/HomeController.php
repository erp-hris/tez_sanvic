<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\Dashboard;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    use Dashboard;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'home';
    }

    public function index()
    {
        if(Auth::user()->level == 1)
        {
            $data = ['module' => $this->module]; 
            return view('home.user-index', $data);
        }
        else
        {
            $data = ['module' => $this->module]; 
            return view('home.index', $data);
        }
        
    }
}
