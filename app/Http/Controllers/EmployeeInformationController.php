<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Traits\Employee;

class EmployeeInformationController extends Controller
{
    use Employee;

    public function __construct()
    {
        $this->middleware('auth');
        $this->module = 'employee-information';
        View::share('title', 'Employee Information');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->list_employees();
        return view('employee-information.index', ['module' => $this->module, 'employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $update_data = [
                    'hired_date' => $request->get('hired_date'),
                    'assumption_date' => $request->get('assumption_date'),
                    'resign_date' => $request->get('resign_date'),
                    'start_date' => $request->get('start_date'),
                    'end_date' => $request->get('end_date'),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
            ];
            DB::beginTransaction();
            DB::table("employee_informations")
                ->where('id', '=',$request->get('id'))
                ->update($update_data); 
            DB::commit();    
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return back()->with('danger', $e->getMessage());
            
            return response(['errors' => $data], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_employee_info($id)
    {
        $info       = $this->get_employee_information($id);
        $workexp    = $this->get_latest_workexp($id);
        return response(['data' => $info, 'workexp' => $workexp]);
    }
}
