<?php

namespace App\Http\Controllers;

use App\Http\Traits\Application;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class ApplicationController extends Controller
{
    use Application;

    protected $location_permit_fields = [
        'proponent_name',
        'representative',
        'business_name',
        'business_line',
        'location',
        'contact_number',
        'fax_number',
        'email_add',
        'website',
        'application_type',
        'other_application',
        'project_type',
        'other_project',
        'units',
        'storey',
        'lot_area',
        'floor_area',
        'ownership_type',
        'other_ownership',
        'investment_cost',
    ];

    protected $business_permit_fields = [
        'enterprise_classification',
        'primary_enterprise_type',
        'secondary_enterprise_type',
        'tourist_transport_type',
        'application_type',
        'payment_mode',
        'tin',
        'registration_number',
        'business_type',
        'amendment_from',
        'amendment_to',
        'tax_incentive_from_govt',
        'govt_entity',
        'last_name',
        'first_name',
        'middle_name',
        'business_name',
        'trade_name',
        'business_address',
        'business_postal_code',
        'business_tel_no',
        'business_email_add',
        'business_mobile_no',
        'owner_address',
        'owner_postal_code',
        'owner_tel_no',
        'owner_email_add',
        'owner_mobile_no',
        'contact_person',
        'contact_person_number',
        'business_area',
        'total_employees',
        'residing_with_lgu',
        'lessor_full_name',
        'lessor_address',
        'lessor_contact_number',
        'lessor_email_add',
        'monthly_rental',
    ];

    protected $building_permit_fields = [
        'application_type',
        'permit_type',
        'area_no',
        'last_name',
        'first_name',
        'middle_name',
        'house_number',
        'street',
        'barangay',
        'city',
        'zip_code',
        'telephone_number',
        'lot_number',
        'block_no',
        'tct_no',
        'tax_declaration_no',
        'construction_street',
        'construction_barangay',
        'construction_city',
        'work_scope',
        'other_work_scope',
        'occupancy_character',
        'other_occupancy_character',
        'occupancy_classification',
        'estimated_cost',
        'units',
        'construction_date',
        'floor_area',
        'expected_completion_date',
    ];
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'business_permit';
    }

    public function index($module, $option)
    {
        if($module == 'business_permit')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'filed_date'],
                ['data' => 'issued_date'],
                ['data' => 'business_name'],
                ['data' => 'status'],
                ['data' => 'file_lock'],
            );
        }
        else if ($module == 'location_permit')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'filed_date'],
                ['data' => 'issued_date'],
                ['data' => 'business_name'],
                ['data' => 'status'],
                ['data' => 'file_lock'],
            );   
        }
        else if ($module == 'building_permit')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'filed_date'],
                ['data' => 'issued_date'],
                ['data' => 'permit_type'],
                ['data' => 'status'],
                ['data' => 'file_lock'],
            );   
        }
        else if ($module == 'file_setup')
        {
            $json_url = url('/'.$module.'/'.$option.'/datatables');
            $columns = array(
                ['data' => 'action', 'sortable' => false],
                ['data' => 'name'],
                ['data' => 'permit_type'],
                ['data' => 'required'],
            );
        }

        $icon = 'mdi mdi-plus';
    	$data = ['module' => $module, 'option' => $option, 'icon' => $icon, 'json_url' => $json_url, 'columns' => $columns];
        return view($module.'.'.$option.'.index', $data);
    }

    public function create($module, $option)
    {   
        $array = array();
        if($module == 'location_permit')
        {
            $fields = $this->location_permit_fields;
            $files = $this->list_files(2);
            $array = ['files' => $files];
        }
        else if($module == 'business_permit')
        {
            $fields = $this->business_permit_fields;
            $files = $this->list_files(1);
            $array = ['files' => $files];
        }
        else if($module == 'building_permit')
        {
            $fields = $this->building_permit_fields;
            $files = $this->list_files(3);
            $array = ['files' => $files];
        }
        else
        {
            $fields = '';
        }
        $icon = 'mdi mdi-plus';
        $application_url  = '';
        $file = $module.'.'.$option.'.add';
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'fields' => $fields, 'array' => $array];
        return view($module.'.'.$option.'.create', $data);
    }

    public function attachment($module, $option, $id)
    {
        if($module == 'location_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->location_permit_fields;
            $files      = $this->list_files(2);
            $attachment_data = $this->get_attachments($user_id, $id, 2);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else if($module == 'business_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->business_permit_fields;
            $files      = $this->list_files(1);
            $attachment_data = $this->get_attachments($user_id, $id, 1);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else if($module == 'building_permit')
        {
            $user_id    = Auth::user()->id;
            $fields     = $this->building_permit_fields;
            $files      = $this->list_files(3);
            $attachment_data = $this->get_attachments($user_id, $id, 3);
            $array = ['files' => $files, 'id' => $id, 'attachments' => $attachment_data];
        }
        else
        {
            $fields = '';
        }
        $icon = 'mdi mdi-plus';
        $application_url  = '';
        $file = $module.'.'.$option.'.add';
        $data = ['module' => $module, 'option' => $option, 'file' => $file, 'icon' => $icon, 'application_url' => $application_url, 'fields' => $fields, 'array' => $array];
        return view($module.'.'.$option.'.create', $data);
    }

    public function datatables($module, $option)
    {
        $user_id = 0;
        $evaluator = 0;
        $approver = 0;
        $issuer = 0;

        if(Auth::user()->level == 1)
        {
            $user_id    = Auth::user()->id;
            $evaluator  = 0;
            $approver   = 0;
            $issuer     = 0;
        }
        else
        {
            if(Auth::user()->evaluator)
            {
                $evaluator  = 1;
                $approver   = 0;
                $issuer     = 0;
            }
            if(Auth::user()->approver)
            {
                $evaluator  = 0;
                $approver   = 1;
                $issuer     = 0;
            }
            if(Auth::user()->issuer)
            {
                $evaluator  = 0;
                $approver   = 0;
                $issuer     = 1;
            }
        }

        if ($module == 'business_permit')
        {
            $data = $this->list_business_permits($user_id, $evaluator, $approver, $issuer);
        }
        else if($module == 'location_permit')
        {
            $data = $this->list_location_permits($user_id, $evaluator, $approver, $issuer);
        }
        else if($module == 'building_permit')
        {
            $data = $this->list_building_permits($user_id, $evaluator, $approver, $issuer);
        }
        else if($module == 'file_setup')
        {
            $data = $this->list_files();
        }
        else
        {
            $data = array();
        }
        if(in_array($option, ['location_permit_attachment', 'business_permit_attachment', 'building_permit_attachment']))
        {
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($module, $option) {
                if($data->file_lock)
                {
                    return '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="'.url('/'.$module.'/'.$data->id.'/view').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-eye"></i>View Attachments
                            </a>
                        </div>
                    </div>';
                }
                else
                {
                    return '<div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="'.url('/'.$module.'/'.$data->id.'/view').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-eye"></i>View Attachments
                            </a>
                            <a href="'.url($module.'/'.$option.'/'.$data->id.'/attachment').'" class="dropdown-item">
                                <i class="icon icon-left mdi mdi-plus"></i>Add Attachments
                            </a>
                        </div>
                    </div>';
                }
                
            });
        }
        else
        {
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($module) {
                return '<div class="tools">
                    <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                        <a href="'.url('/'.$module.'/'.$data->id.'/view').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-eye"></i>View
                        </a>
                        <a href="'.url('/'.$module.'/'.$data->id.'/edit').'" class="dropdown-item">
                            <i class="icon icon-left mdi mdi-edit"></i>Edit
                        </a>
                        <span class="dropdown-item" onclick="delete_record(\''.$module.'\','.$data->id.')">
                            <i class="icon icon-left mdi mdi-delete"></i>Remove
                        </span>
                        <span class="dropdown-item" onclick="to_evaluator(\''.$module.'\','.$data->id.')">
                            <i class="icon icon-left mdi mdi-skip-next"></i>Pass To Evaluator
                        </span>
                    </div>
                </div>';
                
            });
        }
        
        if($module != 'file_setup')
        {
            $datatables
            ->editColumn('status', function($data){
                if ($data->status == 1)
                {
                    $data->status = 'Pending';
                }
                elseif ($data->status == 2)
                {
                    $data->status = 'To Evaluate';
                }
                elseif ($data->status == 3)
                {
                    $data->status = 'To Approve';
                }
                elseif ($data->status == 4)
                {
                    $data->status = 'To Issue';
                }
                elseif ($data->status == 5)
                {
                    $data->status = 'Denied';
                }
                return $data->status;
            });

            $datatables
            ->editColumn('file_lock', function($data){
                if ($data->file_lock == 1)
                {
                    $data->file_lock = 'YES';
                }
                else
                {
                    $data->file_lock = 'NO';
                }
                
                return $data->file_lock;
            });
        }
        else
        {
            $datatables
            ->editColumn('permit_type', function($data){
                if ($data->permit_type == 1)
                {
                    $data->permit_type = 'Business Permit';
                }
                elseif ($data->permit_type == 2)
                {
                    $data->permit_type = 'Location Permit';
                }
                elseif ($data->permit_type == 3)
                {
                    $data->permit_type = 'Building Permit';
                }
                elseif ($data->permit_type == 4)
                {
                    $data->permit_type = 'Occupancy Permit';
                }
                return $data->permit_type;
            });

            $datatables
            ->editColumn('required', function($data){
                if ($data->required == 1)
                {
                    $data->required = 'YES';
                }
                else
                {
                    $data->required = 'NO';
                }
                
                return $data->required;
            });
        }

        if($module == 'building_permit')
        {
            $datatables
            ->editColumn('permit_type', function($data){
                if ($data->permit_type == 1)
                {
                    $data->permit_type = 'Architectural Permit';
                }
                else if ($data->permit_type == 2)
                {
                    $data->permit_type = 'Civil / Structural Permit';
                }
                else if ($data->permit_type == 3)
                {
                    $data->permit_type = 'Electrical Permit';
                }
                else if ($data->permit_type == 4)
                {
                    $data->permit_type = 'Mechanical Permit';
                }
                else if ($data->permit_type == 5)
                {
                    $data->permit_type = 'Electronics Permit';
                }
                else if ($data->permit_type == 6)
                {
                    $data->permit_type = 'Sanitary / Plumbing';
                }
                
                return $data->permit_type;
            });
        }
        
        return $datatables
        ->rawColumns(['action'])
        ->make(true);
    }

    public function store(Request $request,$module, $option)
    {
        try {
            if($module != 'file_setup')
            {
                $user_id    = Auth::user()->id;
                $filed_date = date('Y-m-d',time());
                $insert_data = [
                    'user_id' => $user_id,
                    'filed_date' => $filed_date,
                    'created_by' => Auth::user()->id,
                    'status' => 1,
                    'created_at' => DB::raw('now()')
                ];


                if($module == 'location_permit')
                {
                    $table = 'location_permit';   
                    foreach ($this->location_permit_fields as $key => $value) {
                        $insert_data[$value] = $request->get($value);
                    }
                }
                else if($module == 'business_permit')
                {
                    $table = 'business_permit';   
                    foreach ($this->business_permit_fields as $key => $value) {
                        $insert_data[$value] = $request->get($value);
                    }
                }
                else if($module == 'building_permit')
                {
                    $table = 'building_permit';   
                    foreach ($this->building_permit_fields as $key => $value) {
                        $insert_data[$value] = $request->get($value);
                    }
                }
            }
            else
            {
                $insert_data = [
                    'code' => $request->get('code'),
                    'name' => $request->get('name'),
                    'permit_type' => $request->get('permit_type'),
                    'required' => $request->get('required'),
                    'remarks' => $request->get('remarks'),
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                ];
                $table = 'files';
            }
            


            DB::beginTransaction();

            DB::table($table)
            ->insert($insert_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        
        return response('success', 201);
    }

    public function upload(Request $request, $module, $option)
    {
        try {
            $user_id        = Auth::user()->id;
            $record_id      = $request->get('record_id');
            $file_id        = $request->get('file_id');
            $file_data      = $this->get_record('files', $file_id);
            $file_code      = $file_data->code;
            $file           = Input::File('file');
            $token_code     = Str::random(50);
            $file_extension = $file->getClientOriginalExtension();
            $data_name      = $file_code.'_'.$token_code.'.'.$file_extension;

            $folder_path = public_path().'/tieza/'.$module.'/'.$record_id;

            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);


            $checking       = $this->check_if_attachment_exist($user_id, $file_id, $record_id);

            if($module == 'business_permit')
            {
                $permit_type = 1;
            }
            else if($module == 'location_permit')
            {
                $permit_type = 2;
            }
            else if($module == 'building_permit')
            {
                $permit_type = 3;
            }
            else if($module == 'occupancy_permit')
            {
                $permit_type = 4;
            }
            else
            {
                $permit_type = 0;
            }

            if($checking)
            {
                $request->file('file')->move($folder_path, $data_name);

                $update_data = [
                    'user_id' => $user_id,
                    'record_id' => $record_id,
                    'permit_type' => $permit_type,
                    'file_id' => $file_id,
                    'attachment_name' => $data_name,
                    'lock_file' => 0,
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                ];
                DB::beginTransaction();

                DB::table('attachments')
                ->where('attachments.id', '=', $checking->id)
                ->update($update_data);

                DB::commit();
            }else
            {
                $request->file('file')->move($folder_path, $data_name);

                $insert_data = [
                    'user_id' => $user_id,
                    'record_id' => $record_id,
                    'permit_type' => $permit_type,
                    'file_id' => $file_id,
                    'attachment_name' => $data_name,
                    'lock_file' => 0,
                    'created_by' => $user_id,
                    'created_at' => DB::raw('now()')
                ];
                DB::beginTransaction();

                DB::table('attachments')
                ->insert($insert_data);

                DB::commit();
            }

            
            
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response(['status' => 200, 'file_name' => $data_name], 201);
    } 

    public function to_evaluate($permit, $id)
    {
        try {
            $table = $permit;
            $update_data = [
                'file_lock' => 1,
                'evaluated_date' => date('Y-m-d', time()),
                'status' => 2,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
            ];
            DB::beginTransaction();

            DB::table($table)
            ->where($table.'.id', '=', $id)
            ->update($update_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }
        return response(['status' => 200], 201);
    }

    public function create_account()
    {
        $list = [
            [
                'First Applicant', 'applicant1', '12345', '1', '0', '0', '0', '0'
            ],
            [
                'Second Applicant', 'applicant2', '12345', '1', '0', '0', '0', '0'
            ],
            [
                'Evaluator Account', 'evaluator1', '12345', '0', '1', '0', '0', '0'
            ],
            [
                'Approver Account', 'approver1', '12345', '0', '0', '1', '0', '0'
            ],
            [
                'Issuer Account', 'approver1', '12345', '0', '0', '0', '1', '0'
            ],
        ];
        foreach ($list as $key => $value) {
            $insert_data = [
                'name' => $value[0],
                'username' => $value[1],
                'password' => bcrypt($value[2]),
                'level' => $value[3],
                'evaluator' => $value[4],
                'approver' => $value[5],
                'issuer' => $value[6],
                'permit_type' => $value[7],
                'created_by' => 1,
                'created_at' => DB::raw('now()')
            ];
            DB::beginTransaction();

            DB::table('users')
            ->insert($insert_data);

            DB::commit();
            echo 'Save '.$value[0].'<br>';
        }
    }

    public function add_requirements()
    {
        $arr = [
            'Proof of Business Registration ',
            'Securities and Exchange Commission (SEC), if Corporation',
            'Cooperative Development Authority (CDA), if Cooperative',
            'Department of Trade and Industry (DTI), if Single    Proprietorship',
            'Registration Certificate from LGU',
            'Accreditation Certificate from DOT',
            'Certified True Copy of Articles of Incorporation and By-Laws in case of Partnership, corporation, association',
            'Occupancy Permit',
            'Proof of Right over the business location',
            'Original/Transfer Certificate of Title), if owned by the applicant',
            'Contract of Lease, if not owned by the applicant',
            'Land Tax Clearance from LGU',
            'Zoning /Locational Clearance from TIEZA',
            'Sanitary Permit/Inspection Certificate from LGU',
            'Fire Safety Insurance Certificate from BFP',
            'Social Security System Identification and Clearance',
            'Phil Health Clearance',
            'PAG-IBIG/HDMF Clearance',
            'Valid visa and Labor Permit from DOLE for non-Filipino personnel',
            'Management Structure or list of names of Employer and Employees',
            'Insurance Coverage against accidents for passenger and loss of luggage',
            'List of vehicles owned by the agency',
            'Travel Agency Management Training Certificate or equivalent',
            'MARINA Certificate of Public Conveyance (all crews are duly licensed) for sea transport',
            'DOPT Franchise for land transport',
        ];
    }
}
