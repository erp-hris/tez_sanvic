<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

Class PayrollController extends Controller
{
    public function datatables_payroll($option, $id = null)
    {
        try
        {
            $this->is_payroll_option_exist($option);

            if($option == 'loan_info')
            {
                $data = $this->get_payroll_loan_info(null, $id);
            }
            elseif($option == 'benefit_info')
            {
                $data = $this->get_payroll_benefit_info(null, $id);
            }
            elseif($option == 'deduction_info')
            {
                $data = $this->get_payroll_deduction_info(null, $id);
            }
            else
            {
                $data = array();   
            } 
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 201);
        }

        $datatables = Datatables::of($data)
        ->addColumn('action', function($data) use ($option) {
            return '<div class="tools"><button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button><div role="menu" class="dropdown-menu" x-placement="bottom-start"><a href="'.url('/payroll/'.$option.'/'.$data->id.'/edit').'" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>'.trans('page.edit').'</a><a href="javascript:void(0);" class="dropdown-item" onclick="delete_record('.$data->id.');"><i class="icon icon-left mdi mdi-delete"></i>'.trans('page.delete').'</a></div></div>';
        })
        ->rawColumns(['action'])
        ->make(true);

        return $datatables;
    }

    public function get_employee_policy_by($id)
    {
        try
        {
            $employee = $this->check_exist_payroll_employee_policy($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee]);
    }

    public function get_employee_loan_by($id)
    {
        try
        {
            $employee = $this->check_exist_payroll_loan_info($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee]);
    }

    public function get_employee_benefit_by($id)
    {
        try
        {
            $employee = $this->check_exist_payroll_benefit_info($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee]);
    }

    public function get_employee_deduction_by($id)
    {
        try
        {
            $employee = $this->check_exist_payroll_deduction_info($id);
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response(['employee' => $employee]);
    }
}