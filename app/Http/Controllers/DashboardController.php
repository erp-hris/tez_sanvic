<?php

namespace App\Http\Controllers;

use App\Http\Traits\Dashboard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\Employee;

class DashboardController extends Controller
{
    use Dashboard;
    use Employee;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'dashboard';
    }

    public function index()
    {
        if(Auth::user()->isAdmin())
        {
            $file                       = 'dashboard.home';
            $month                      = date('m',time());
            $year                       = date('Y',time());
            $assignment_array           = array();
            $employement_status         = array();

            $count_male                 = $this->count_by('gender', 'M');
            $count_female               = $this->count_by('gender', 'F');
            $count_undefined            = $this->count_by('gender', null);

            $count_plantilla            = $this->count_by('employment_category', '1');
            $count_non_plantilla        = $this->count_by('employment_category', '0');
            $count_undefined_plantilla  = $this->count_by('employment_category', null);

            $latest_announcement        = $this->latest_announcement();
            $get_active_quotation       = $this->get_active_quotation();

            $birthday_celebrants        = $this->count_celebrants_by($month);
            $assignment_list            = $this->list_assignments();

            
            foreach($this->list_employment_status() as $key => $val)
            {
                $name = 'count_'.strtolower(str_replace(' ', '_', $val->name));

                $employement_status[$name] = $this->count_by('employment_status', $val->id);
            }

            $place_of_assignment = array();

            foreach($this->list_assignments() as $key => $val)
            {
                $place_of_assignment[] = array('field' => $val->name, 'count' => $this->count_by('place_of_assignnment', $val->id));
            }

            $data = array_merge($employement_status, 
                [
                    'module' => $this->module, 
                    'count_male' => $count_male, 
                    'count_female' => $count_female, 
                    'count_undefined' => $count_undefined, 
                    'count_plantilla' => $count_plantilla, 
                    'count_non_plantilla' => $count_non_plantilla, 
                    'count_undefined_plantilla' => $count_undefined_plantilla, 
                    'latest_announcement' => $latest_announcement, 
                    'count_undefined_employment_status' => $this->count_by('employment_status', null), 
                    'get_active_quotation' => $get_active_quotation,
                    'birthday_celebrants' => $birthday_celebrants,
                    'hired_employees' => $this->count_hired_employees_by($month, $year),
                    'resign_employees' => $this->count_resigned_employees_by($month, $year),
                    'place_of_assignment' => $place_of_assignment
                ]
            );
        }
        elseif(Auth::user()->isUser())
        {
            $latest_announcement        = $this->latest_announcement();
            $get_active_quotation       = $this->get_active_quotation();


            $file = 'user-dashboard.index';
            $data = [
            'module' => $this->module, 
            'option' => 'dashboard', 
            'default_table_id' => '', 
            'default_json_url' => '', 
            'default_columns' => '',
            'latest_announcement' => $latest_announcement,
            'get_active_quotation' => $get_active_quotation,
            ];
        }

        return view($file, $data);
    }
}
