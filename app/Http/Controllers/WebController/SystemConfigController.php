<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Employee;
use App\Http\Traits\System_Config;
use DB;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class SystemConfigController extends Controller
{
    use Employee, System_Config;

    private $system_config_url;

    private $system_config_icon;

    private $input_announcement;

    private $input_quotation;

    private $input_multiple_training;

    private $input_administrator_account;

    private $input_employee_account;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'system_config';

        $this->system_config_url = url('/system_config');

        $this->system_config_icon = 'mdi mdi-settings';

        $this->input_announcement = array(
            ['field_name' => 'announcement', 'input_name' => 'announcement_'],
            ['field_name' => 'start_date', 'input_name' => 'start_date'],
            ['field_name' => 'end_date', 'input_name' => 'end_date']
        );

        $this->input_quotation = array(
            ['field_name' => 'quotation', 'input_name' => 'quotation_']
        );

        $this->input_multiple_training = array(
            ['field_name' => 'employee_id', 'input_name' => 'employee-list'],
            ['field_name' => 'training_id', 'input_name' => 'training_name'],
            ['field_name' => 'training_type', 'input_name' => 'training_type'],
            ['field_name' => 'training_start_date', 'input_name' => 'start_date'],
            ['field_name' => 'training_end_date', 'input_name' => 'end_date'],
            ['field_name' => 'training_total_hours', 'input_name' => 'no_of_hours'],
            ['field_name' => 'training_sponsor', 'input_name' => 'sponsor'],
        );

        $this->input_administrator_account = array(
            ['field_name' => 'name', 'input_name' => 'name'],
            ['field_name' => 'username', 'input_name' => 'username'],
            ['field_name' => 'email', 'input_name' => 'email_address'],
            ['field_name' => '', 'input_name' => 'new_password'],
            ['field_name' => '', 'input_name' => 'confirm_password'],
            ['field_name' => 'user_status', 'input_name' => 'employee_type']
        );

        $this->input_employee_account = array(
            ['field_name' => 'name', 'input_name' => 'name'],
            ['field_name' => 'username', 'input_name' => 'username'],
            ['field_name' => 'email', 'input_name' => 'email_address'],
            ['field_name' => '', 'input_name' => 'new_password'],
            ['field_name' => '', 'input_name' => 'confirm_password'],
            ['field_name' => 'user_status', 'input_name' => 'employee_type']
        );
    }

    public function index(request $request)
    {
       return back();
    }

    public function system_config(request $request, $option)
    {
        try
        {
            $this->is_system_config_option_exist($option);
            $data = ['module' => $this->module, 'option' => $option, 'system_config_url' => $this->system_config_url, 'icon' => $this->system_config_icon];

            if($option == 'announcement')
            {
                $view = 'system_config.announcement.index';

                $data = array_merge($data, ['file' => 'system_config.announcement.table', 'default_table_id' => 'announcement_tbl', 'default_json_url' => url('/system_config/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'announcement'], ['data' => 'start_date'], ['data' => 'end_date'])]);
            }
            elseif($option == 'quotation')
            {
                $view = 'system_config.quotation.index';

                $data = array_merge($data, ['file' => 'system_config.quotation.table', 'default_table_id' => 'announcement_tbl', 'default_json_url' => url('/system_config/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'quotation'])]);
            }
            elseif($option == 'multiple_training')
            {
                $view = 'system_config.'.$option.'.index';

                $data = array_merge($data, ['file' => 'system_config.multiple_training.table', 'default_table_id' => 'training_tbl', 'default_json_url' => url('/system_config/datatables/'.$option), 'default_columns' => array(['data' => 'action', 'sortable' => false], ['data' => 'employee_name'], ['data' => 'training_name'], ['data' => 'training_start_date'], ['data' => 'training_end_date'])]);
            }
            elseif($option == 'administrator')
            {
                $view = 'system_config.user_account.index';

                $data = array_merge($data, ['file' => 'system_config.user_account.administrator.form', 'default_inputs' => $this->input_administrator_account]);
            }
            elseif($option == 'employee')
            {
                $view = 'system_config.user_account.index';

                $data = array_merge($data, ['file' => 'system_config.user_account.employee.form', 'list_employees' => $this->list_employees(), 'default_inputs' => $this->input_employee_account]);
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, $data);
    }

    public function create_system_config(request $request, $option)
    {
        try
        {
            $this->is_system_config_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'system_config_url' => $this->system_config_url, 'icon' => $this->system_config_icon];

            if($option == 'announcement')
            {
                $view = 'system_config.announcement.create';

                $data = array_merge($data, ['default_inputs' => $this->input_announcement, 'file' => 'system_config.announcement.form', 'cancel_url' => $this->system_config_url.'/'.$option, 'frm_action' => $this->system_config_url.'/'.$option.'/store' ]);
            }
            elseif($option == 'quotation')
            {
                $view = 'system_config.quotation.create';

                $data = array_merge($data, ['default_inputs' => $this->input_quotation, 'file' => 'system_config.quotation.form', 'cancel_url' => $this->system_config_url.'/'.$option, 'frm_action' => $this->system_config_url.'/'.$option.'/store' ]);
            }
            elseif($option == 'multiple_training')
            {
                $view = 'system_config.multiple_training.create';

                $data = array_merge($data, ['default_inputs' => $this->input_multiple_training, 'file' => 'system_config.multiple_training.form', 'cancel_url' => $this->system_config_url.'/'.$option, 'frm_action' => $this->system_config_url.'/'.$option.'/store', 'trainings' => $this->list_trainings(), 'training_types' => $this->list_training_types() ]);
            }
            elseif($option == 'administrator')
            {
                $view = 'system_config.user_account.create';

                $data = array_merge($data, ['default_inputs' => $this->input_administrator_account, 'file' => 'system_config.user_account.administrator.form', 'frm_action' => $this->system_config_url.'/'.$option.'/store', 'cancel_url' => $this->system_config_url.'/'.$option ]);
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, $data);
    }

    public function store_system_config(request $request, $option)
    {   
        try 
        {  
            $this->is_system_config_option_exist($option);

            if($option == 'announcement')
            {
                $insert_data = [
                'announcement' => $request->get('announcement_'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                $rules = [
                'announcement_' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                DB::beginTransaction();

                DB::table('announcements')
                ->insert($insert_data);

                DB::commit();
            }  
            elseif($option == 'quotation')
            {
                $insert_data = [
                'quotation' => $request->get('quotation_'),
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ];

                $rules = [
                'quotation_' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                DB::beginTransaction();

                DB::table('quotations')
                ->insert($insert_data);

                DB::commit();
            }
            elseif($option == 'multiple_training')
            {
                DB::beginTransaction();
                $employees = $request->get('employee-list');
                $employees = explode(",", $employees);
                foreach ($employees as $key => $value) {
                    if ($value != '')
                    {
                       $insert_data = [
                            'employee_id' => $value,
                            'training_id' => $request->get('training_name'),
                            'training_type' => $request->get('training_type'),
                            'training_start_date' => $request->get('start_date'),
                            'training_end_date' => $request->get('end_date'),
                            'training_total_hours' => $request->get('no_of_hours'),
                            'training_sponsor' => $request->get('sponsor'),
                            'multiple' => '1',
                            'created_by' => Auth::user()->id,
                            'created_at' => DB::raw('now()')
                        ];
                        DB::table('employee_training')
                        ->insert($insert_data); 
                    }
                }
                DB::commit();
            }
            elseif($option == 'administrator')  
            {
                $rules = [
                'name' => 'required|max:255',
                'email_address' => 'sometimes|nullable|email',
                'username' => 'required|alpha_dash|min:6|max:50|not_in:'.implode(',', $this->get_value_by($this->get_user(), 'username')),
                'new_password' => 'required|min:6|max:50',
                'confirm_password' => 'same:new_password',
                'employee_type' => 'required|in:0,1',
                ];

                $this->validate_request($request->all(), $rules);

                DB::beginTransaction();

                DB::table('users')
                ->insert([
                'name' => $request->get('name'),
                'email' => $request->get('email_address'),
                'username' => $request->get('username'),
                'password' =>  bcrypt($request->get('new_password')),
                'user_status' => $request->get('employee_type'),
                'level' => '0',
                'created_by' => Auth::user()->id,
                'created_at' => DB::raw('now()')
                ]);

                DB::commit();
            } 
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response('success', 201);
    }

    public function edit_system_config(request $request, $option, $id)
    {
        try
        {
            $this->is_system_config_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'system_config_url' => $this->system_config_url, 'icon' => $this->system_config_icon];

            if($option == 'announcement')
            {
                $this->check_exist_announcement($id);

                $view = 'system_config.announcement.edit';

                $data = array_merge($data, ['default_inputs' => $this->input_announcement, 'file' => 'system_config.announcement.form', 'cancel_url' => $this->system_config_url.'/'.$option, 'frm_action' => $this->system_config_url.'/'.$option.'/'.$id.'/update', 'item_id' => $id ]);
            }
            elseif($option == 'quotation')
            {
                $this->check_exist_quotation($id);

                $view = 'system_config.quotation.edit';

                $data = array_merge($data, ['default_inputs' => $this->input_quotation, 'file' => 'system_config.quotation.form', 'cancel_url' => $this->system_config_url.'/'.$option, 'frm_action' => $this->system_config_url.'/'.$option.'/'.$id.'/update', 'item_id' => $id ]);
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, $data);
    }

    public function update_system_config(request $request, $option, $id = null)
    {
        try 
        {  
            $this->is_system_config_option_exist($option);

            if($option == 'announcement')
            {
                $this->check_exist_announcement($id);

                $update_data = [
                'announcement' => $request->get('announcement_'),
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                $rules = [
                'announcement_' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                DB::beginTransaction();

                DB::table('announcements')
                ->where('announcements.id', $id)
                ->update($update_data);

                DB::commit();
            } 
            elseif($option == 'quotation')
            {
                $this->check_exist_quotation($id);

                $update_data = [
                'quotation' => $request->get('quotation_'),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                $rules = [
                'quotation_' => 'required'
                ];

                $this->validate_request($request->all(), $rules);

                DB::beginTransaction();

                DB::table('quotations')
                ->where('quotations.id', $id)
                ->update($update_data);

                DB::commit();
            } 
            elseif($option == 'administrator')  
            {
                $emp_id = $request->get('emp_id');

                $this->check_exist_administrator_account($emp_id);

                $rules = [
                'name' => 'required|max:255',
                'email_address' => 'sometimes|nullable|email',
                'username' => 'required|alpha_dash|min:6|max:50|not_in:'.implode(',', $this->get_value_by($this->get_user(null, null, null, [$emp_id]), 'username')),
                'new_password' => 'required|min:6|max:50',
                'confirm_password' => 'same:new_password',
                'employee_type' => 'required|in:0,1',
                ];

                $this->validate_request($request->all(), $rules);

                $update_data = [
                'name' => $request->get('name'),
                'email' => $request->get('email_address'),
                'username' => $request->get('username'),
                'password' =>  bcrypt($request->get('new_password')),
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                if($emp_id !== Auth::user()->id) $update_data['user_status'] =  $request->get('employee_type');

                DB::beginTransaction();

                DB::table('users')
                ->where('users.id', $emp_id)
                ->update($update_data);

                DB::commit();
            } 
            elseif($option == 'employee')
            {
                $id = $request->get('emp_id');

                if($id)
                {
                    $employee_id = $request->get('name');

                    $this->check_exist_employee_account($employee_id);

                    $rules = [
                    'email_address' => 'sometimes|nullable|email',
                    'username' => 'required|alpha_dash|min:6|max:50|not_in:'.implode(',', $this->get_value_by($this->get_user('1', null, null, [$id]), 'username')),
                    'new_password' => 'required|min:6|max:50',
                    'confirm_password' => 'same:new_password',
                    'employee_type' => 'required|in:0,1',
                    ];

                    $this->validate_request($request->all(), $rules);

                    $update_data = [
                    'email' => $request->get('email_address'),
                    'username' => $request->get('username'),
                    'password' =>  bcrypt($request->get('new_password')),
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ];

                    if($id !== Auth::user()->id) $update_data['user_status'] =  $request->get('employee_type');

                    DB::beginTransaction();

                    DB::table('users')
                    ->where('users.id', $id)
                    ->update($update_data);

                    DB::commit();

                    $this->success = ['last_id' => $employee_id];
                }
                else
                {
                    $employee_id = $request->get('name');

                    $employee = $this->check_exist_employee($employee_id);

                    $rules = [
                    'email_address' => 'sometimes|nullable|email',
                    'username' => 'required|alpha_dash|min:6|max:50|not_in:'.implode(',', $this->get_value_by($this->get_user('1', null, null, null), 'username')),
                    'new_password' => 'required|min:6|max:50',
                    'confirm_password' => 'same:new_password',
                    'employee_type' => 'required|in:0,1',
                    ];

                    $this->validate_request($request->all(), $rules);

                    $insert_data = [
                    'email' => $request->get('email_address'),
                    'username' => $request->get('username'),
                    'password' =>  bcrypt($request->get('new_password')),
                    'user_status' =>  $request->get('employee_type'),
                    'level' => '1',
                    'employee_id' => $employee_id,
                    'created_by' => Auth::user()->id,
                    'created_at' => DB::raw('now()')
                    ];

                    DB::beginTransaction();

                    DB::table('users')
                    ->insert($insert_data);

                    $new_id = DB::getPdo()->lastInsertId();

                    DB::commit();

                    $this->success = ['last_id' => $employee_id];
                }
            }
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201);   
    }

    public function delete_system_config(request $request, $option, $id)
    {
        try
        {
            $this->is_system_config_option_exist($option);

            if($option == 'announcement')
            {
                $this->check_exist_announcement($id);

                DB::beginTransaction();

                DB::table('announcements')
                ->where('announcements.id', $id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == 'quotation')
            {
                $this->check_exist_quotation($id);

                DB::beginTransaction();

                DB::table('quotations')
                ->where('quotations.id', $id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }  
            elseif($option == 'multiple_training')
            {
                DB::beginTransaction();

                DB::table('employee_training')
                ->where('employee_training.id', $id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }  
        } 
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response('success', 201);  
    }

    public function set_active_quotation($id)
    {
        try
        {
            $this->check_exist_quotation($id);

            $update_data = [
            'status' => '1',
            'updated_by' => Auth::user()->id,
            'updated_at' => DB::raw('now()')
            ];

            DB::beginTransaction();

            DB::table('quotations')
            ->where('quotations.status', '1')
            ->update(['status' => '0']);

            DB::table('quotations')
            ->where('quotations.id', $id)
            ->update($update_data);

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response('success', 201);   
    }   
}
