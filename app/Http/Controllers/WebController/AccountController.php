<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AccountController extends Controller
{
    private $input_account;

    public function __construct()
    {
    	$this->middleware('auth');

        $this->module = 'account';

        $this->input_account = array(
        ['field_name' => '', 'input_name' => 'old_password'],
        ['field_name' => '', 'input_name' => 'new_password'],
        ['field_name' => '', 'input_name' => 'confirm_password'],
        );
   	}

   	public function index()
   	{
        $data = [
        'module' => $this->module, 
        'option' => 'account',
        'title' => 'edit_account',
        'icon' => 'mdi mdi-account-circle',
        'cancel_url' => url('/account'),
        'file' => 'account.form',
        'frm_action' => url('/account/update'),
        'default_inputs' => $this->input_account
        ];

   		return view('account.index', $data);
   	}

    public function update_account(request $request)
    {
        try 
        { 
            $rules = [
            'old_password' => 'required',
            'new_password' => 'required|min:6|max:50|different:old_password',
            'confirm_password' => 'same:new_password',
            ];

            $this->validate_request($request->all(), $rules);

            if(!Hash::check($request->get('old_password'), Auth::user()->password)) throw new Exception(json_encode(['old_password' => ["The old password doesn't match."]]));

            if(Auth::user()->pw_update == 0)
            {
                $update_data = [
                    'password' => bcrypt($request->get('new_password')),
                    'pw_update' => 1
                ];    
            }
            else
            {
                $update_data = ['password' => bcrypt($request->get('new_password'))];
            }
            

            DB::beginTransaction();

            DB::table('users')
            ->where('users.id', Auth::id())
            ->update($update_data);

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }
}
