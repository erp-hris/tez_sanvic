<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifUser
{
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
	        if(Auth::user()->isUser()) 
	        {
	            return $next($request);
	        }
	        else
	        {
	            return redirect('/');
	        }
	    }
	    else
	    {
	        return redirect('/');
	    }
    }
}
