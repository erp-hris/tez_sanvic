<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingHoursConversion extends Model
{
    //
    protected $table = 'working_hours_conversion';
    protected $fillables = array('hours','equivalent','created_by');
}
