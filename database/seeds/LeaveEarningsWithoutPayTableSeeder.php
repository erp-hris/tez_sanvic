<?php

use Illuminate\Database\Seeder;
use App\LeaveEarningsWithoutPay;

class LeaveEarningsWithoutPayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        LeaveEarningsWithoutPay::truncate();

        $data = [
            ['days_present' => '30', 'days_lwop' => '0', 'earnings' => '1.25', 'created_by' => '1'],
			['days_present' => '29.5', 'days_lwop' => '0.5', 'earnings' => '1.229', 'created_by' => '1'],
			['days_present' => '29', 'days_lwop' => '1', 'earnings' => '1.208', 'created_by' => '1'],
			['days_present' => '28.5', 'days_lwop' => '1.5', 'earnings' => '1.188', 'created_by' => '1'],
			['days_present' => '28', 'days_lwop' => '2', 'earnings' => '1.167', 'created_by' => '1'],
			['days_present' => '27.5', 'days_lwop' => '2.5', 'earnings' => '1.146', 'created_by' => '1'],
			['days_present' => '27', 'days_lwop' => '3', 'earnings' => '1.125', 'created_by' => '1'],
			['days_present' => '26.5', 'days_lwop' => '3.5', 'earnings' => '1.104', 'created_by' => '1'],
			['days_present' => '26', 'days_lwop' => '4', 'earnings' => '1.083', 'created_by' => '1'],
			['days_present' => '25.5', 'days_lwop' => '4.5', 'earnings' => '1.063', 'created_by' => '1'],
			['days_present' => '25', 'days_lwop' => '5', 'earnings' => '1.042', 'created_by' => '1'],
			['days_present' => '24.5', 'days_lwop' => '5.5', 'earnings' => '1.021', 'created_by' => '1'],
			['days_present' => '24', 'days_lwop' => '6', 'earnings' => '1', 'created_by' => '1'],
			['days_present' => '23.5', 'days_lwop' => '6.5', 'earnings' => '0.979', 'created_by' => '1'],
			['days_present' => '23', 'days_lwop' => '7', 'earnings' => '0.958', 'created_by' => '1'],
			['days_present' => '22.5', 'days_lwop' => '7.5', 'earnings' => '0.938', 'created_by' => '1'],
			['days_present' => '22', 'days_lwop' => '8', 'earnings' => '0.917', 'created_by' => '1'],
			['days_present' => '21.5', 'days_lwop' => '8.5', 'earnings' => '0.896', 'created_by' => '1'],
			['days_present' => '21', 'days_lwop' => '9', 'earnings' => '0.875', 'created_by' => '1'],
			['days_present' => '20.5', 'days_lwop' => '9.5', 'earnings' => '0.854', 'created_by' => '1'],
			['days_present' => '20', 'days_lwop' => '10', 'earnings' => '0.833', 'created_by' => '1'],
			['days_present' => '19.5', 'days_lwop' => '10.5', 'earnings' => '0.813', 'created_by' => '1'],
			['days_present' => '19', 'days_lwop' => '11', 'earnings' => '0.792', 'created_by' => '1'],
			['days_present' => '18.5', 'days_lwop' => '11.5', 'earnings' => '0.771', 'created_by' => '1'],
			['days_present' => '18', 'days_lwop' => '12', 'earnings' => '0.75', 'created_by' => '1'],
			['days_present' => '17.5', 'days_lwop' => '12.5', 'earnings' => '0.729', 'created_by' => '1'],
			['days_present' => '17', 'days_lwop' => '13', 'earnings' => '0.708', 'created_by' => '1'],
			['days_present' => '16.5', 'days_lwop' => '13.5', 'earnings' => '0.687', 'created_by' => '1'],
			['days_present' => '16', 'days_lwop' => '14', 'earnings' => '0.667', 'created_by' => '1'],
			['days_present' => '15.5', 'days_lwop' => '14.5', 'earnings' => '0.646', 'created_by' => '1'],
			['days_present' => '15', 'days_lwop' => '15', 'earnings' => '0.625', 'created_by' => '1'],
			['days_present' => '14.5', 'days_lwop' => '15.5', 'earnings' => '0.604', 'created_by' => '1'],
			['days_present' => '14', 'days_lwop' => '16', 'earnings' => '0.583', 'created_by' => '1'],
			['days_present' => '13.5', 'days_lwop' => '16.5', 'earnings' => '0.562', 'created_by' => '1'],
			['days_present' => '13', 'days_lwop' => '17', 'earnings' => '0.542', 'created_by' => '1'],
			['days_present' => '12.5', 'days_lwop' => '17.5', 'earnings' => '0.521', 'created_by' => '1'],
			['days_present' => '12', 'days_lwop' => '18', 'earnings' => '0.5', 'created_by' => '1'],
			['days_present' => '11.5', 'days_lwop' => '18.5', 'earnings' => '0.479', 'created_by' => '1'],
			['days_present' => '11', 'days_lwop' => '19', 'earnings' => '0.458', 'created_by' => '1'],
			['days_present' => '10.5', 'days_lwop' => '19.5', 'earnings' => '0.437', 'created_by' => '1'],
			['days_present' => '10', 'days_lwop' => '20', 'earnings' => '0.417', 'created_by' => '1'],
			['days_present' => '9.5', 'days_lwop' => '20.5', 'earnings' => '0.396', 'created_by' => '1'],
			['days_present' => '9', 'days_lwop' => '21', 'earnings' => '0.375', 'created_by' => '1'],
			['days_present' => '8.5', 'days_lwop' => '21.5', 'earnings' => '0.354', 'created_by' => '1'],
			['days_present' => '8', 'days_lwop' => '22', 'earnings' => '0.333', 'created_by' => '1'],
			['days_present' => '7.5', 'days_lwop' => '22.5', 'earnings' => '0.312', 'created_by' => '1'],
			['days_present' => '7', 'days_lwop' => '23', 'earnings' => '0.292', 'created_by' => '1'],
			['days_present' => '6.5', 'days_lwop' => '23.5', 'earnings' => '0.271', 'created_by' => '1'],
			['days_present' => '6', 'days_lwop' => '24', 'earnings' => '0.25', 'created_by' => '1'],
			['days_present' => '5.5', 'days_lwop' => '24.5', 'earnings' => '0.229', 'created_by' => '1'],
			['days_present' => '5', 'days_lwop' => '25', 'earnings' => '0.208', 'created_by' => '1'],
			['days_present' => '4.5', 'days_lwop' => '25.5', 'earnings' => '0.187', 'created_by' => '1'],
			['days_present' => '4', 'days_lwop' => '26', 'earnings' => '0.167', 'created_by' => '1'],
			['days_present' => '3.5', 'days_lwop' => '26.5', 'earnings' => '0.146', 'created_by' => '1'],
			['days_present' => '3', 'days_lwop' => '27', 'earnings' => '0.125', 'created_by' => '1'],
			['days_present' => '2.5', 'days_lwop' => '27.5', 'earnings' => '0.104', 'created_by' => '1'],
			['days_present' => '2', 'days_lwop' => '28', 'earnings' => '0.083', 'created_by' => '1'],
			['days_present' => '1.5', 'days_lwop' => '28.5', 'earnings' => '0.062', 'created_by' => '1'],
			['days_present' => '1', 'days_lwop' => '29', 'earnings' => '0.042', 'created_by' => '1'],
			['days_present' => '0.5', 'days_lwop' => '29.5', 'earnings' => '0.021', 'created_by' => '1'],
			['days_present' => '0', 'days_lwop' => '30', 'earnings' => '0', 'created_by' => '1']
        ];

        DB::table('leave_earnings_without_pay')->insert($data);
    }
}
