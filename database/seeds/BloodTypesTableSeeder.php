<?php

use Illuminate\Database\Seeder;
use App\BloodTypesModel;


class BloodTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BloodTypesModel::truncate();

        $blood_types = [
            ['id' => '1' ,'code' => 'O', 'name' => 'O', 'created_by' => '1'],
            ['id' => '2' ,'code' => 'AB', 'name' => 'AB', 'created_by' => '1'],
            ['id' => '4' ,'code' => 'B', 'name' => 'B', 'created_by' => '1'],
            ['id' => '5' ,'code' => 'A+', 'name' => 'A+', 'created_by' => '1'],
            ['id' => '6' ,'code' => 'B+', 'name' => 'B+', 'created_by' => '1'],
            ['id' => '7' ,'code' => 'A-', 'name' => 'A-', 'created_by' => '1'],
            ['id' => '8' ,'code' => 'B-', 'name' => 'B-', 'created_by' => '1'],
            ['id' => '9' ,'code' => 'A', 'name' => 'A', 'created_by' => '1'],
            ['id' => '10' ,'code' => 'O-', 'name' => 'O-', 'created_by' => '1'],
            ['id' => '11' ,'code' => 'O+', 'name' => 'O+', 'created_by' => '1'],
            ['id' => '12' ,'code' => 'AB+', 'name' => 'AB+', 'created_by' => '1'],

        ];

        DB::table('blood_types')->insert($blood_types);    }

}
