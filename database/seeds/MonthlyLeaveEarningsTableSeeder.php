<?php

use Illuminate\Database\Seeder;
use App\MonthlyLeaveEarnings;

class MonthlyLeaveEarningsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Let's truncate our existing records to start from scratch.
        MonthlyLeaveEarnings::truncate();

        $data = [
            ['calendar_month' => '1', 'vl_equivalent' => '1.25', 'sl_equivalent' => '1.25', 'created_by' => '1'],
			['calendar_month' => '2', 'vl_equivalent' => '2.5', 'sl_equivalent' => '2.5', 'created_by' => '1'],
			['calendar_month' => '3', 'vl_equivalent' => '3.75', 'sl_equivalent' => '3.75', 'created_by' => '1'],
			['calendar_month' => '4', 'vl_equivalent' => '5', 'sl_equivalent' => '5', 'created_by' => '1'],
			['calendar_month' => '5', 'vl_equivalent' => '6.25', 'sl_equivalent' => '6.25', 'created_by' => '1'],
			['calendar_month' => '6', 'vl_equivalent' => '7.5', 'sl_equivalent' => '7.5', 'created_by' => '1'],
			['calendar_month' => '7', 'vl_equivalent' => '8.75', 'sl_equivalent' => '8.75', 'created_by' => '1'],
			['calendar_month' => '8', 'vl_equivalent' => '10', 'sl_equivalent' => '10', 'created_by' => '1'],
			['calendar_month' => '9', 'vl_equivalent' => '11.25', 'sl_equivalent' => '11.25', 'created_by' => '1'],
			['calendar_month' => '10', 'vl_equivalent' => '12.5', 'sl_equivalent' => '12.5', 'created_by' => '1'],
			['calendar_month' => '11', 'vl_equivalent' => '13.75', 'sl_equivalent' => '13.75', 'created_by' => '1'],
			['calendar_month' => '12', 'vl_equivalent' => '15', 'sl_equivalent' => '15', 'created_by' => '1'],
        ];

        DB::table('monthly_leave_earned')->insert($data);
    }
}
