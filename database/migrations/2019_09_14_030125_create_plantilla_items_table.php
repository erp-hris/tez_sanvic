<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantillaItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('plantilla_items')) {
            Schema::create('plantilla_items', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code')->nullable();
                $table->string('name');
                $table->integer('position_id')->nullable();
                $table->string('position_level')->nullable();
                $table->string('position_classification')->nullable();
                $table->integer('salary_grade')->nullable();
                $table->integer('salary_step')->nullable();
                $table->decimal('salary_amount',15,2)->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantilla_items');
    }
}
