<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('trainings')) {
            Schema::create('trainings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code')->nullable();
                $table->string('name');
                $table->string('training_institution')->nullable();
                $table->string('training_type')->nullable();
                $table->integer('semester')->nullable();
                $table->integer('year')->nullable();
                $table->integer('provider_id')->nullable();
                $table->integer('seminar_class_id')->nullable();
                $table->string('training_description')->nullable();
                $table->string('proposed_attendees')->nullable();
                $table->string('venue')->nullable();
                $table->date('training_start_date')->nullable();
                $table->date('training_end_date')->nullable();
                $table->integer('slot')->nullable();
                $table->decimal('cost_per_employee',15,2)->nullable();
                $table->decimal('approved_budget',15,2)->nullable();
                $table->date('training_deadline_date')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
