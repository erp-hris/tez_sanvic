<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_training')) {
            Schema::create('employee_training', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->integer('training_id')->nullable();
                $table->date('training_start_date')->nullable();
                $table->date('training_end_date')->nullable();
                $table->boolean('training_present')->nullable();
                $table->string('training_sponsor')->nullable();
                $table->string('training_place')->nullable();
                $table->string('training_description')->nullable();
                $table->integer('training_total_hours')->nullable();
                $table->integer('provider_id')->nullable();
                $table->string('training_type')->nullable();
                $table->boolean('training_classification')->nullable();
                $table->boolean('description')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_training');
    }
}
