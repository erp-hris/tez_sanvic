<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyMonthlyEarnedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('monthly_leave_earned')) {
            Schema::create('monthly_leave_earned', function (Blueprint $table) {
                $table->increments('id');
                $table->date('effectivity_date')->nullable();
                $table->string('calendar_month')->nullable();
                $table->decimal('vl_equivalent', 10, 3)->nullable();
                $table->decimal('sl_equivalent', 10, 3)->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_leave_earned');
    }
}
