<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employees_loan')) {
            Schema::create('employees_loan', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->date('start_date')->nullable();
                $table->date('end_date')->nullable();
                $table->date('date_granted')->nullable();
                $table->date('date_terminated')->nullable();
                $table->integer('loan_id');
                $table->integer('total_loan')->nullable();
                $table->integer('loan_balance')->nullable();
                $table->integer('amortization')->nullable();
                $table->date('pay_period')->nullable();
                $table->boolean('terminated')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees_loan');
    }
}
