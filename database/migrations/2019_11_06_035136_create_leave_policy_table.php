<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavePolicyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('leave_policy')) {
            Schema::create('leave_policy', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('leave_policy_group_id');
                $table->integer('leave_id');
                $table->boolean('accumulating')->nullable();
                $table->boolean('offset_late')->nullable();
                $table->boolean('offset_undertime')->nullable();
                $table->boolean('force_leave_applied')->nullable();
                $table->boolean('affected_by_absent')->nullable();
                $table->integer('value')->nullable();
                $table->integer('max_force_leave')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_policy');
    }
}
