<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtrProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('dtr_process')) {
            Schema::create('dtr_process', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->integer('year');
                $table->integer('month');
                $table->integer('total_regular_hours')->nullable();
                $table->integer('total_excess_hours')->nullable();
                $table->integer('total_coc_hours_working_days')->nullable();
                $table->integer('total_overtime_hours_working_days')->nullable();
                $table->integer('total_coc_hours_rest_day')->nullable();
                $table->integer('total_overtime_hours_rest_day')->nullable();
                $table->integer('total_coc_hours_holiday')->nullable();
                $table->integer('total_overtime_hours_holiday')->nullable();

                $table->integer('total_tardy_hours')->nullable();
                $table->integer('total_undertime_hours')->nullable();

                $table->integer('total_tardy_count')->nullable();
                $table->integer('total_undertime_count')->nullable();
                $table->integer('total_absent_count')->nullable();
                $table->integer('total_present_count')->nullable();

                $table->decimal('vl_earned',5,2)->nullable();
                $table->decimal('sl_earned',5,2)->nullable();

                $table->integer('spl_used')->nullable();
                $table->integer('coc_used')->nullable();
                $table->decimal('vl_used',5,2)->nullable();
                $table->decimal('sl_used',5,2)->nullable();

                $table->decimal('tardy_equivalent',5,2)->nullable();
                $table->decimal('undertime_equivalent',5,2)->nullable();
                $table->decimal('regular_hour_equivalent',5,2)->nullable();
                $table->decimal('present_equivalent',5,2)->nullable();
                $table->decimal('absent_equivalent',5,2)->nullable();

                $table->integer('01_late')->nullable();
                $table->integer('01_undertime')->nullable();
                $table->integer('01_overtime')->nullable();
                $table->integer('01_coc')->nullable();
                $table->integer('01_application')->nullable();

                $table->integer('02_late')->nullable();
                $table->integer('02_undertime')->nullable();
                $table->integer('02_overtime')->nullable();
                $table->integer('02_coc')->nullable();
                $table->integer('02_application')->nullable();

                $table->integer('03_late')->nullable();
                $table->integer('03_undertime')->nullable();
                $table->integer('03_overtime')->nullable();
                $table->integer('03_coc')->nullable();
                $table->integer('03_application')->nullable();

                $table->integer('04_late')->nullable();
                $table->integer('04_undertime')->nullable();
                $table->integer('04_overtime')->nullable();
                $table->integer('04_coc')->nullable();
                $table->integer('04_application')->nullable();

                $table->integer('05_late')->nullable();
                $table->integer('05_undertime')->nullable();
                $table->integer('05_overtime')->nullable();
                $table->integer('05_coc')->nullable();
                $table->integer('05_application')->nullable();

                $table->integer('06_late')->nullable();
                $table->integer('06_undertime')->nullable();
                $table->integer('06_overtime')->nullable();
                $table->integer('06_coc')->nullable();
                $table->integer('06_application')->nullable();

                $table->integer('07_late')->nullable();
                $table->integer('07_undertime')->nullable();
                $table->integer('07_overtime')->nullable();
                $table->integer('07_coc')->nullable();
                $table->integer('07_application')->nullable();

                $table->integer('08_late')->nullable();
                $table->integer('08_undertime')->nullable();
                $table->integer('08_overtime')->nullable();
                $table->integer('08_coc')->nullable();
                $table->integer('08_application')->nullable();

                $table->integer('09_late')->nullable();
                $table->integer('09_undertime')->nullable();
                $table->integer('09_overtime')->nullable();
                $table->integer('09_coc')->nullable();
                $table->integer('09_application')->nullable();

                $table->integer('10_late')->nullable();
                $table->integer('10_undertime')->nullable();
                $table->integer('10_overtime')->nullable();
                $table->integer('10_coc')->nullable();
                $table->integer('10_application')->nullable();

                $table->integer('11_late')->nullable();
                $table->integer('11_undertime')->nullable();
                $table->integer('11_overtime')->nullable();
                $table->integer('11_coc')->nullable();
                $table->integer('11_application')->nullable();

                $table->integer('12_late')->nullable();
                $table->integer('12_undertime')->nullable();
                $table->integer('12_overtime')->nullable();
                $table->integer('12_coc')->nullable();
                $table->integer('12_application')->nullable();

                $table->integer('13_late')->nullable();
                $table->integer('13_undertime')->nullable();
                $table->integer('13_overtime')->nullable();
                $table->integer('13_coc')->nullable();
                $table->integer('13_application')->nullable();

                $table->integer('14_late')->nullable();
                $table->integer('14_undertime')->nullable();
                $table->integer('14_overtime')->nullable();
                $table->integer('14_coc')->nullable();
                $table->integer('14_application')->nullable();

                $table->integer('15_late')->nullable();
                $table->integer('15_undertime')->nullable();
                $table->integer('15_overtime')->nullable();
                $table->integer('15_coc')->nullable();
                $table->integer('15_application')->nullable();

                $table->integer('16_late')->nullable();
                $table->integer('16_undertime')->nullable();
                $table->integer('16_overtime')->nullable();
                $table->integer('16_coc')->nullable();
                $table->integer('16_application')->nullable();

                $table->integer('17_late')->nullable();
                $table->integer('17_undertime')->nullable();
                $table->integer('17_overtime')->nullable();
                $table->integer('17_coc')->nullable();
                $table->integer('17_application')->nullable();

                $table->integer('18_late')->nullable();
                $table->integer('18_undertime')->nullable();
                $table->integer('18_overtime')->nullable();
                $table->integer('18_coc')->nullable();
                $table->integer('18_application')->nullable();

                $table->integer('19_late')->nullable();
                $table->integer('19_undertime')->nullable();
                $table->integer('19_overtime')->nullable();
                $table->integer('19_coc')->nullable();
                $table->integer('19_application')->nullable();

                $table->integer('20_late')->nullable();
                $table->integer('20_undertime')->nullable();
                $table->integer('20_overtime')->nullable();
                $table->integer('20_coc')->nullable();
                $table->integer('20_application')->nullable();

                $table->integer('21_late')->nullable();
                $table->integer('21_undertime')->nullable();
                $table->integer('21_overtime')->nullable();
                $table->integer('21_coc')->nullable();
                $table->integer('21_application')->nullable();

                $table->integer('22_late')->nullable();
                $table->integer('22_undertime')->nullable();
                $table->integer('22_overtime')->nullable();
                $table->integer('22_coc')->nullable();
                $table->integer('22_application')->nullable();

                $table->integer('23_late')->nullable();
                $table->integer('23_undertime')->nullable();
                $table->integer('23_overtime')->nullable();
                $table->integer('23_coc')->nullable();
                $table->integer('23_application')->nullable();

                $table->integer('24_late')->nullable();
                $table->integer('24_undertime')->nullable();
                $table->integer('24_overtime')->nullable();
                $table->integer('24_coc')->nullable();
                $table->integer('24_application')->nullable();

                $table->integer('25_late')->nullable();
                $table->integer('25_undertime')->nullable();
                $table->integer('25_overtime')->nullable();
                $table->integer('25_coc')->nullable();
                $table->integer('25_application')->nullable();

                $table->integer('26_late')->nullable();
                $table->integer('26_undertime')->nullable();
                $table->integer('26_overtime')->nullable();
                $table->integer('26_coc')->nullable();
                $table->integer('26_application')->nullable();

                $table->integer('27_late')->nullable();
                $table->integer('27_undertime')->nullable();
                $table->integer('27_overtime')->nullable();
                $table->integer('27_coc')->nullable();
                $table->integer('27_application')->nullable();

                $table->integer('28_late')->nullable();
                $table->integer('28_undertime')->nullable();
                $table->integer('28_overtime')->nullable();
                $table->integer('28_coc')->nullable();
                $table->integer('28_application')->nullable();

                $table->integer('29_late')->nullable();
                $table->integer('29_undertime')->nullable();
                $table->integer('29_overtime')->nullable();
                $table->integer('29_coc')->nullable();
                $table->integer('29_application')->nullable();

                $table->integer('30_late')->nullable();
                $table->integer('30_undertime')->nullable();
                $table->integer('30_overtime')->nullable();
                $table->integer('30_coc')->nullable();
                $table->integer('30_application')->nullable();

                $table->integer('31_late')->nullable();
                $table->integer('31_undertime')->nullable();
                $table->integer('31_overtime')->nullable();
                $table->integer('31_coc')->nullable();
                $table->integer('31_application')->nullable();


                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtr_process');
    }
}
