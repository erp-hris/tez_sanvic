<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('work_schedules')) {
            Schema::create('work_schedules', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code')->nullable();
                $table->string('name');
                $table->string('schedule_type')->nullable();
                $table->boolean('auto_lunch')->nullable();

                $table->integer('sunday_time_in')->nullable();
                $table->integer('sunday_lunch_out')->nullable();
                $table->integer('sunday_lunch_in')->nullable();
                $table->integer('sunday_time_out')->nullable();
                $table->integer('sunday_strict_mid')->nullable();
                $table->integer('sunday_flexi')->nullable();
                $table->integer('sunday_flexi_time')->nullable();
                $table->boolean('sunday_restday')->nullable();

                $table->integer('monday_time_in')->nullable();
                $table->integer('monday_lunch_out')->nullable();
                $table->integer('monday_lunch_in')->nullable();
                $table->integer('monday_time_out')->nullable();
                $table->integer('monday_strict_mid')->nullable();
                $table->integer('monday_flexi')->nullable();
                $table->integer('monday_flexi_time')->nullable();
                $table->boolean('monday_restday')->nullable();

                $table->integer('tuesday_time_in')->nullable();
                $table->integer('tuesday_lunch_out')->nullable();
                $table->integer('tuesday_lunch_in')->nullable();
                $table->integer('tuesday_time_out')->nullable();
                $table->integer('tuesday_strict_mid')->nullable();
                $table->integer('tuesday_flexi')->nullable();
                $table->integer('tuesday_flexi_time')->nullable();
                $table->boolean('tuesday_restday')->nullable();

                $table->integer('wednesday_time_in')->nullable();
                $table->integer('wednesday_lunch_out')->nullable();
                $table->integer('wednesday_lunch_in')->nullable();
                $table->integer('wednesday_time_out')->nullable();
                $table->integer('wednesday_strict_mid')->nullable();
                $table->integer('wednesday_flexi')->nullable();
                $table->integer('wednesday_flexi_time')->nullable();
                $table->boolean('wednesday_restday')->nullable();

                $table->integer('thursday_time_in')->nullable();
                $table->integer('thursday_lunch_out')->nullable();
                $table->integer('thursday_lunch_in')->nullable();
                $table->integer('thursday_time_out')->nullable();
                $table->integer('thursday_strict_mid')->nullable();
                $table->integer('thursday_flexi')->nullable();
                $table->integer('thursday_flexi_time')->nullable();
                $table->boolean('thursday_restday')->nullable();

                $table->integer('friday_time_in')->nullable();
                $table->integer('friday_lunch_out')->nullable();
                $table->integer('friday_lunch_in')->nullable();
                $table->integer('friday_time_out')->nullable();
                $table->integer('friday_strict_mid')->nullable();
                $table->integer('friday_flexi')->nullable();
                $table->integer('friday_flexi_time')->nullable();
                $table->boolean('friday_restday')->nullable();

                $table->integer('saturday_time_in')->nullable();
                $table->integer('saturday_lunch_out')->nullable();
                $table->integer('saturday_lunch_in')->nullable();
                $table->integer('saturday_time_out')->nullable();
                $table->integer('saturday_strict_mid')->nullable();
                $table->integer('saturday_flexi')->nullable();
                $table->integer('saturday_flexi_time')->nullable();
                $table->boolean('saturday_restday')->nullable();


                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->boolean('is_deleted')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_schedules');
    }
}
