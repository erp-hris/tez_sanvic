<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employees')) {
            Schema::create('employees', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('company_id')->nullable();
                $table->string('employee_number')->nullable();
                $table->boolean('completed')->nullable();
                $table->integer('active')->nullable();
                $table->string('last_name')->nullable();
                $table->string('first_name')->nullable();
                $table->string('middle_name')->nullable();
                $table->string('extension_name')->nullable();
                $table->string('nickname')->nullable();
                $table->string('contact_number')->nullable();
                $table->string('mobile_number')->nullable();
                $table->string('telephone_number')->nullable();
                $table->date('birthday')->nullable();
                $table->string('birth_place')->nullable();
                $table->string('email_address')->nullable();
                $table->string('gender')->nullable();
                $table->string('civil_status_id')->nullable();
                $table->string('other_civil_status')->nullable();
                $table->integer('citizenship_id')->nullable();
                $table->integer('citizenship_country_id')->nullable();
                $table->boolean('filipino')->nullable();
                $table->boolean('naturalized')->nullable();
                $table->decimal('height',4,2)->nullable();
                $table->decimal('weight',4,2)->nullable();
                $table->string('blood_type_id')->nullable();
                $table->string('pagibig')->nullable();
                $table->string('gsis')->nullable();
                $table->string('philhealth')->nullable();
                $table->string('tin')->nullable();
                $table->string('sss')->nullable();
                $table->string('govt_issued_id')->nullable();
                $table->string('govt_issued_id_number')->nullable();
                $table->string('govt_issued_id_place')->nullable();
                $table->date('govt_id_date_issued')->nullable();
                $table->date('govt_id_valid_until')->nullable();
                $table->string('residential_house_number')->nullable();
                $table->string('residential_street' )->nullable();
                $table->string('residential_subdivision')->nullable();
                $table->string('residential_brgy')->nullable();
                $table->string('residential_city_id')->nullable();
                $table->string('residential_province_id')->nullable();
                $table->string('residential_zip_code')->nullable();
                $table->string('residential_country_id' )->nullable();
                $table->string('residential_telephone_number')->nullable();
                $table->string('permanent_house_number')->nullable();
                $table->string('permanent_street' )->nullable();
                $table->string('permanent_subdivision')->nullable();
                $table->string('permanent_brgy')->nullable();
                $table->string('permanent_city_id')->nullable();
                $table->string('permanent_province_id')->nullable();
                $table->string('permanent_zip_code')->nullable();
                $table->string('permanent_country_id' )->nullable();
                $table->string('permanent_telephone_number')->nullable();
                $table->string('image_path')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
