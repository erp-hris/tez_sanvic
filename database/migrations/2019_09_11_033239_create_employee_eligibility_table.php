<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEligibilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('employee_eligibility')) {
            Schema::create('employee_eligibility', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->string('eligibility_id')->nullable();
                $table->string('rating')->nullable();
                $table->date('examination_date')->nullable();
                $table->date('release_date')->nullable();
                $table->string('examination_place')->nullable();
                $table->string('license_number')->nullable();
                $table->date('validity_date')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_eligibility');
    }
}
